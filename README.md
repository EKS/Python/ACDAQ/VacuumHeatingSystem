Vacuum Heating System
=====================
This is the PyAcdaq/pykka based project for the GSI Vacuum Heating System (VHS) to be used at ESR and SIS. It is utilizing PXIe HW from NI.
It measures temperatures from analog inputs and applies a fuzzy system to calculate the digital output pulse width ratio for the heating power supplies.

Run hvs from command line:
----------------------
- `py hvs.py -h`
- `py hvs.py -b localhost -l hvs.log`


Dependencies
------------
You can use the requerements file to install the dependencies.
´python -m pip install -r requirements.txt´

### Mandatory
- *platformdirs*
- *pyacdaq*: https://git.gsi.de/EKS/Python/ACDAQ/PyAcdaq
- *nidaqmx*: https://pypi.org/project/nidaqmx/
- *python-statemachine*: https://pypi.org/project/python-statemachine/
- *simpful*: https://pypi.org/project/simpful/
- *pandas*: https://pypi.org/project/pandas/

### Optional
- *matplotlib*: https://pypi.org/project/matplotlib/
- *jupyterlab*: https://pypi.org/project/jupyterlab/
- *python-statemachine diagrams: https://python-statemachine.readthedocs.io/en/latest/diagram.html
  - *pydot*: https://pypi.org/project/pydot/
    - graphviz*: https://graphviz.org/ 
      - sudo apt install graphviz
- *influxdb-client*

Known issues
------------
- pyacdaq is not jet published on pypi.org.
  - Install with: `pip install pyacdaq --index-url https://git.gsi.de/api/v4/projects/2332/packages/pypi/simple`
- tkinter is maybe not installed by default on Linux.
  - You may need to install missing tkinter, Make sure to install the version associated with yout Python version!
  >sudo apt install python311-tk

- Multiprocessing does work on Linux within Spyder, only.
  - vhs.py crashes with SIGSEGV when started from console.
  - Windows version does not use multiporocessing for now.

Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme

Licensed under the EUPL. Refer to license files on disc.

Acknowledgements
----------------
- The MQTT part is inpired by http://www.steves-internet-guide.com

Revision History
----------------
Revision 0.0.1.0 - 11.01.2024 H.Brand@gsi.de
- Improve logging.
- Handle exception (daqmx) in periodic action of DAQmxAi and DAQmxDoPwm.
- Improve starting FuzzyControllerProcess. (Hopefully)

Revision 0.0.0.19 - 11.01.2024 H.Brand@gsi.de
- Improve logging.
- Correct a depricated daqmx attribute.
- Add more constructor parameters
  - nof_channels, samples_per_channel, sampling_freq

Revision 0.0.0.18 - 09.01.2024 H.Brand@gsi.de
- pip install --upgrade packages.

Revision 0.0.0.17 - 29.08.2023 H.Brand@gsi.de
- Ignore disabled channels for cooldown.

Revision 0.0.0.16 - 28.08.2023 H.Brand@gsi.de
- Implement fuzzy calculations in FuzzyProcess derived from Proccess class.

Revision 0.0.0.15 - 11.08.2023 H.Brand@gsi.de
- Improved multiporocessing for fuzzy calculations.

Revision 0.0.0.13 - 04.08.2023 H.Brand@gsi.de
- Add multiporocessing for fuzzy calculations.

Revision 0.0.0.12 - 04.08.2023 H.Brand@gsi.de
- Vhs._periodic(): slf._t_start_profile = self._t_start_cooldown = None when in 'manual' or 'controlled' state.

Revision 0.0.0.11 - 03.08.2023 H.Brand@gsi.de
- Switch to Controlled when Profile or Cooldown has finished.
- Reordering code in Vhs._periodic() and reducing DAQmxAi to 500S@1kH reduced the interval time from ~15s to <5s.

Revision 0.0.0.8 - 03.08.2023 H.Brand@gsi.de
- Use 'Temperatur 3' instead of 'Starttemperatur' for Cooldown.
- Move trial/handle_http_request.py src/vacuum_heating_system/vhs_http_server.py

Revision 0.0.0.7 - 02.08.2023 H.Brand@gsi.de
- Upgrade nidaqmx to 0.8.0
- Add state Cooling
- Modify http server to handle transitions to Cooling.

Revision 0.0.0.2 - 16.06.2023 H.Brand@gsi.de
- Upgrade nidaqmx to 0.7.0
- Add nidaqmx ai input range
- Scale fuzzy (mamdani) calculated power

Revision 0.0.0.1 - 05.05.2023 H.Brand@gsi.de
- Beginn of commissioning

Revision 0.0.0.0 - 13.02.2023 H.Brand@gsi.de
- Project start
