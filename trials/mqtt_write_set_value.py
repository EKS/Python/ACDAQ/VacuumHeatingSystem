# -*- coding: utf-8 -*-
"""
Created on Tue Mar 14 09:33:47 2023

@author: brand
"""
from datetime import datetime
import json
import numpy as np
import paho.mqtt.publish as publish

"""
publish.single(topic, payload=None, qos=2, retain=False, hostname="localhost",
    port=1883, client_id="", keepalive=60, will=None, auth=None, tls=None,
    protocol=mqtt.MQTTv311, transport="tcp")
"""
broker = 'sadpc009'
client_id = f'VHS_Test_{str(datetime.now()).replace(" ", "_")}'
process = 'vacpc021'
actor = 'Vhs'
nof_channels = 256
topic_prefix = f'{process}/{actor}'
ones = np.ones(nof_channels, dtype=bool)
zeros = np.zeros(nof_channels, dtype=bool)

if False:
    topic = topic_prefix + '/Set-DO'
    value = np.copy(ones)
    # value[0] = True
    publish.single(topic, json.dumps(value.tolist()), qos=2, hostname=broker, client_id='VHS-HB-Publish')
if False:
    topic = topic_prefix + '/Set-Enabled'
    value = np.copy(ones)
    # value[0] = True
    publish.single(topic, json.dumps(value.tolist()), qos=2, hostname=broker, client_id='VHS-HB-Publish')
if False:
    topic = topic_prefix + '/Set-Forced'
    value = np.copy(ones)
    # value[0] = True
    publish.single(topic, json.dumps(value.tolist()), qos=2, hostname=broker, client_id='VHS-HB-Publish')
if False:
    topic = topic_prefix + '/Set-PWMRatios'
    value = np.full(nof_channels, 50.0)
    # value[0] = 50.
    publish.single(topic, json.dumps(value.tolist()), qos=2, hostname=broker, client_id='VHS-HB-Publish')
if False:
    topic = topic_prefix + '/Set-SPTemperatures'
    value = np.full(nof_channels, 20.0)
    # value[0] = 20.
    publish.single(topic, json.dumps(value.tolist()), qos=2, hostname=broker, client_id='VHS-HB-Publish')
if False:
    topic = topic_prefix + '/Set-ChannelDO'
    value = {'channel': 0, 'state': True}
    publish.single(topic, json.dumps(value), qos=2, hostname=broker, client_id='VHS-HB-Publish')
if False:
    topic = topic_prefix + '/Set-ChannelEnabled'
    value = {'channel': 0, 'enabled': True}
    publish.single(topic, json.dumps(value), qos=2, hostname=broker, client_id='VHS-HB-Publish')
if False:
    topic = topic_prefix + '/Set-ChannelForced'
    value = {'channel': 0, 'forced': False}
    publish.single(topic, json.dumps(value), qos=2, hostname=broker, client_id='VHS-HB-Publish')
if False:
    topic = topic_prefix + '/Set-ChannelPWMRatio'
    value = {'channel': 0, 'ratio': 1.0}
    publish.single(topic, json.dumps(value), qos=2, hostname=broker, client_id='VHS-HB-Publish')
if False:
    topic = topic_prefix + '/Set-ChannelSPTemperature'
    value = {'channel': 0, 'sptemperature': 20.0}
    publish.single(topic, json.dumps(value), qos=2, hostname=broker, client_id='VHS-HB-Publish')
if False:
    topic = topic_prefix + '/Request_State_Change'
    value = 'start_profile'
    publish.single(topic, value, qos=2, hostname=broker, client_id='VHS-HB-Publish')
if False:
    topic = topic_prefix + '/Set-Profiles'
    value = 'D:\\Brand\\Python\\VacuumHeatingSystem\\profiles\\Beispiel_config_ESR_Sued_GS_50Grad_short.csv'
    publish.single(topic, value, qos=2, hostname=broker, client_id='VHS-HB-Publish')
