# -*- coding: utf-8 -*-
"""
https://matplotlib.org/stable/tutorials/colors/colors.html

Created on Fri Mar 31 14:02:17 2023

@author: brand
"""
import matplotlib.pyplot as plt
import numpy as np

status_int = {
    'unknown': 0,
    'super_cooled': 1,
    'under_cooled': 2,
    'in_range': 3,
    'heating': 4,
    'forced': 5,
    'over_heated': 6,
    'super_heated': 7,
    'broken': 8,
    'short_cut': 9}
status_color = {
    0: '0.0',
    1: 'cyan',
    2: 'darkblue',
    3: 'darkgreen',
    4: 'lightgreen',
    5: 'yellowgreen',
    6: 'darkred',
    7: 'red',
    8: '0.8',
    9: 'magenta'}
print('Status int:', status_int)
print('Status color:', status_color)
status = [status_int['unknown'], status_int['super_cooled'], status_int['under_cooled'], status_int['in_range'], status_int['heating'], status_int['forced'], status_int['over_heated'], status_int['super_heated'], status_int['broken'], status_int['short_cut']]
print('Status:', status)
color = []
for s in status:
    # print(s)
    color.append(status_color[s])
print('Color:', color)
y = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], dtype=np.float64)
x = np.linspace(0, y.size - 1, y.size)
plt.bar(x, y, color=color)
plt.show()
