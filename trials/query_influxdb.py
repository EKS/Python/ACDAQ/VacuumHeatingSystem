#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 16 11:47:45 2023

@author: brand
"""

from influxdb_client import InfluxDBClient
import matplotlib.pyplot as plt
import time

url = 'http://sadpc009:8086'
token = 'YCtPg6E_2fq6LRUuM82m1MCFUv9oJKZ7N94koFioSJiLjuFNhVQBWlkrgT3VptqX42n8dWB3J1RLzxTrMBY5WA=='
org = 'GSI-VAC'
bucket = 'VHS'
time_span = '-6h'
# aggregate_window = None
aggregate_window = '60s'
process = 'vacpc021'
actor = 'Vhs'
channel = 0
attributes = ('Temperatures', 'DTemperatures', 'GTemperatures', 'SPTemperatures', 'PWMRatios', 'Status')
t0 = time.time()
queries = []
for attribute in attributes:
    query = 'from(bucket: "VHS") |> range(start: ' + time_span + ') |> filter(fn:(r) => r["Process"] == "' + process + '") |> filter(fn:(r) => r["Actor"] == "' + actor + '") |> filter(fn:(r) => r["Attribute"] == "' + attribute + '") |> filter(fn:(r) => r["Channel"] == "' + str(channel) + '")'
    if aggregate_window:
        query += ' |> aggregateWindow(every: ' + aggregate_window + ', fn: last, createEmpty: false) |> yield(name: "last")'
    queries.append(query)

if True:
    with InfluxDBClient(url=url, token=token, org=org) as client:
        plt.title(f'Historical data for {process}/{actor}[{channel}]')
        plt.xlabel('date-time')
        plt.ylabel('value')
        query_api = client.query_api()
        for ii, query in enumerate(queries):
            # print('Query:', query)
            result = query_api.query(org=org, query=query)
            results = []
            values = []
            times = []
            for table in result:
                for record in table.records:
                    try:
                        # print('record:\n', record)
                        # print('type(record["_time"])', type(record["_time"]))
                        results.append((str(record["_time"]), record.get_value()))
                        times.append(record["_time"])
                        values.append(record.get_value())
                    except Exception:
                        pass
            # print(results)
            plt.plot(times, values, label=attributes[ii][:4])
        plt.legend(loc='best')
        plt.show()
else:  # show all
    with InfluxDBClient(url=url, token=token, org=org) as client:
        query_api = client.query_api()
        tables = query_api.query('from(bucket: "VHS") |> range(start: -5m)')
        # print('tables:\n', tables)
        if True:
            for table in tables:
                # print('table:\n', table)
                if True:
                    for record in table.records:
                        try:
                            #print('record:\n', record)
                            print(str(record["Process"]) + "." + str(record["Actor"]) + "." + str(record["Attribute"]) + "[" + str(record["Channel"]) + "]: " + str(record["_time"]) + " - " + record.get_measurement() + " " + record.get_field() + "=" + str(record.get_value()))
                        except Exception:
                            pass
print('Iteration time:', time.time() - t0)
