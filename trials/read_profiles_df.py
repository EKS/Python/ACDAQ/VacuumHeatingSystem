# -*- coding: utf-8 -*-
"""
Created on Mon Mar 13 11:56:17 2023

@author: brand
"""
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
import time

nof_channels = 256
input_ok = False
profile_filename_default = '20230703_Test_SIS18_VAB7_config.csv'
match os.name:
    case 'nt':
        profile_dir = 'D:\\Brand\\Python\\VacuumHeatingSystem\\profiles'
    case _:
        profile_dir = '/home/brand/Python/VacuumHeatingSystem/profiles'
        pass
profile_filename = os.path.join(profile_dir, profile_filename_default)
while not input_ok:
    channel = int(input(f'Select channel to plot ([0,{nof_channels-1}]):'))
    if 0 <= channel < nof_channels:
        break
all_channels = np.linspace(start=0, stop=nof_channels - 1, num=nof_channels, endpoint=True, dtype=np.int16)
ramps = pd.read_csv(profile_filename,
                    sep=';',
                    encoding='ISO-8859_2',
                    header=0,
                    dtype={0: 'int16', 1: 'float64', 2: 'float64', 3: 'float64', 4: 'float64', 5: 'float64', 6: 'float64', 7: 'float64', 8: 'float64', 9: 'float64', 10: 'float64', 11: 'float64', 12: 'float64', 13: 'float64', 14: 'str'}
                    )
default_row = ['-1', False, float('-inf'), float('-inf'), float('-inf'), float('-inf'), float('-inf'), float('-inf'), float('-inf'), float('-inf'), float('-inf'), float('-inf'), float('-inf'), float('-inf'), float('-inf'), 'NA']
enabled = np.ones(len(ramps.index), dtype=bool)
ramps.insert(1, 'Enabled', enabled)
print(ramps.columns)
ramps.sort_values(by=['Channel'], ascending=True, inplace=True)
existing_channels = ramps['Channel']
missing_channels = np.delete(all_channels, existing_channels)
for missing_channel in missing_channels:
    new_row = default_row
    new_row[0] = missing_channel
    ramps.loc[len(ramps)] = new_row
ramps.sort_values(by=['Channel'], ascending=True, ignore_index=True, inplace=True)
ramps['Haltezeit 1'] = ramps['Haltezeit 1'] * 60.
ramps['Haltezeit 2'] = ramps['Haltezeit 2'] * 60.
ramps['Rampe 1'] = ramps['Rampe 1'] / 3600.
ramps['Rampe 2'] = ramps['Rampe 2'] / 3600.
ramps['Rampe 3'] = ramps['Rampe 3'] / 3600.
rsign1 = np.sign((ramps['Temperatur 1'] - ramps['Starttemperatur']).to_numpy())
rsign2 = np.sign((ramps['Temperatur 2'] - ramps['Temperatur 1']).to_numpy())
rsign3 = np.sign((ramps['Temperatur 3'] - ramps['Temperatur 2']).to_numpy())
# ramps = ramps.reset_index()
print(ramps)
print('Überheizen', ramps['Überheizen'][channel])
print('Unterheizen', ramps['Unterheizen'][channel])
print('Warnung Über.', ramps['Warnung Über.'][channel])
print('Warnung Unter.', ramps['Warnung Unter.'][channel])
ramps.to_csv('test.csv', sep='\t')
# Times and temperature offsets need to be checked.
dt0 = tr1 = abs(ramps['Temperatur 1'] - ramps['Starttemperatur']) / ramps['Rampe 1']
th1 = ramps['Haltezeit 1']
dt1 = dt0 + th1
tr2 = abs(ramps['Temperatur 2'] - ramps['Temperatur 1']) / ramps['Rampe 2']
dt2 = dt1 + tr2
th2 = ramps['Haltezeit 2']
dt3 = dt2 + th2
tr3 = abs(ramps['Temperatur 3'] - ramps['Temperatur 2']) / ramps['Rampe 3']
dt4 = dt3 + tr3
# print(f'dt4\n{dt4}')
print(max(tr1.dropna().unique()), max(th1.dropna().unique()), max(tr2.dropna().unique()), max(th2.dropna().unique()), max(tr3.dropna().unique()))
profile_length = max(tr1.dropna().unique()) + max(th1.dropna().unique()) + max(tr2.dropna().unique()) + max(th2.dropna().unique()) + max(tr3.dropna().unique())
print(f'Maximum Length = {profile_length}')
spt = np.zeros(nof_channels, dtype=np.float64)
spt_t = []
t = []
t0 = time.time()
dt = 0.  # time.time() - t0
while dt < profile_length:
    t.append(dt / 3600.)
    for ii in range(len(ramps)):
        if dt < dt0[ii]:
            spt[ii] = ramps.loc[ii, 'Starttemperatur'] + ramps.loc[ii, 'Rampe 1'] * dt * rsign1[ii]
        elif dt < dt1[ii]:
            spt[ii] = ramps.loc[ii, 'Temperatur 1']
        elif dt < dt2[ii]:
            spt[ii] = ramps.loc[ii, 'Temperatur 1'] + ramps.loc[ii, 'Rampe 2'] * (dt - dt1[ii]) * rsign2[ii]
        elif dt < dt3[ii]:
            spt[ii] = ramps.loc[ii, 'Temperatur 2']
        elif dt < dt4[ii]:
            spt[ii] = ramps.loc[ii, 'Temperatur 2'] + ramps.loc[ii, 'Rampe 3'] * (dt - dt3[ii]) * rsign3[ii]
        else:
            spt[ii] = ramps.loc[ii, 'Starttemperatur']
    # print(dt / 3600., ramps.loc[0, 'Channel'], ramps.loc[0, 'Enabled'], dt4[0] / 3600., spt[0])
    spt_t.append(spt[channel])
    dt += 60.
    pass
# print(spt_t)
plt.title(f'Temperature Profile [{channel}]')
plt.xlabel('Time /h')
plt.ylabel('T / degC')
plt.plot(t, spt_t)
plt.show()

# print(f'dt = {dt:.3}s\n{spt}')
