# -*- coding: utf-8 -*-
"""
Created on Fri Mar  3 11:39:58 2023

@author: brand
"""

import numpy as np
import time

nof_channels = 64
sleep_time = 0.01
ones = np.ones((nof_channels, 1), dtype=bool)
zeros = np.zeros((nof_channels, 1), dtype=bool)
# ratios = np.random.random_sample((nof_channels, 1))
ratios = np.linspace(0.05, 0.95, nof_channels, endpoint=False, dtype=np.float64)
print(ratios)
t0 = time.time()
print(time.asctime(time.localtime(t0)))
for _ in range(100):
    time.sleep(sleep_time)
    t_diff = (time.time() - t0)
    ratio = t_diff - int(t_diff)
    do = ratios > ratio
    print(round(t_diff, 3), do[:])
