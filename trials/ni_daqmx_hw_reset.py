#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Reset all hardware modules supported and found by NI-DAQmx device driver.

Created on Tue Feb 14 13:18:22 2023
@author: H.Brand@gsi.de
"""

import nidaqmx
import nidaqmx.system

system = nidaqmx.system.System.local()
print('NI DAQmx version:', system.driver_version)
# Reset devices
for device in system.devices:
    print(f'Resetting {device}')
    device.reset_device()
