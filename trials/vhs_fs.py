#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
The is the implementation of the VHS Fuzzy System with Simpful.

You need to install:
- pip install simpful
- pip install seaborn

Lizenziert unter EUPL V. 1.2

Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
"""

import matplotlib.pyplot as plt
import numpy as np
import simpful as sf
import multiprocessing as mp
# from multiprocessing import Process, Queue
# from multiprocessing import Pool
import time


def create_uhvhc_fs(mode='mamdani'):
    # Create a fuzzy system object for UHVHC.
    FS = sf.FuzzySystem()
    create_uhvhc_fs_input(FS)
    create_uhvhc_fs_output(FS, mode)
    R = create_uhvhc_fs_rules()
    FS.add_rules(R)
    return FS, R


def create_uhvhc_fs_input(FS):
    """Define fuzzy sets and linguistic variables."""
    T_1 = sf.TrapezoidFuzzySet(a=-25., b=-25., c=-20., d=-0, term='negative')
    T_2 = sf.TrapezoidFuzzySet(a=-20., b=0., c=0., d=20., term='constant')
    T_3 = sf.TrapezoidFuzzySet(a=0., b=20., c=25., d=25., term='positive')
    # sf.LinguisticVariable([T_1, T_2, T_3], universe_of_discourse=[-25, 25]).plot()
    FS.add_linguistic_variable('TDiff', sf.LinguisticVariable([T_1, T_2, T_3], concept='Temperature-Difference', universe_of_discourse=[-25, 25]))

    G_1 = sf.TrapezoidFuzzySet(a=-25., b=-25., c=-20., d=-10., term='negative')
    G_2 = sf.TrapezoidFuzzySet(a=-20., b=-10., c=0., d=20., term='constant')
    G_3 = sf.TrapezoidFuzzySet(a=0., b=20, c=25, d=25, term='positive')
    # sf.LinguisticVariable([G_1, G_2, G_3], universe_of_discourse=[-25, 25]).plot()
    FS.add_linguistic_variable('TGain', sf.LinguisticVariable([G_1, G_2, G_3], concept='Temperature-Gain', universe_of_discourse=[-25, 25]))
    return FS


def create_uhvhc_fs_output(FS, mode='mamdani'):
    """Define UHVHC Fuzzy System Output."""
    match mode:
        case 'mamdani':
            # Define output fuzzy sets and linguistic variable
            P_1 = sf.TrapezoidFuzzySet(a=1., b=100., c=100., d=100., term='on')
            # P_1 = sf.FuzzySet(points=[[0.0, 0.0], [1.0, 0.0], [10.0, .1], [30.0, 0.2], [50.0, 0.6], [75.0, 0.9], [90.0, 0.95], [100.0, 1.0]], term="on")
            P_2 = sf.CrispSet(a=0, b=1, term="off")
            # P_2 = sf.TrapezoidFuzzySet(a=0., b=0., c=1., d=2., term='off')
            # sf.LinguisticVariable([P_1], universe_of_discourse=[0, 120]).plot()
            FS.add_linguistic_variable('Power', sf.LinguisticVariable([P_1, P_2], concept='Power', universe_of_discourse=[0, 100]))
            pass
        case 'sugeno':
            # Define output functions
            FS.set_output_function('on', '-TDiff*3 - TGain*3')
            FS.set_crisp_output_value('off', 0.)
            pass
        case _:
            pass
    pass


def create_uhvhc_fs_rules():
    """Define UHVHC Fuzzy System Rules."""
    R0 = 'IF (TDiff IS constant) AND (TGain IS negative) THEN (Power IS on) WEIGHT 1.0'
    R1 = 'IF (TDiff IS constant) AND (TGain IS constant) THEN (Power IS off WEIGHT 1.0'
    R2 = 'IF (TDiff IS constant) AND (TGain IS positive) THEN (Power IS off) WEIGHT 1.0'
    R3 = 'IF (TDiff IS positive) AND (TGain IS negative) THEN (Power IS on) WEIGHT 1.0'
    R4 = 'IF (TDiff IS positive) AND (TGain IS constant) THEN (Power IS off) WEIGHT 1.0'
    R5 = 'IF (TDiff IS positive) AND (TGain IS positive) THEN (Power IS off) WEIGHT 1.0'
    R6 = 'IF (TDiff IS negative) AND (TGain IS negative) THEN (Power IS on) WEIGHT 1.0'
    R7 = 'IF (TDiff IS negative) AND (TGain IS constant) THEN (Power IS on) WEIGHT 1.0'
    R8 = 'IF (TDiff IS negative) AND (TGain IS positive) THEN (Power IS on) WEIGHT 1.0'
    R = [R0, R1, R2, R3, R4, R5, R6, R7, R8]
    return R


def calc_uhvhc_fs(FS, d, g, mode='mamdani', strength=False):
    """ Caluculate power using UHVHC Fuzzy System."""
    FS.set_variable("TDiff", d)
    FS.set_variable("TGain", g)
    strengths = FS.get_firing_strengths()
    match mode:
        case 'mamdani':
            p = FS.Mamdani_inference(["Power"])["Power"]
            if p < 5.:
                p = 0.
            else:
                p *= 1.5
            pass
        case 'sugeno':
            p = FS.Sugeno_inference(["Power"])["Power"]
            if p < 5.:
                p = 0.
            else:
                p *= 2.
            pass
            pass
        case _:
            p = FS.inference(["Power"])["Power"]  # Perform inference
            pass
    p = min(max(0, p), 100)
    if strength:
        return p, strengths
    else:
        return p, None


def plot_uhvhc_fs(fs, xlist, ylist, mode='mamdani'):
    """Plot Power vs. TDiff, TGain for selected values"""
    # FS.plot_variable('TDiff')
    # FS.plot_variable('TGain')
    # FS.plot_variable('Power')
    X, Y = np.meshgrid(xlist, ylist)
    P = np.zeros((xlist.size, ylist.size))
    pdi = 0
    for d in xlist:
        pgi = 0
        for g in ylist:
            p, s = calc_uhvhc_fs(fs, d, g, mode)
            P[pdi, pgi] = p
            pgi += 1
        pdi += 1
    plt.figure()
    cp = plt.contourf(X, Y, P)
    plt.colorbar(cp)
    plt.xlabel('TDiff /degC')
    plt.ylabel('TGain /degC/s')
    match mode:
        case 'mamdani':
            plt.title('UHVHC Fuzzy System (Mamdani)')
            pass
        case 'sugeno':
            plt.title('UHVHC Fuzzy System (Sugeno)')
            pass
        case _:
            plt.title('UHVHC Fuzzy System (auto)')
            pass
    plt.show()
    return P, X, Y


def print_uhvhc_fs(fs, xlist, ylist, mode='mamdani', strength=True):
    """Print TDiff, TGain, Power with rules for selected values"""
    for d in xlist:
        for g in ylist:
            p, strengths = calc_uhvhc_fs(fs, d, g, mode, strength)
            print('\nTDiff=', d, 'TGain=', g, 'Power=', p)
            if strengths:
                for i in range(len(strengths)):
                    if strengths[i] > 0:
                        print('\t', R[i], 'Strength=', strengths[i])
    pass


def calc_powers(q, fs, subset, D, G, mode='mamdani'):
    """
    Caluculate powers using Fuzzy System.

    Parameters
    ----------
    D : numpy.array(numpy.float64)
        Array of Temperature differences d = t - set-value. Unit: K
    G : numpy.array(numpy.float64)
        Array of Gains of temperature. Unit: K/s.
    mode : str, optional
        Interference mode: 'mamdani' or 'sugeno'. The default is 'mamdani'.

    Returns
    -------
    P : numpy.array(numpy.float64)
        Array of Powers as PWM ratio.

    """
    # t0 = time.time()
    # time.sleep(.75)
    # print(f'D: {D}')
    # print(f'G: {G}')
    # print(f'Size of D={D.size}, Size of G={G.size}')
    P = np.zeros((min(D.size, G.size)), dtype=np.float64)
    for ii in range(min(D.size, G.size)):
        p, s = calc_uhvhc_fs(fs, D[ii], G[ii], mode, strength=False)
        # print(f'ii={ii}, d={D[ii]}, , g={G[ii]}, p={p}, s={s}')
        P[ii] = p
        pass
    # print(f'P: {P}')
    # print(self._name + ' FuzzyController calc_powers(): dt=' + str(time.time() - t0))
    if q:
        q.put({'subset': subset, 'Power': P})
    else:
        return {'subset': subset, 'Power': P}


def calc_powers_task(p):
    """Calculate powers using Fuzzy System.

    Parameters
    ----------
    D : numpy.array(numpy.float64)
        Array of Temperature differences d = t - set-value. Unit: K
    G : numpy.array(numpy.float64)
        Array of Gains of temperature. Unit: K/s.
    mode : str, optional
        Interference mode: 'mamdani' or 'sugeno'. The default is 'mamdani'.

    Returns
    -------
    P : numpy.array(numpy.float64)
        Array of Powers as PWM ratio.

    """
    fs = p['FS']
    D = p['D']
    G = p['G']
    mode = p['mode']
    subset = p['subset']
    # t0 = time.time()
    # time.sleep(.75)
    # print(f'Size of D={D.size}, Size of G={G.size}')
    P = np.zeros((min(D.size, G.size)), dtype=np.float64)
    for ii in range(min(D.size, G.size)):
        p, s = calc_uhvhc_fs(fs, D[ii], G[ii], mode, strength=False)
        # print(f'ii={ii}, d={D[ii]}, , g={G[ii]}, p={p}, s={s}')
        P[ii] = p
        pass
    # print(self._name + ' FuzzyController calc_powers_task(): dt=' + str(time.time() - t0))
    return {'subset': subset, 'power': P}


if __name__ == '__main__':
    print("vhs_fs.py\n\
Copyright 2022 GSI Helmholtzzentrum für Schwerionenforschung GmbH\n\
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany\n\
eMail: H.Brand@gsi.de\n\
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme\n")
    mp.freeze_support()
    print(__name__, 'Create fuzzy system.')
    mode = 'mamdani'  # 'mamdani' or 'sugeno'
    FS, R = create_uhvhc_fs(mode)
    if True:
        FS.produce_figure(outputfile='', max_figures_per_row=4)
        xlist = np.linspace(-25.0, 25.0, 51)
        ylist = np.linspace(-25.0, 25.0, 51)
        # ylist = np.linspace(-5.0, 5.0, 51)
        P, X, Y = plot_uhvhc_fs(FS, xlist, ylist, mode)
        print('Power Minimium=', np.min(P[0]), 'Maximium=', np.max(P[0]))
    # Test antecedents values
    if True:
        print(__name__, 'Calculate some discrete values.')
        td_list = np.array([-25., -10., -5., 2.5, 0., 2.5, 5., 10., 15., 25.])
        tg_list = np.array([-25., -15., -10., -5., 0., 5., 10., 15., 25.])
        print_uhvhc_fs(FS, td_list, tg_list, mode, strength=False)

    if True:
        print(__name__, 'Calculate one discrete values.')
        d = np.float64(-15.)
        g = np.float64(-15.)
        p = calc_uhvhc_fs(FS, d, g, mode)
        print('\nTDiff=', d, 'TGain=', g, 'Power=', p)

    # Test performance direct call vs. processing of array subsets.
    if True:
        print(__name__, 'Text performance of fuzzy system.')
        nof_channels = 256
        # D = np.full(nof_channels, -15.)
        # G = np.full(nof_channels, -15.)
        D = np.random.rand(nof_channels) * 5. - 5.
        G = np.random.rand(nof_channels) * 5. - 5.

        # Direct call
        if True:
            print(__name__, 'Calculate using direct call.')
            t_start = time.time()
            P = calc_powers(None, FS, None, D, G, mode)
            t_stop = time.time()
            # print(f'Powers: {P}')
            print(f'Direct Call Time(calc_powers): {t_stop - t_start}s')
            pass

        # Start one process explicitly
        if True:
            print(__name__, 'Calculate using one process.')
            q = mp.Queue()
            process = mp.Process(target=calc_powers, args=(q, FS, 0, D, G, mode))
            print('Number of processes: 1')
            t_start = time.time()
            process.start()
            # time.sleep(0.1)
            # wait for all processes to complete
            if False:
                print(f'Process result: {q.get()}')
            else:
                P = q.get()['Power']
                # print(f'Process result: Power={P}')
            process.join()
            q.close()
            t_stop = time.time()
            # print(f'Powers = {P}')
            print(f'Process(calc_powers): {t_stop - t_start}s')
            pass
        # Start four processes explictly
        if True:
            print(__name__, 'Calculate using four processes.')
            P = np.full(nof_channels, 0.)
            power_results = [None, None, None, None]
            q = mp.Queue()
            processes = [mp.Process(target=calc_powers, args=(q, FS, 0, D[0:64], G[0:64], mode))]
            processes.append(mp.Process(target=calc_powers, args=(q, FS, 1, D[64:128], G[64:128], mode)))
            processes.append(mp.Process(target=calc_powers, args=(q, FS, 2, D[128:192], G[128:192], mode)))
            processes.append(mp.Process(target=calc_powers, args=(q, FS, 3, D[192:256], G[192:256], mode)))
            print(f'Number of explicit processes: {len(processes)}')
            t_start = time.time()
            for process in processes:
                process.start()
                # time.sleep(0.1)
            # wait for all processes to complete
            for process in processes:
                result = q.get()
                # print(f'result: {result}')
                power_results[result["subset"]] = result["Power"]
                process.join()
            q.close()
            # print(f'power_results: {power_results}')
            P[0:64] = power_results[0]
            P[64:128] = power_results[1]
            P[128:192] = power_results[2]
            P[192:256] = power_results[3]
            t_stop = time.time()
            # print(f'Powers = {P}')
            print(f'Process(calc_powers): {t_stop - t_start}s')
            pass
        # Start four processes using ProcessPool
        if True:
            print(__name__, 'Calculate using process pool with four processes.')
            P = np.full(nof_channels, 0.)
            power_results = [None, None, None, None]
            with mp.Pool() as pool:
                # issue one task for each call to the function
                f_p_0 = {'FS': FS, 'subset': 0, 'D': D[0:64], 'G': G[0:64], 'mode': mode}
                f_p_1 = {'FS': FS, 'subset': 1, 'D': D[64:128], 'G': G[64:128], 'mode': mode}
                f_p_2 = {'FS': FS, 'subset': 2, 'D': D[128:192], 'G': G[128:192], 'mode': mode}
                f_p_3 = {'FS': FS, 'subset': 3, 'D': D[192:256], 'G': G[192:256], 'mode': mode}
                fuzzy_parameters = [f_p_0, f_p_1, f_p_2, f_p_3]
                print(f'Number of pool-processes: {len(fuzzy_parameters)}')
                t_start = time.time()
                for result in pool.imap(calc_powers_task, fuzzy_parameters):
                    # handle the result
                    # print(f'>got {result}')
                    # print(f'>got result of subset: {result["subset"]}')
                    power_results[result["subset"]] = result["power"]
            P[0:64] = power_results[0]
            P[64:128] = power_results[1]
            P[128:192] = power_results[2]
            P[192:256] = power_results[3]
            # report that all tasks are completed
            t_stop = time.time()
            # print(f'Powers = {P}')
            print(f'ProcessPool(calc_powers_task): {t_stop - t_start}s')
            pass
    print(__name__, 'Done.')
