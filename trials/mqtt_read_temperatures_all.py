# -*- coding: utf-8 -*-
"""
Created on Mon Mar 13 13:55:04 2023

@author: brand
"""
import time
from datetime import datetime
import matplotlib.pyplot as plt
import numpy as np
import paho.mqtt.subscribe as subscribe

broker = 'sadpc009'
client_id = f'VHS_Test_{str(datetime.now()).replace(" ", "_")}'
process = 'vacpc021'
actor = 'Vhs'
nof_channels = 256
plot_temperature_only = False
temperature_range = [10, 40]
topic_prefix = f'{process}/{actor}'
status_color = {
    0: '0.0',
    1: 'cyan',
    2: 'darkblue',
    3: 'darkgreen',
    4: 'lightgreen',
    5: 'yellowgreen',
    6: 'darkred',
    7: 'red',
    8: '0.8',
    9: 'magenta'}
# msg = subscribe.simple("paho/test/simple", hostname="mqtt.eclipseprojects.io")
# print("%s %s" % (msg.topic, msg.payload))
while True:
    try:
        t0 = time.time()
        # Read temperatures
        msg = subscribe.simple(f'{topic_prefix}/Temperatures', hostname=broker, client_id=client_id)
        temperatures = np.fromstring(msg.payload[2:len(msg.payload) - 2], dtype=np.float64, count=-1, sep=',')[:]
        # Read set-point temperatures
        if True:
            msg = subscribe.simple(f'{topic_prefix}/SPTemperatures', hostname=broker, client_id=client_id)
            sptemperatures = np.fromstring(msg.payload[2:len(msg.payload) - 2], dtype=np.float64, count=-1, sep=',')
        else:
            sptemperatures = np.zeros(nof_channels, dtype=np.float64)
        # Read temperature differences
        msg = subscribe.simple(f'{topic_prefix}/DTemperatures', hostname=broker, client_id=client_id)
        dtemperatures = np.fromstring(msg.payload[2:len(msg.payload) - 2], dtype=np.float64, count=-1, sep=',')[:]
        # Read temperature gains
        msg = subscribe.simple(f'{topic_prefix}/GTemperatures', hostname=broker, client_id=client_id)
        gtemperatures = np.fromstring(msg.payload[2:len(msg.payload) - 2], dtype=np.float64, count=-1, sep=',')[:]
        # Read PWM ratios
        msg = subscribe.simple(f'{topic_prefix}/PWMRatios', hostname=broker, client_id=client_id)
        pwmratios = np.fromstring(msg.payload[2:len(msg.payload) - 2], dtype=np.float64, count=-1, sep=',')[:]
        # Read status
        msg = subscribe.simple(f'{topic_prefix}/Status', hostname=broker, client_id=client_id)
        status = np.fromstring(msg.payload[1:len(msg.payload) - 1], dtype=np.int16, count=-1, sep=',')[:]
        # print(status)
        color = []
        for s in status:
            # print(s)
            color.append(status_color[s])
        # print('Color:', color)
    
        # Generate values for x-axis
        channels = np.arange(len(temperatures))
        if plot_temperature_only:  # Plot temperature data
            plt.title(f'{process} Temperatures {datetime.now():%Y-%m-%d %H:%M:%S%z}')
            plt.xlabel('Channel')
            plt.ylabel('T / degC')
            plt.ylim(temperature_range)
            plt.bar(channels, temperatures, 0.9, label='T', color=color)
            plt.bar(channels, sptemperatures, 0.2, label='Set')
            plt.show()
        else:
            fig = plt.figure()
            fig.suptitle(f'{process} {datetime.now():%Y-%m-%d %H:%M:%S%z}')
            plt.subplot(221)
            plt.title('Temperatures')
            plt.xlabel('Channel')
            plt.ylabel('T / degC')
            # plt.ylim([-10, 100])
            plt.bar(channels, temperatures, 0.9, label='T', color=color)
            plt.bar(channels, sptemperatures, 0.2, label='Set')
            plt.legend(loc='best')
            plt.subplot(222)
            plt.title('Temperature Gains')
            plt.xlabel('Channel')
            plt.ylabel('T Gain / degC/s')
            plt.bar(channels, gtemperatures, 0.9, label='T-Gain')
            plt.legend(loc='best')
            plt.subplot(223)
            plt.title('Temperature Differences')
            plt.xlabel('Channel')
            plt.ylabel('T Diff/ degC')
            # plt.ylim([-55, 55])
            plt.bar(channels, dtemperatures, 0.9, label='T-Delta')
            plt.legend(loc='best')
            plt.subplot(224)
            plt.title('Temperature PWM Ratios')
            plt.xlabel('Channel')
            plt.ylabel('PWM Ratio')
            plt.ylim([0, 110])
            plt.bar(channels, pwmratios, 0.9, label='Ratio')
            plt.legend(loc='best')
            plt.tight_layout()
            plt.show()
        print('Iteration time:', time.time() - t0)
        time.sleep(10)
    except KeyboardInterrupt:
        break



