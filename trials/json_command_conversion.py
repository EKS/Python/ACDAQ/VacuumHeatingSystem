# -*- coding: utf-8 -*-
"""
Created on Tue Mar  7 13:58:11 2023

@author: brand
"""
import json
import numpy as np

if True:
    if True:
        msg = "[[false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false], [false]]"
    else:
        msg = "[[0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0]]"
    value = json.loads(msg)
    print(type(value), value)
    v = np.array(value)
    print(type(v), v)
else:
    if True:
        channel_map = {'channel': 1, 'state': True}
    else:
        channel_map = {'channel': 1, 'ratio': 1.234}
    msg_channel = json.dumps(channel_map)
    print(msg_channel)
    value = json.loads(msg_channel)
    print(type(value), value)
