#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Apr 22 17:03:54 2023

@author: H.Brand@gsi.de

https://gist.github.com/mdonkers/63e115cc0c79b4f6b8b3a6b797e485c7
License: MIT License
Copyright (c) 2023 Miel Donkers

Very simple HTTP server in python for parsing http requests to MQTT broker.
"""
import argparse
import getpass
from http.server import BaseHTTPRequestHandler, HTTPServer
import json
import logging
import os
import paho.mqtt.publish as publish
import platform
import re

http_request_examples = "\n\n\
Syntax for Browser-URL: http://host:8080/Command?P1=123&P2=2.34\n\
Type in Browser-URL:\n\
    - http://localhost:8080/Request_State_Change?request=[request_control|release_control|start_profile|stop_profile|pause_profile|continue_profile|cooling_controlled|cooling_manual|controlled_cooling|manual_cooling|profile_cooling|pause_cooling|request_stop]\n\
    - http://localhost:8080/Set-Profiles?filename=D:\Brand\Python\VacuumHeatingSystem\profiles\Beispiel_config_ESR_Sued_GS_50Grad_short.csv\n\
    - http://localhost:8080/Set-ChannelEnabled?channel=0&enabled=false\n\
    - http://localhost:8080/Set-ChannelForced?channel=0&forced=false\n\
    - http://localhost:8080/Set-ChannelPWMRatio?channel=0&ratio=0.\n\
    - http://localhost:8080/Set-ChannelSPTemperature?channel=0&sptemperature=0."

http_port = 8080
mqtt_client_id = "HTTP_Request_Handler_" + platform.node()
# mqtt_broker_default = "sadpc009"
topic_prefix = platform.node() + '/Vhs/'


class S(BaseHTTPRequestHandler):
    def _set_response(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        logging.info("GET request,\nPath: %s\nHeaders:\n%s\n", str(self.path), str(self.headers))
        return_string = self.handle_command(self.path)
        if len(return_string) > 0:
            self._set_response()
            self.wfile.write("GET request for {} not accepted. Exception: {} ".format(self.path, return_string).encode('utf-8'))
        else:
            self._set_response()
            self.wfile.write("GET request for {} accepted.".format(self.path).encode('utf-8'))

    def do_POST(self):
        content_length = int(self.headers['Content-Length'])  # <--- Gets the size of data
        post_data = self.rfile.read(content_length)  # <--- Gets the data itself
        logging.info("POST request,\nPath: %s\nHeaders:\n%s\n\nBody:\n%s\n", str(self.path), str(self.headers), post_data.decode('utf-8'))
        self._set_response()
        self.wfile.write("POST request for {}".format(self.path).encode('utf-8'))

    def handle_command(self, path):
        logging.info("Handle command: Path: %s\n", str(path)[1:])
        try:
            parts = str(path)[1:].split('?')
            logging.info("Handle command: Parts: %s\n", str(parts))
            command = parts[0]
            parameters = parts[1].split('&')
            logging.info("Handle command: Command: %s", command)
            for parameter in parameters:
                name_value = parameter.split('=')
                p_name = name_value[0]
                p_value = name_value[1]
                logging.info("Handle command: Parameter: %s = %s", p_name, p_value)
            match command:
                case 'Set-ChannelEnabled':
                    if len(parameters) == 2:
                        try:
                            p0_name_value = parameters[0].split('=')
                            p0_name = p0_name_value[0]
                            p0_value = p0_name_value[1]
                            p1_name_value = parameters[1].split('=')
                            p1_name = p1_name_value[0]
                            p1_value = p1_name_value[1]
                            if re.match('true', p1_value.lower()):
                                enabled = True
                            else:
                                enabled = False
                            topic = topic_prefix + command
                            topic_value = {'channel': int(p0_value), 'enabled': enabled}
                            # logging.info("Handle command: Publishing: ", str(topic_value))
                            publish.single(topic, json.dumps(topic_value), qos=2, hostname=mqtt_broker, client_id=mqtt_client_id)
                            return ''
                        except Exception as e:
                            return str(e)
                    else:
                        return 'Unexpected number of parameters.'
                case 'Set-ChannelForced':
                    if len(parameters) == 2:
                        try:
                            p0_name_value = parameters[0].split('=')
                            p0_name = p0_name_value[0]
                            p0_value = p0_name_value[1]
                            p1_name_value = parameters[1].split('=')
                            p1_name = p1_name_value[0]
                            p1_value = p1_name_value[1]
                            if re.match('true', p1_value.lower()):
                                forced = True
                            else:
                                forced = False
                            topic = topic_prefix + command
                            topic_value = {'channel': int(p0_value), 'forced': forced}
                            # logging.info("Handle command: Publishing: ", str(topic_value))
                            publish.single(topic, json.dumps(topic_value), qos=2, hostname=mqtt_broker, client_id=mqtt_client_id)
                            return ''
                        except Exception as e:
                            return str(e)
                    else:
                        return 'Unexpected number of parameters.'
                case 'Set-ChannelPWMRatio':
                    if len(parameters) == 2:
                        try:
                            p0_name_value = parameters[0].split('=')
                            p0_name = p0_name_value[0]
                            p0_value = p0_name_value[1]
                            p1_name_value = parameters[1].split('=')
                            p1_name = p1_name_value[0]
                            p1_value = p1_name_value[1]
                            topic = topic_prefix + command
                            topic_value = {'channel': int(p0_value), 'ratio': float(p1_value)}
                            # logging.info("Handle command: Publishing: ", str(topic_value))
                            publish.single(topic, json.dumps(topic_value), qos=2, hostname=mqtt_broker, client_id=mqtt_client_id)
                            return ''
                        except Exception as e:
                            return str(e)
                    else:
                        return 'Unexpected number of parameters.'
                case 'Set-ChannelSPTemperature':
                    if len(parameters) == 2:
                        try:
                            p0_name_value = parameters[0].split('=')
                            p0_name = p0_name_value[0]
                            p0_value = p0_name_value[1]
                            p1_name_value = parameters[1].split('=')
                            p1_name = p1_name_value[0]
                            p1_value = p1_name_value[1]
                            topic = topic_prefix + command
                            topic_value = {'channel': int(p0_value), 'sptemperature': float(p1_value)}
                            # logging.info("Handle command: Publishing: ", str(topic_value))
                            publish.single(topic, json.dumps(topic_value), qos=2, hostname=mqtt_broker, client_id=mqtt_client_id)
                            return ''
                        except Exception as e:
                            return str(e)
                    else:
                        return 'Unexpected number of parameters.'
                case 'Request_State_Change':
                    if len(parameters) == 1:
                        try:
                            allowed_requests = {
                                'request_control',
                                'release_control',
                                'start_profile',
                                'stop_profile',
                                'pause_profile',
                                'continue_profile',
                                'controlled_cooling',
                                'profile_cooling',
                                'pause_cooling',
                                'cooling_controlled',
                                'cooling_manual',
                                'manual_cooling'}
                            name_value = parameters[0].split('=')
                            p_name = name_value[0]
                            if re.match('request', p_name.lower()):
                                p_value = name_value[1]
                                if p_value in allowed_requests:
                                    topic = topic_prefix + command
                                    logging.info("Handle command: Publishing: %s = %s", p_name, p_value)
                                    publish.single(topic, p_value, qos=2, hostname=mqtt_broker, client_id=mqtt_client_id)
                                    return ''
                                else:
                                    return 'Request ist not allowed.'
                            else:
                                return 'Parameter name request not recognized.'
                        except Exception as e:
                            return str(e)
                    else:
                        return 'Unexpected number of parameters.'
                case 'Set-Profiles':
                    if len(parameters) == 1:
                        try:
                            name_value = parameters[0].split('=')
                            p_name = name_value[0]
                            p_value = name_value[1]
                            print('Command: ', command, ' p_name = ', p_name, ' value = ', p_value)
                            if re.match('filename', p_name.lower()):
                                if os.path.exists(p_value):
                                    topic = topic_prefix + command
                                    logging.info("Handle command: Publishing: %s = %s", p_name, p_value)
                                    publish.single(topic, p_value, qos=2, hostname=mqtt_broker, client_id=mqtt_client_id)
                                    return ''
                                else:
                                    return 'File does not exist.' + p_value
                            else:
                                return 'Parameter name not recognized.'
                        except Exception as e:
                            return str(e)
                    else:
                        return 'Unexpected number of parameters.'
                case _:
                    return 'Request not implemented.'
        except Exception as e:
            return str(e)


def run(server_class=HTTPServer, handler_class=S, port=http_port):
    logging.basicConfig(level=logging.INFO)
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    logging.info('Starting httpd...\n')
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    logging.info('Stopping httpd...\n')


if __name__ == '__main__':
    print("vhs_http_server.py (Version 0.0.1.0\n\
Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH\n\
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany\n\
eMail: H.Brand@gsi.de\n\
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme")
    parser = argparse.ArgumentParser(prog='vhs_http_server.py', description='VHS HTTP Server parsing http requests to MQTT broker.', epilog=http_request_examples)
    parser.add_argument('-p', '--port', dest='http_port', default=8080, type=int, help='HTTP Server Listener Port')
    parser.add_argument('-b', '--broker', dest='broker', default='sadpc009', help='MQTT Broker Node (Name or IP)')
    parser.add_argument('-u', '--user', dest='user', default=None, help='MQTT Broker User Name')
    parser.add_argument('--password', dest='password', default=None, help='MQTT Broker Password')
    args = parser.parse_args()
    # set global variables used by class S(BaseHTTPRequestHandler)
    http_port = args.http_port
    mqtt_broker = args.broker
    mqtt_user = args.user
    mqtt_password = args.password
    """
    if mqtt_password is None:  # but required
        mqtt_password = getpass.getpass()
        if len(mqtt_password) == 0:
            mqtt_password = None
    """
    print(f'\r\nhttp_port = {http_port}\r\nmqtt_broker = {mqtt_broker}\r\nmqtt_client_id = {mqtt_client_id}\r\ntopic_prefix = {topic_prefix}\r\n')
    run(port=http_port)
