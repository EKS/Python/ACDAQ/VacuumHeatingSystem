#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Derived class of Actor in pyacdaq performing the vacuum heating control.

Lizenziert unter EUPL V. 1.2

Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme

- Known issues
  - Multiprocessing does not work when called from shell, but spyder only.
"""
import argparse
from datetime import datetime
import getpass
import logging
import json
import multiprocessing as mp
import numpy as np
import os
import pandas as pd
import platform
import re
import time
from pyacdaq.logger import configure_logging_handler
from pyacdaq.actor.actor import PeriodicCallReason as PeriodicCallReason
from pyacdaq.actor.actor import Actor as Actor
from pyacdaq.actor.mqtt import Mqtt as Mqtt
from daqmx.daqmx_ai import DAQmxAi as DAQmxAi
from daqmx.daqmx_do_pwm import DAQmxDoPwm as DAQmxDoPwm
from fuzzy.fuzzy_controller import FuzzyController as FuzzyController
from vhs_sm import VhsSm
from vhs_cli import VhsCli

vhs_logger = logging.getLogger('vhs')


class Vhs(Actor):
    """Derived class performing the heat control."""
    status = {
        'unknown': 0,
        'super_cooled': 1,
        'under_cooled': 2,
        'in_range': 3,
        'heating': 4,
        'forced': 5,
        'over_heated': 6,
        'super_heated': 7,
        'broken': 8,
        'short_cut': 9}

    def __init__(self, name,
                 start_periodic=False,
                 mqtt=None,
                 expand=True,
                 profile_config_filename=None,
                 fuzzy_interference_mode=None,
                 fuzzy_mp=False
                 ):
        """Initialize instance."""
        super().__init__(name, start_periodic, mqtt)
        self._nof_channels = 256
        self._nof_modules = 4
        self._nof_channels_per_module = int(self._nof_channels / self._nof_modules)
        self._samples_per_channel=500
        self._sampling_freq=1000
        self._fuzzy_mp = fuzzy_mp
        if not fuzzy_interference_mode:
            self._fuzzy_interference_mode = fuzzy_interference_mode
        else:
            self._fuzzy_interference_mode = 'mamdani'
        self._expand = expand
        self._profile_config_file = profile_config_filename
        self._scaling_ai2t = np.float64(33.33333)
        self._vhs_subscriptions = ('Set-DO', 'Set-Enabled', 'Set-Forced', 'Set-PWMRatios', 'Set-SPTemperatures', 'Set-ChannelDO', 'Set-ChannelEnabled', 'Set-ChannelForced', 'Set-ChannelPWMRatio', 'Set-ChannelSPTemperature', 'Set-Profiles', 'Request_State_Change')
        self._t_ai = None
        self._t_ai_previous = None
        self._t_start_profile = None
        self._t_start_pause = None
        self._t_start_cooldown = None
        self._cooldown_length = 0.  # Unit: s
        self._cooldown_duration = 0.  # Unit: s
        self._cooldown_left = 0.  # Unit: s
        self._profile_length = 0.  # Unit: s
        self._profile_duration = 0.  # Unit: s
        self._profile_pause = 0.  # Unit: s
        self._profile_left = 0.  # Unit: s
        self._sm = None
        self._AI_0 = None
        self._AI_1 = None
        self._AI_2 = None
        self._AI_3 = None
        self._AI = [self._AI_0, self._AI_1, self._AI_2, self._AI_3]
        self._DOPWM_0 = None
        self._DOPWM_1 = None
        self._DOPWM_2 = None
        self._DOPWM_3 = None
        self._DOPWM = [self._DOPWM_0, self._DOPWM_1, self._DOPWM_2, self._DOPWM_3]
        self._FC_0 = None
        self._FC_1 = None
        self._FC_2 = None
        self._FC_3 = None
        self._FC = [self._FC_0, self._FC_1, self._FC_2, self._FC_3]
        self._cooldown_sign = np.ones(self._nof_channels, dtype=np.float64)
        self._ramp_sign_1 = np.ones(self._nof_channels, dtype=np.float64)
        self._ramp_sign_2 = np.ones(self._nof_channels, dtype=np.float64)
        self._ramp_sign_3 = np.ones(self._nof_channels, dtype=np.float64)
        self._previous_temperatures = np.zeros(self._nof_channels, dtype=np.float64)
        self._temperatures = np.zeros(self._nof_channels, dtype=np.float64)   # Unit: °C
        self._dtemperatures = np.zeros(self._nof_channels, dtype=np.float64)  # Unit: °C
        self._gtemperatures = np.zeros(self._nof_channels, dtype=np.float64)  # Unit: °C/s
        self._sptemperatures = np.zeros(self._nof_channels, dtype=np.float64)  # Unit: °C
        self._ratios = np.zeros(self._nof_channels, dtype=np.float64)   # Unit: %
        self._do = np.zeros(self._nof_channels, dtype=bool)
        self._enabled = np.zeros(self._nof_channels, dtype=bool)
        self._forced = np.zeros(self._nof_channels, dtype=bool)
        self._heating = np.zeros(self._nof_channels, dtype=bool)
        self._status = np.full(self._nof_channels, Vhs.status['unknown'], dtype=np.int_)
        self._f_AI = [None, None, None, None]
        self._f_FC = [None, None, None, None]
        self._ramps = None
        self._ramp_dt_0 = None
        self._ramp_dt_1 = None
        self._ramp_dt_2 = None
        self._ramp_dt_3 = None
        self._ramp_dt_4 = None
        self._default_stop_temperature = 20.  # Unit: °C
        self._temperatures_soc = np.full(self._nof_channels, float('-inf'), dtype=np.float64)   # Unit: °C soc: Start of Cooling
        self._temperatures_eoc = np.full(self._nof_channels, float('-inf'), dtype=np.float64)   # Unit: °C soc: End of Cooling
        self._gtemperatures_cooling = np.full(self._nof_channels, 20. / 3600., dtype=np.float64)  # Unit: °C/s
        self._cooldown_lengths = np.zeros(self._nof_channels, dtype=np.float64)  # Unit: s
        pass

    def __del__(self):
        """Finalize instance."""
        super().__del__()
        pass

    # Private methods
    def __a_e(self, ii):
        a = ii * 64
        e = (ii + 1) * 64
        return a, e

    def __publish_topics(self):
        # vhs_logger.debug(self._name + ' publish_topics(). Publish all topics')
        try:
            if self._mqtt is not None:
                # vhs_logger.debug(self._name + ' Vhs _on_connect() Publishing own topics.')
                self._mqtt.publish_topic(self._name + '/Temperatures', self._temperatures, qos=0, retain=True, expand=self._expand)
                self._mqtt.publish_topic(self._name + '/DTemperatures', self._dtemperatures, qos=0, retain=True, expand=self._expand)
                self._mqtt.publish_topic(self._name + '/GTemperatures', self._gtemperatures, qos=0, retain=True, expand=self._expand)
                self._mqtt.publish_topic(self._name + '/SPTemperatures', self._sptemperatures, qos=0, retain=True, expand=self._expand)
                self._mqtt.publish_topic(self._name + '/PWMRatios', self._ratios, qos=0, retain=True, expand=self._expand)
                self._mqtt.publish_topic(self._name + '/Enabled', self._enabled, qos=0, retain=True, expand=self._expand)
                self._mqtt.publish_topic(self._name + '/Forced', self._forced, qos=0, retain=True, expand=self._expand)
                self._mqtt.publish_topic(self._name + '/Heating', self._heating, qos=0, retain=True, expand=self._expand)
                self._mqtt.publish_topic(self._name + '/Status', self._status, qos=0, retain=True, expand=self._expand)
                self._mqtt.publish_topic(self._name + '/ProfileLength', self._profile_length, qos=0, retain=True, expand=False)
                self._mqtt.publish_topic(self._name + '/ProfileDuration', self._profile_duration, qos=0, retain=True, expand=False)
                self._mqtt.publish_topic(self._name + '/ProfilePause', self._profile_pause, qos=0, retain=True, expand=False)
                self._mqtt.publish_topic(self._name + '/ProfileLeft', self._profile_left, qos=0, retain=True, expand=False)
                self._mqtt.publish_topic(self._name + '/CooldownLength', np.nanmax(self._cooldown_length), qos=0, retain=True, expand=False)
                self._mqtt.publish_topic(self._name + '/CooldownDuration', self._cooldown_duration, qos=0, retain=True, expand=False)
                self._mqtt.publish_topic(self._name + '/CooldownLeft', self._cooldown_left, qos=0, retain=True, expand=False)
                if not self._t_start_profile:
                    self._mqtt.publish_topic(self._name + '/ProfileStartTime', 'NA', qos=0, retain=True, expand=False)
                else:
                    self._mqtt.publish_topic(self._name + '/ProfileStartTime', self._t_start_profile.ctime(), qos=0, retain=True, expand=False)
                if not self._t_start_cooldown:
                    self._mqtt.publish_topic(self._name + '/CooldownStartTime', 'NA', qos=0, retain=True, expand=False)
                else:
                    self._mqtt.publish_topic(self._name + '/CooldownStartTime', self._t_start_cooldown.ctime(), qos=0, retain=True, expand=False)
        except Exception as e:
            vhs_logger.exception(self._name + ' Vhs _on_connect(): Exception cought:')
            vhs_logger.exception(self._name + ' Vhs _on_connect(): Exception: ' + str(e))
        pass

    # Protected override methods from super Actor
    def _on_connect(self):
        """
        Called from _on_receive() when message was received from paho.mqtt.client callback message on_connect.

        Perform action on MQTT connect here, e.g. publish and subscribe topics.

        Returns
        -------
        None.

        """
        # vhs_logger.debug(self._name + ' Vhs _on_connect() Received MQTT connected message. Publish and subscribe...')
        try:
            self.__publish_topics()
            if self._mqtt is not None:
                self._mqtt.publish_topic(self._name + '/Profiles', self._profile_config_file, qos=0, retain=True, expand=False)
                if not self._t_start_profile:
                    self._mqtt.publish_topic(self._name + '/ProfileStartTime', 'NA', qos=0, retain=True, expand=False)
                else:
                    self._mqtt.publish_topic(self._name + '/ProfileStartTime', self._t_start_profile.ctime(), qos=0, retain=True, expand=False)
                self._mqtt.publish_topic(self._name + '/State', self._sm.current_state.id, qos=0, retain=True, expand=False)
                self.__publish_topics()
                # vhs_logger.debug(self._name + ' Vhs _on_connect() Publishing own topics.')
                # vhs_logger.debug(self._name + ' Vhs _on_connect() Publishing:' + str(self._vhs_subscriptions))
                prefixed_topics = ()
                for topic in self._vhs_subscriptions:
                    prefixed_topic = self._name + '/' + topic
                    # vhs_logger.debug(self._name + ' Vhs _on_connect() Publishing:' + str(prefixed_topic))
                    prefixed_topics = prefixed_topics + (prefixed_topic,)
                    self._mqtt.publish_topic(prefixed_topic, '', qos=0, retain=True, expand=False)
                # vhs_logger.debug(self._name + ' Vhs _on_connect() Subscribing to: ' + str(prefixed_topics))
                rcs_mids = self._mqtt.subscribe_topics(self._in_future, prefixed_topics, qos=0).get()
                # vhs_logger.debug(self._name + ' Vhs _on_connect() Subscribing to: ' + str(prefixed_topics) + ' returned rcs, mids = ' + str(rcs_mids))
        except Exception as e:
            vhs_logger.exception(self._name + ' Vhs _on_connect(): Exception cought:')
            vhs_logger.exception(self._name + ' Vhs _on_connect(): Exception: ' + str(e))
            pass
        super()._on_connect()
        pass

    def _on_message(self, topic, msg):
        """
        Called from Mqtt._on_receive() when message was received from paho.mqtt.client callback message on_message.

        Perform action on MQTT messages here or forward to super.

        Returns
        -------
        None.

        """
        vhs_logger.debug(self._name + ' Received MQTT topic:' + topic + ' msg: ' + str(msg))
        sub_topics = re.split('/', topic)
        # vhs_logger.debug(self._name + ' Received MQTT sub-topics: ', sub_topics, 'msg:', msg)
        match sub_topics[2]:
            case 'Request_State_Change':
                try:
                    if len(msg) > 2:
                        vhs_logger.info(self._name + ' Vhs _on_message() Request_State_Change received, msg=' + str(msg.strip()))
                        self.request_state_change(msg.strip())
                    else:
                        vhs_logger.debug(self._name + ' Vhs _on_message() Request_State_Change ignored. len(msg)=' + str(len(msg)))
                        pass
                except Exception as e:
                    vhs_logger.debug(self._name + ' _Vhs on_message() Request_State_Change ignored due to exception: ' + str(e))
                    pass
            case 'Set-ChannelDO':
                try:
                    if len(msg) > 2:
                        vhs_logger.info(self._name + ' Vhs _on_message() Set-ChannelDO received, msg=' + str(msg.strip()))
                        value = json.loads(msg)
                        channel = int(value['channel'])
                        state = value['state']
                        self.setChannelDO(channel, state)
                    else:
                        vhs_logger.debug(self._name + ' Vhs _on_message() Set-ChannelDO ignored. len(msg)=' + str(len(msg)))
                        pass
                except Exception as e:
                    vhs_logger.debug(self._name + ' Vhs _on_message() Set-ChannelDO ignored due to exception: ' + str(e))
                    pass
            case 'Set-ChannelEnabled':
                try:
                    if len(msg) > 2:
                        vhs_logger.info(self._name + ' Vhs _on_message() Set-ChannelEnabled received, msg=' + str(msg.strip()))
                        value = json.loads(msg)
                        channel = int(value['channel'])
                        enabled = value['enabled']
                        self.setChannelEnabled(channel, enabled)
                    else:
                        vhs_logger.debug(self._name + ' Vhs _on_message() Set-ChannelEnabled ignored. len(msg)=' + str(len(msg)))
                        pass
                except Exception as e:
                    vhs_logger.debug(self._name + ' Vhs _on_message() Set-ChannelEnabled ignored due to exception: ' + str(e))
                    pass
            case 'Set-ChannelForced':
                try:
                    if len(msg) > 2:
                        vhs_logger.info(self._name + ' Vhs _on_message() Set-ChannelForced received, msg=' + str(msg.strip()))
                        value = json.loads(msg)
                        channel = int(value['channel'])
                        forced = value['forced']
                        self.setChannelForced(channel, forced)
                    else:
                        vhs_logger.debug(self._name + ' Vhs _on_message() Set-ChannelForced ignored. len(msg)=' + str(len(msg)))
                        pass
                except Exception as e:
                    vhs_logger.debug(self._name + ' Vhs _on_message() Set-ChannelForced ignored due to exception: ' + str(e))
                    pass
            case 'Set-ChannelPWMRatio':
                try:
                    if len(msg) > 2:
                        vhs_logger.info(self._name + ' Vhs _on_message() Set-ChannelPWMRatio received, msg=' + str(msg.strip()))
                        value = json.loads(msg)
                        channel = int(value['channel'])
                        ratio = float(value['ratio'])
                        self.setChannelPWMRatio(channel, ratio)
                    else:
                        vhs_logger.debug(self._name + ' Vhs _on_message() Set-ChannelPWMRatio ignored. len(msg)=' + str(len(msg)))
                        pass
                except Exception as e:
                    vhs_logger.debug(self._name + ' Vhs _on_message() Set-ChannelPWMRatio ignored due to exception: ' + str(e))
                    pass
            case 'Set-ChannelSPTemperature':
                try:
                    if len(msg) > 2:
                        vhs_logger.info(self._name + ' Vhs _on_message() Set-ChannelSPTemperature received, msg=' + str(msg.strip()))
                        value = json.loads(msg)
                        channel = int(value['channel'])
                        sptemperature = float(value['sptemperature'])
                        self.setChannelSPTemperature(channel, sptemperature)
                    else:
                        vhs_logger.debug(self._name + ' Vhs _on_message() Set-ChannelSPTemperature ignored. len(msg)=' + str(len(msg)))
                        pass
                except Exception as e:
                    vhs_logger.debug(self._name + ' Vhs _on_message() Set-ChannelSPTemperature ignored due to exception: ' + str(e))
                    pass
            case 'Set-DO':
                try:
                    if len(msg) > 2:
                        vhs_logger.info(self._name + ' Vhs _on_message() Set-DO received, msg=' + str(msg.strip()))
                        value = np.array(json.loads(msg))
                        self.setDO(value)
                    else:
                        vhs_logger.debug(self._name + ' Vhs _on_message() Set-DO ignored. len(msg)=' + str(len(msg)))
                        pass
                except Exception as e:
                    vhs_logger.debug(self._name + ' Vhs _on_message() Set-DO ignored due to exception: ' + str(e))
                    pass
            case 'Set-Enabled':
                try:
                    if len(msg) > 2:
                        vhs_logger.info(self._name + ' Vhs _on_message() Set-Enabled received, msg=' + str(msg.strip()))
                        value = np.array(json.loads(msg))
                        self.setEnabled(value)
                    else:
                        vhs_logger.debug(self._name + ' Vhs _on_message() Set-Enabled ignored. len(msg)=' + str(len(msg)))
                        pass
                except Exception as e:
                    vhs_logger.debug(self._name + ' Vhs _on_message() Set-Enabled ignored due to exception: ' + str(e))
                    pass
            case 'Set-Forced':
                try:
                    if len(msg) > 2:
                        vhs_logger.info(self._name + ' Vhs _on_message() Set-Forced received, msg=' + str(msg.strip()))
                        value = np.array(json.loads(msg))
                        self.setForced(value)
                    else:
                        vhs_logger.debug(self._name + ' Vhs _on_message() Set-Forced ignored. len(msg)=' + str(len(msg)))
                        pass
                except Exception as e:
                    vhs_logger.debug(self._name + ' Vhs _on_message() Set-Enabled ignored due to exception: ' + str(e))
                    pass
            case 'Set-PWMRatios':
                try:
                    if len(msg) > 2:
                        vhs_logger.info(self._name + ' Vhs _on_message() Set-PWMRatio received, msg=' + str(msg.strip()))
                        value = np.array(json.loads(msg))
                        self.setPWMRatios(value)
                    else:
                        vhs_logger.debug(self._name + ' Vhs _on_message() Set-PWMRatios ignored. len(msg)=' + str(len(msg)))
                        pass
                except Exception as e:
                    vhs_logger.debug(self._name + ' Vhs _on_message() Set-PWMRatio ignored due to exception: ' + str(e))
                    pass
            case 'Set-SPTemperatures':
                try:
                    if len(msg) > 2:
                        vhs_logger.info(self._name + ' Vhs _on_message() Set-SPTemperatures received, msg=' + str(msg.strip()))
                        value = np.array(json.loads(msg))
                        self.setSPTemperatures(value)
                    else:
                        vhs_logger.debug(self._name + ' Vhs _on_message() Set-SPTemperatures ignored. len(msg)=' + str(len(msg)))
                        pass
                except Exception as e:
                    vhs_logger.debug(self._name + ' Vhs _on_message() Set-PWMRatio ignored due to exception: ' + str(e))
                    pass
            case 'Set-Profiles':
                try:
                    if len(msg) > 2:
                        vhs_logger.info(self._name + ' Vhs _on_message() Set-Profiles received, msg=' + str(msg.strip()))
                        self.setProfiles(str(msg.strip()))
                    else:
                        vhs_logger.debug(self._name + ' Vhs _on_message() Set-Profiles ignored. len(msg)=' + str(len(msg)))
                        pass
                except Exception as e:
                    vhs_logger.debug(self._name + ' _Vhs on_message() Set-Profile ignored due to exception: ' + str(e))
                    pass
            case _:  # Call super
                vhs_logger.debug(self._name + ' Vhs _on_message() Calling super() due for unknown topic: ' + topic + ' msg: ' + str(msg))
                super()._on_message(topic, msg)
        pass

    def _on_receive(self, message):
        """
        May be implemented for the actor to handle regular non-proxy messages.

        Virtual protected method.
        Override with concrete child class implementation.

        Returns
        -------
        None.

        """
        # warnings.warn(self._name + ' VHS _on_receive() Method needs to be implemented by derived class.')
        # super()._on_receive(message)
        print(f'{self._name} _on_receive: {message}')
        try:
            command = message.split(' ')
            match command[0]:
                case 'enable':  # channel 0/1
                    self.setChannelEnabled(int(command[1]), int(command[2]) > 0)
                    pass
                case 'force':  # channel 0/1
                    self.setChannelForced(int(command[1]), int(command[2]) > 0)
                    pass
                case 'profile':  # absolute path
                    self.setProfiles(command[1])
                    pass
                case 'pwmr':  # ratio/%
                    self.setChannelPWMRatio(int(command[1]), float(command[2]) > 0)
                    pass
                case 'spt':  # T/°C
                    self.setChannelSPTemperature(int(command[1]), float(command[2]) > 0)
                    pass
                case 'rt':  # rt transition: Request state transition. Allowed transitions:
                    if len(command[1]) > 2:
                        self.request_state_change(command[1])
                    else:
                        print(self._name + ' Vhs _on_receive() Request_State_Change ignored.')
                    pass
                case _:
                    print(f'Unknown command received: {message}')
        except Exception as e:
            vhs_logger.warning(self._name + ' _Vhs on_receive() Exception handling message: ' + message + ' \r\n' + str(e))
        pass

    def _on_start(self):
        """
        Hook for doing any setup that should be done after the actor is started, but before it starts processing messages.

        Virtual protected method.
        Override with concrete child class implementation.

        Returns
        -------
        None.

        """
        # warnings.warn(self._name + ' VHS _on_start() Method needs to be implemented by derived class.')
        # super()._on_start()
        # Launch nested actors
        self._DOPWM_0 = DAQmxDoPwm.start('PXI1Slot6',
                                         start_periodic=True,
                                         mqtt=self._mqtt,
                                         expand=False,
                                         resource='/PXI1Slot6',
                                         id_query_flag=True,
                                         reset_flag=False,
                                         reset_options='',
                                         selftest_flag=False,
                                         nof_channels=self._nof_channels_per_module
                                         ).proxy()
        self._DOPWM_1 = DAQmxDoPwm.start('PXI1Slot7',
                                         start_periodic=True,
                                         mqtt=self._mqtt,
                                         expand=False,
                                         resource='/PXI1Slot7',
                                         id_query_flag=True,
                                         reset_flag=False,
                                         reset_options='',
                                         selftest_flag=False,
                                         nof_channels=self._nof_channels_per_module
                                         ).proxy()
        self._DOPWM_2 = DAQmxDoPwm.start('PXI1Slot8',
                                         start_periodic=True,
                                         mqtt=self._mqtt,
                                         expand=False,
                                         resource='/PXI1Slot8',
                                         id_query_flag=True,
                                         reset_flag=False,
                                         reset_options='',
                                         selftest_flag=False,
                                         nof_channels=64
                                         ).proxy()
        self._DOPWM_3 = DAQmxDoPwm.start('PXI1Slot9',
                                         start_periodic=True,
                                         mqtt=self._mqtt,
                                         expand=False,
                                         resource='/PXI1Slot9',
                                         id_query_flag=True,
                                         reset_flag=False,
                                         reset_options='',
                                         selftest_flag=False,
                                         nof_channels=self._nof_channels_per_module
                                         ).proxy()
        self._DOPWM = [self._DOPWM_0, self._DOPWM_1, self._DOPWM_2, self._DOPWM_3]
        if self._fuzzy_mp:
            self._fsq_in = [mp.Queue() for i in range(4)]
            self._fsq_ret = [mp.Queue() for i in range(4)]
        else:
            self._fsq_in = [None, None, None, None]
            self._fsq_ret = [None, None, None, None]
        self._FC_0 = FuzzyController.start('FC0',
                                           start_periodic=False,
                                           mqtt=self._mqtt,
                                           fuzzy_interference_mode=self._fuzzy_interference_mode,
                                           fuzzy_mp=self._fuzzy_mp,
                                           mpq_in=self._fsq_in[0],
                                           mpq_ret=self._fsq_ret[0],
                                           ).proxy()
        # time.sleep(1)
        self._FC_1 = FuzzyController.start('FC1',
                                           start_periodic=False,
                                           mqtt=self._mqtt,
                                           fuzzy_interference_mode=self._fuzzy_interference_mode,
                                           fuzzy_mp=self._fuzzy_mp,
                                           mpq_in=self._fsq_in[1],
                                           mpq_ret=self._fsq_ret[1],
                                           ).proxy()
        # time.sleep(1)
        self._FC_2 = FuzzyController.start('FC2',
                                           start_periodic=False,
                                           mqtt=self._mqtt,
                                           fuzzy_interference_mode=self._fuzzy_interference_mode,
                                           fuzzy_mp=self._fuzzy_mp,
                                           mpq_in=self._fsq_in[2],
                                           mpq_ret=self._fsq_ret[2],
                                           ).proxy()
        # time.sleep(1)
        self._FC_3 = FuzzyController.start('FC3',
                                           start_periodic=False,
                                           mqtt=self._mqtt,
                                           fuzzy_interference_mode=self._fuzzy_interference_mode,
                                           fuzzy_mp=self._fuzzy_mp,
                                           mpq_in=self._fsq_in[3],
                                           mpq_ret=self._fsq_ret[3],
                                           ).proxy()
        # time.sleep(1)
        self._FC = [self._FC_0, self._FC_1, self._FC_2, self._FC_3]
        self._AI_0 = DAQmxAi.start('PXI1Slot2',
                                   start_periodic=True,
                                   mqtt=self._mqtt,
                                   expand=False,
                                   resource='/PXI1Slot2',
                                   id_query_flag=True,
                                   reset_flag=False,
                                   reset_options='',
                                   selftest_flag=False,
                                   nof_channels=self._nof_channels_per_module,
                                   samples_per_channel=self._samples_per_channel,
                                   sampling_freq=self._sampling_freq
                                   ).proxy()
        self._AI_1 = DAQmxAi.start('PXI1Slot3',
                                   start_periodic=True,
                                   mqtt=self._mqtt,
                                   expand=False,
                                   resource='/PXI1Slot3',
                                   id_query_flag=True,
                                   reset_flag=False,
                                   reset_options='',
                                   selftest_flag=False,
                                   nof_channels=self._nof_channels_per_module,
                                   samples_per_channel=self._samples_per_channel,
                                   sampling_freq=self._sampling_freq
                                   ).proxy()
        self._AI_2 = DAQmxAi.start('PXI1Slot4',
                                   start_periodic=True,
                                   mqtt=self._mqtt,
                                   expand=False,
                                   resource='/PXI1Slot4',
                                   id_query_flag=True,
                                   reset_flag=False,
                                   reset_options='',
                                   selftest_flag=False,
                                   nof_channels=self._nof_channels_per_module,
                                   samples_per_channel=self._samples_per_channel,
                                   sampling_freq=self._sampling_freq
                                   ).proxy()
        self._AI_3 = DAQmxAi.start('PXI1Slot5',
                                   start_periodic=True,
                                   expand=False,
                                   mqtt=self._mqtt,
                                   resource='/PXI1Slot5',
                                   id_query_flag=True,
                                   reset_flag=False,
                                   reset_options='',
                                   selftest_flag=False,
                                   nof_channels=self._nof_channels_per_module,
                                   samples_per_channel=self._samples_per_channel,
                                   sampling_freq=self._sampling_freq
                                   ).proxy()
        self._AI = [self._AI_0, self._AI_1, self._AI_2, self._AI_3]
        # Instantiate statemachine
        self._sm = VhsSm(vhs_in_future=self._in_future, nof_channels=self._nof_channels, history=False)
        self.read_profile_configuration(filename=self._profile_config_file)
        self.calc_temperature_setpoints(0.)
        self._in_future.wait_for_fuzzy_processes()
        pass

    def wait_for_fuzzy_processes(self):
        # Wait for fuzzy processes to have started
        vhs_logger.info(f'{self._name} wait_for_fuzzy_processes: Wait for responses from fuzzy processes having started.')
        items = []
        for q in self._fsq_ret:
            try:
                item = q.get(True, 10.)
                # item = q.get()
                items.append(item)
                vhs_logger.info(f'{self._name} wait_for_fuzzy_processes:  got item from fuzzy process: {item}')
            except mp.queues.Empty:
                vhs_logger.error(f'{self._name} wait_for_fuzzy_processes: {q}.get() from fuzzy process exception queue empty caught.')
        if len(items) < 4:
            vhs_logger.error(f'{self._name} wait_for_fuzzy_processes: Not all ({len(items)}) fuzzy processes have been startet. Stop self!')
            self._in_future.stop()
        else:
            vhs_logger.info(f'{self._name} wait_for_fuzzy_processes: All ({len(items)}) fuzzy processes have been startet.')
        pass

    def _on_stop(self):
        """
        Hook for doing any cleanup that should be done after the actor has processed the last message, and before the actor stops.

        Virtual protected method.
        Override with concrete child class implementation.

        Returns
        -------
        None.

        """
        vhs_logger.debug(self._name + ' Vhs _on_stop()')
        # Stop nested actors
        for a in self._AI:
            if a:
                a.stop()
        for a in self._FC:
            if a:
                a.stop()
        for a in self._DOPWM:
            if a:
                a.stop()
        # Queues become closes by nested actors.
        try:
            if self._mqtt is not None:
                prefixed_topics = ()
                # vhs_logger.debug(self._name + ' Vhs _on_stop() Unsubscribing: ' + str(self._vhs_subscriptions))
                for topic in self._vhs_subscriptions:
                    prefixed_topic = self._name + '/' + topic
                    prefixed_topics = prefixed_topics + (prefixed_topic,)
                # vhs_logger.debug(self._name + ' Vhs _on_stop() Unsubscribing from: ' + str(prefixed_topics))
                self._mqtt.unsubscribe_topics(self, prefixed_topics)
        except Exception as e:
            vhs_logger.exception(self._name + ' Vhs _on_stop(): Exception cought:')
            vhs_logger.exception(self._name + ' Vhs _on_stop(): Exception: ' + str(e))
        super()._on_stop()
        pass

    def _periodic(self, reason):
        """
        Implementation of periodic actions.

        Read data from instrument.

        Returns
        -------
        None.

        """
        try:
            match reason:
                case PeriodicCallReason.Start:
                    for ii in range(len(self._AI)):
                        a, e = self.__a_e(ii)
                        ais = self._AI[ii].getAI().get()
                        self._previous_temperatures[a: e] = ais * self._scaling_ai2t
                    self._t_ai_previous = time.time()
                    pass
                case PeriodicCallReason.Do:
                    # print('state: ', self._sm.current_state)
                    match self._sm.current_state_value:
                        case 'manual' | 'controlled':
                            self._t_start_profile = None
                            self._t_start_cooldown = None
                        case 'profile':
                            if self._t_start_profile:
                                self._profile_duration = (datetime.now() - self._t_start_profile).total_seconds() - self._profile_pause
                                self._profile_left = self._profile_length - self._profile_duration
                                self.calc_temperature_setpoints(self._profile_duration)
                        case 'cooling':
                            if self._t_start_cooldown:
                                self._cooldown_duration = (datetime.now() - self._t_start_cooldown).total_seconds()
                                self._cooldown_left = self._cooldown_length - self._cooldown_duration
                                self.calc_cooling_setpoints(self._cooldown_duration)
                        case _:
                            pass
                    # Request analog inputs
                    for ii in range(len(self._AI)):
                        self._f_AI[ii] = self._AI[ii].getAI()
                    # Read analog input values from future ref.
                    for ii in range(len(self._AI)):
                        a, e = self.__a_e(ii)
                        ais = self._f_AI[ii].get()
                        # print('AI=', ais)
                        self._temperatures[a: e] = ais * self._scaling_ai2t
                    self._t_ai = time.time()
                    # print(f'Time interval: {self._t_ai - self._t_ai_previous}s = {(self._t_ai - self._t_ai_previous) / 3600.}h')
                    np.subtract(self._temperatures, self._sptemperatures, self._dtemperatures)
                    np.subtract(self._temperatures, self._previous_temperatures, self._gtemperatures) * 3600. / (self._t_ai - self._t_ai_previous)
                    # print(f'self._temperatures:\n{self._temperatures}')
                    # print(f'self._previous_temperatures:\n{self._previous_temperatures}')
                    # print(f'Equal:\n{self._temperatures == self._previous_temperatures}')
                    # print(f'self._gtemperatures:\n{self._gtemperatures}')
                    self._previous_temperatures = np.copy(self._temperatures)
                    self._t_ai_previous = self._t_ai
                    if not self._sm.manual.is_active:
                        if self._fuzzy_mp:
                            try:
                                # Read PWMRatios from fuzzy controllers via queue.
                                # print(f'{self._name} _periodic: Enqueue request to fuzzy processes.')
                                for ii in range(len(self._AI)):
                                    a, e = self.__a_e(ii)
                                    # self._fsq_in[ii].put({'subset': ii, 'D': self._dtemperatures[a: e], 'G': self._gtemperatures[a: e], 'mode': self._fuzzy_interference_mode}, True, 10.)
                                    self._fsq_in[ii].put({'subset': ii, 'D': self._dtemperatures[a: e], 'G': self._gtemperatures[a: e], 'mode': self._fuzzy_interference_mode}, True, 10.)
                                try:
                                    # print(f'{self._name} _periodic: Wait for responses from fuzzy processes.')
                                    for ii in range(len(self._AI)):
                                        a, e = self.__a_e(ii)
                                        item = self._fsq_ret[ii].get(True, 10.)
                                        # print(f'{self._name} _periodic:  got item from fuzzy processes.')
                                        # print(f'{self._name} periodic: got item from fsq: {item}', flush=True)
                                        self._ratios[a: e] = item['Power']
                                    # print(f'{self._name} _periodic: Responses received from fuzzy processes.')
                                except mp.queues.Empty:
                                    logging.exception(f'{self._name} _periodic: queue.get() from fuzzy process exception queue empty caught.')
                                    print(f'{self._name} _periodic: queue.get() from fuzzy process exception queue empty caught.')
                            except Exception as e:
                                logging.exception(f'{self._name} _periodic: queue.put() to fuzzy process exception caught: {str(e)}')
                                print(f'{self._name} _periodic: queue.put() to fuzzy process exception caught: {str(e)}')
                            pass
                        else:
                            # Request PWMRatios from fuzzy controllers via future.
                            for ii in range(len(self._AI)):
                                a, e = self.__a_e(ii)
                                self._f_FC[ii] = self._FC[ii].calc_powers(self._dtemperatures[a: e], self._gtemperatures[a: e])
                            # Read PWMRatios from fuzzy controllers from future ref.
                            for ii in range(len(self._AI)):
                                a, e = self.__a_e(ii)
                                self._ratios[a: e] = self._f_FC[ii].get()
                    self._check_alarm_conditions()
                    # Set PWMRatios
                    for ii in range(len(self._AI)):
                        a, e = self.__a_e(ii)
                        self._DOPWM[ii].setPWMRatios(self._ratios[a: e] / 100.)
                    self.__publish_topics()
                    # Check for end of profile or cooldown.
                    if self._sm.profile.is_active and self._t_start_profile and self._profile_left <= 0.:
                        self.request_state_change('stop_profile')
                    if self._sm.cooling.is_active and self._t_start_cooldown and self._cooldown_left <= 0.:
                        self.request_state_change('cooling_controlled')
                    pass
                case PeriodicCallReason.Stop:
                    self.setForced(np.zeros(self._nof_channels, dtype=bool))
                    self._status[self._forced] = Vhs.status['forced']
                    self._ratios = np.zeros(self._nof_channels, dtype=np.float64)
                    self._heating = self._ratios > 0.
                    self._status[self._heating] = Vhs.status['heating']
                    for ii in range(len(self._AI)):
                        self._ratios[a: e] = self._f_FC[ii].get()
                        self._DOPWM[ii].setPWMRatios(self._ratios[a: e])
                        # print('PWMRatios =', self._ratios)
                    self.__publish_topics()
                    pass
        except Exception as e:
            vhs_logger.exception(self._name + ' _Vhs _periodic Exception: ' + str(e))
        pass

    # Protected override methods from super Device
    def _sw_revision(self):
        """
        Return revision of this class inclusive parents.

        Returns
        -------
        string
            Software revision.

        """
        return 'Vhs_0.0.0.19|' + super()._sw_revision()

    def _check_alarm_conditions(self):
        # Reset status to unkown then in Range
        self._status = np.full(self._nof_channels, Vhs.status['unknown'], dtype=np.int_)
        self._status[np.logical_and(self._dtemperatures >= -1 * self._ramps['Warnung Unter.'], self._dtemperatures <= self._ramps['Warnung Über.'])] = Vhs.status['in_range']
        # Check limits for temperature and difference (t - spt)
        self._status[self._dtemperatures < -1 * self._ramps['Warnung Unter.']] = Vhs.status['under_cooled']
        self._status[self._dtemperatures < -1 * self._ramps['Unterheizen']] = Vhs.status['super_cooled']
        self._ratios[self._dtemperatures < -1 * self._ramps['Unterheizen']] = 0.
        self._status[self._dtemperatures > self._ramps['Warnung Über.']] = Vhs.status['over_heated']
        self._status[self._dtemperatures > self._ramps['Überheizen']] = Vhs.status['super_heated']
        self._ratios[self._dtemperatures > self._ramps['Überheizen']] = 0.
        self._heating = self._ratios > 0.
        self._status[self._heating] = Vhs.status['heating']
        self._ratios[self._forced] = 100.
        self._status[self._forced] = Vhs.status['forced']
        # Check for sensor fault
        self._ratios[self._temperatures < 0.] = 0.
        self._status[self._temperatures < 0.] = Vhs.status['broken']
        self._ratios[self._temperatures > 310.] = 0.
        self._status[self._temperatures > 310.] = Vhs.status['short_cut']
        pass

    # Public methods of this class
    def calc_cooling_setpoints(self, dt):
        # print(self._name + ' calc_cooling_setpoints: Cooldown calc_cooling_setpoints: dt=', dt)
        if not self._t_start_cooldown:
            print(self._name + '  calc_cooling_setpoints: Cooling parameters not jet calculated, waiting for next iteration.')
        else:
            if dt < 0:
                dt = 0
            for ii in range(self._nof_channels):
                if dt < self._cooldown_lengths[ii]:
                    self._sptemperatures[ii] = self._temperatures_soc[ii] + self._cooldown_sign[ii] * self._gtemperatures_cooling[ii] * dt
                else:
                    self._sptemperatures[ii] = self._ramps.loc[ii, 'Temperatur 3']
            # print(self._name + ' calc_cooling_setpoints: dt=' + str(dt))
            # print(self._name + ' calc_cooling_setpoints: \n' + str(self._sptemperatures))
        pass

    def calc_temperature_setpoints(self, dt):
        if dt < 0:
            dt = 0
        for ii in range(len(self._ramps)):
            if dt < self._ramp_dt_0[ii]:
                self._sptemperatures[ii] = self._ramps.loc[ii, 'Starttemperatur'] + self._ramps.loc[ii, 'Rampe 1'] * dt * self._ramp_sign_1[ii]
            elif dt < self._ramp_dt_1[ii]:
                self._sptemperatures[ii] = self._ramps.loc[ii, 'Temperatur 1']
            elif dt < self._ramp_dt_2[ii]:
                self._sptemperatures[ii] = self._ramps.loc[ii, 'Temperatur 1'] + self._ramps.loc[ii, 'Rampe 2'] * (dt - self._ramp_dt_1[ii]) * self._ramp_sign_2[ii]
            elif dt < self._ramp_dt_3[ii]:
                self._sptemperatures[ii] = self._ramps.loc[ii, 'Temperatur 2']
            elif dt < self._ramp_dt_4[ii]:
                self._sptemperatures[ii] = self._ramps.loc[ii, 'Temperatur 2'] + self._ramps.loc[ii, 'Rampe 3'] * (dt - self._ramp_dt_3[ii]) * self._ramp_sign_3[ii]
            else:
                self._sptemperatures[ii] = self._ramps.loc[ii, 'Starttemperatur']
        # print(self._name + ' calc_temperature_setpoints: \n' + str(self._sptemperatures))
        pass

    def read_profile_configuration(self, filename=None):
        if os.path.exists(filename):
            self._profile_config_file = filename
            vhs_logger.info(self._name + ' read_profile_configuration: ' + filename)
            all_channels = np.linspace(start=0, stop=self._nof_channels - 1, num=self._nof_channels, endpoint=True, dtype=np.int16)
            try:
                ramps = pd.read_csv(self._profile_config_file,
                                    sep=';',
                                    encoding='ISO-8859_2',
                                    header=0,
                                    dtype={0: 'int16', 1: 'float64', 2: 'float64', 3: 'float64', 4: 'float64', 5: 'float64', 6: 'float64', 7: 'float64', 8: 'float64', 9: 'float64', 10: 'float64', 11: 'float64', 12: 'float64', 13: 'float64', 14: 'str'}
                                    )
                default_row = ['-1', False, float('-inf'), float('-inf'), float('-inf'), float('-inf'), float('-inf'), float('-inf'), float('-inf'), float('-inf'), float('-inf'), float('-inf'), float('-inf'), float('-inf'), float('-inf'), 'NA']
                enabled = np.ones(len(ramps.index), dtype=bool)
                ramps.insert(1, 'Enabled', enabled)
                # vhs_logger.debug(self._name + ' read_profile_configuration: ' + str(ramps.columns))
                ramps.sort_values(by=['Channel'], ascending=True, inplace=True)
                existing_channels = ramps['Channel']
                missing_channels = np.delete(all_channels, existing_channels)
                for missing_channel in missing_channels:
                    new_row = default_row
                    new_row[0] = missing_channel
                    ramps.loc[len(ramps)] = new_row
                ramps.sort_values(by=['Channel'], ascending=True, ignore_index=True, inplace=True)
                ramps['Haltezeit 1'] = ramps['Haltezeit 1'] * 60.
                ramps['Haltezeit 2'] = ramps['Haltezeit 2'] * 60.
                ramps['Rampe 1'] = ramps['Rampe 1'] / 3600.
                ramps['Rampe 2'] = ramps['Rampe 2'] / 3600.
                ramps['Rampe 3'] = ramps['Rampe 3'] / 3600.
                self._ramp_sign_1 = np.sign((ramps['Temperatur 1'] - ramps['Starttemperatur']).to_numpy())
                self._ramp_sign_2 = np.sign((ramps['Temperatur 2'] - ramps['Temperatur 1']).to_numpy())
                self._ramp_sign_3 = np.sign((ramps['Temperatur 3'] - ramps['Temperatur 2']).to_numpy())
                # Times and temperature offsets need to be checked.
                self._ramp_dt_0 = tr1 = abs(ramps['Temperatur 1'] - ramps['Starttemperatur']) / ramps['Rampe 1']
                th1 = ramps['Haltezeit 1']
                self._ramp_dt_1 = self._ramp_dt_0 + th1
                tr2 = abs(ramps['Temperatur 2'] - ramps['Temperatur 1']) / ramps['Rampe 2']
                self._ramp_dt_2 = self._ramp_dt_1 + tr2
                th2 = ramps['Haltezeit 2']
                self._ramp_dt_3 = self._ramp_dt_2 + th2
                tr3 = abs(ramps['Temperatur 3'] - ramps['Temperatur 2']) / ramps['Rampe 3']
                self._ramp_dt_4 = self._ramp_dt_3 + tr3
                # vhs_logger.debug(self._name + ' read_profile_configuration: dts: \n' + str(self._ramp_dt_0[0:3]) + '\n' + str(self._ramp_dt_2[0:3]) + '\n ' + str(self._ramp_dt_2[0:3]) + '\n' + str(self._ramp_dt_3[0:3]) + '\n ' + str(self._ramp_dt_4[0:3]))
                self._profile_length = max(tr1.dropna().unique()) + max(th1.dropna().unique()) + max(tr2.dropna().unique()) + max(th2.dropna().unique()) + max(tr3.dropna().unique())
                self._ramps = ramps
                self._temperatures_eoc = self._ramps['Starttemperatur'].to_numpy()
                # print('Cooldown temperatures_eoc: ', self._temperatures_eoc)
                self.calc_temperature_setpoints(0.)
                self.setEnabled(self._ramps['Enabled'])
                if self._mqtt is not None:
                    # vhs_logger.debug(self._name + ' Vhs _on_connect() Publishing channel descriptions.')
                    self._mqtt.publish_topic(self._name + '/Descriptions', ramps['Bemerkung'], qos=0, retain=True, expand=self._expand)
                    self._mqtt.publish_topic(self._name + '/ProfileLength', self._profile_length, qos=0, retain=True, expand=False)
            except Exception as e:
                print(f'read_profile_configuration: Exception cought. {e}')
        else:
            vhs_logger.error(self._name + ' read_profile_configuration: ' + filename + ' does not exist.')
        pass

    def request_state_change(self, request):
        try:
            self._sm.send(request)
            vhs_logger.info(self._name + ' _Vhs request_state_change() State change request accepted, new state is ' + self._sm.current_state.name)
            print(self._name + ' _Vhs request_state_change() State change request accepted, new state is ' + self._sm.current_state.name)
            if self._mqtt is not None:
                self._mqtt.publish_topic(self._name + '/State', self._sm.current_state.id, qos=0, retain=True, expand=False)
        except Exception as e:
            vhs_logger.exception(self._name + ' _Vhs request_state_change() Request_State_Change ignored due to exception. ')
            print(self._name + ' _Vhs request_state_change() Request_State_Change ignored due to exception. ')
        pass

    def start_profile(self):
        """ Called from vhs_sm:VhsSM.on_enter_profile()"""
        vhs_logger.info(self._name + ' Vhs setProfileStartTime.')
        if not self._t_start_profile:
            self._t_start_profile = datetime.now()
            self._profile_duration = 0.
            self._profile_pause = 0.
            self._t_start_cooldown = None
            self._cooldown_length = 0.  # Unit: s
            self._cooldown_duration = 0.  # Unit: s
            self._cooldown_left = 0.  # Unit: s
        if self._mqtt is not None:
            if not self._t_start_cooldown:
                self._mqtt.publish_topic(self._name + '/CooldownStartTime', 'NA', qos=0, retain=True, expand=False)
            else:
                self._mqtt.publish_topic(self._name + '/CooldownStartTime', self._t_start_cooldown.ctime(), qos=0, retain=True, expand=False)
            self._mqtt.publish_topic(self._name + '/CooldownLength', self._cooldown_length, qos=0, retain=True, expand=False)
            self._mqtt.publish_topic(self._name + '/CooldownDuration', self._cooldown_duration, qos=0, retain=True, expand=False)
            self._mqtt.publish_topic(self._name + '/CooldownLeft', self._cooldown_left, qos=0, retain=True, expand=False)
            if not self._t_start_profile:
                self._mqtt.publish_topic(self._name + '/ProfileStartTime', 'NA', qos=0, retain=True, expand=False)
            else:
                self._mqtt.publish_topic(self._name + '/ProfileStartTime', self._t_start_profile.ctime(), qos=0, retain=True, expand=False)
            self._mqtt.publish_topic(self._name + '/CooldownLength', np.nanmax(self._cooldown_length), qos=0, retain=True, expand=False)
            self._mqtt.publish_topic(self._name + '/ProfileDuration', self._profile_duration, qos=0, retain=True, expand=False)
            self._mqtt.publish_topic(self._name + '/ProfilePause', self._profile_pause, qos=0, retain=True, expand=False)
        pass

    def pause_profile(self):
        """ Called from vhs_sm:VhsSM.on_enter_paused()"""
        vhs_logger.info(self._name + ' Vhs pauseProfile.')
        self._t_pause_profile = datetime.now()
        pass

    def exit_pause(self):
        """ Called from vhs_sm:VhsSM.on_exit_paused()"""
        vhs_logger.info(self._name + ' Vhs continueProfile.')
        self._profile_pause += (datetime.now() - self._t_pause_profile).total_seconds()
        if self._mqtt is not None:
            self._mqtt.publish_topic(self._name + '/ProfilePause', self._profile_pause, qos=0, retain=True, expand=False)
        pass

    def cool_down(self):
        """ Called from vhs_sm:VhsSM.on_enter_cooling()"""
        vhs_logger.info(self._name + ' Vhs cool_down.')
        if not self._t_start_cooldown:
            self._t_start_profile = None
            self._temperatures_soc = np.full(self._nof_channels, float('-inf'), dtype=np.float64)   # Unit: °C soc: Start of Cooling
            self._temperatures_eoc = np.full(self._nof_channels, float('-inf'), dtype=np.float64)   # Unit: °C soc: End of Cooling
            self._t_start_cooldown = datetime.now()
            # print(self._name + ' cool_down: start= ', self._t_start_cooldown.ctime())
            self._temperatures_soc[self._ramps['Enabled']] = self._temperatures[self._ramps['Enabled']]  # Latch actual temperatures
            # print(self._name + ' cool_down: temperatures_soc: ', self._temperatures_soc)
            self._temperatures_eoc[self._ramps['Enabled']] = self._ramps['Temperatur 3'].to_numpy()[self._ramps['Enabled']]  # Calculated in read_profile_configuration()
            # print(self._name + ' cool_down:  temperatures_eoc: ', self._temperatures_eoc)
            self._cooldown_sign = np.sign(self._temperatures_eoc - self._temperatures_soc)
            self._cooldown_lengths = np.fabs(self._temperatures_eoc - self._temperatures_soc) / self._gtemperatures_cooling
            # print(self._name + ' cool_down: lengths:\n', str(self._cooldown_lengths))
            self._cooldown_length = np.nanmax(self._cooldown_lengths[self._cooldown_lengths != np.inf])
            self._cooldown_duration = 0.
            self._cooldown_left = self._cooldown_length
            # print(f'Cooldown length={self._cooldown_length}s, Duration={self._cooldown_duration}s, Left={self._cooldown_left}s')
        else:
            print('Cooldown start time already defined. Cooldown parameters not calculated.')
        if self._mqtt is not None:
            if not self._t_start_cooldown:
                self._mqtt.publish_topic(self._name + '/CooldownStartTime', 'NA', qos=0, retain=True, expand=False)
            else:
                self._mqtt.publish_topic(self._name + '/CooldownStartTime', self._t_start_cooldown.ctime(), qos=0, retain=True, expand=False)
            self._mqtt.publish_topic(self._name + '/CooldownLength', np.nanmax(self._cooldown_length), qos=0, retain=True, expand=False)
            self._mqtt.publish_topic(self._name + '/CooldownDuration', self._cooldown_duration, qos=0, retain=True, expand=False)
            self._mqtt.publish_topic(self._name + '/CooldownLeft', self._cooldown_left, qos=0, retain=True, expand=False)
        pass

    # Getter
    def getDO(self):
        """Return DO state."""
        return self._do

    def getEnabled(self):
        """Return enabled state."""
        return self._enabled

    def getForced(self):
        """Return enabled state."""
        return self._forced

    def getRatios(self):
        """Return PWM ratios."""
        return self._ratios

    # Setter
    def setChannelDO(self, channel, state):
        """Set DO state for channel."""
        if self._sm.manual.is_manual:
            # vhs_logger.debug(self._name + ' Vhs setChannelDO: ' + str(channel) + ' ' + str(state))
            # print(self._name + ' Vhs setChannelDO: ' + str(channel) + ' ' + str(state))
            self._do[channel] = state
            self.setDO(self._do)
        else:
            vhs_logger.debug(self._name + ' Vhs setChannelDO: Request ignored, if not in manual mode.')
            # print(self._name + ' Vhs setChannelDO: Request ignored, if not in manual mode')
            pass
        pass

    def setChannelEnabled(self, channel, enabled):
        """Set enabled state for channel."""
        if not self._sm.profile.is_active:
            # vhs_logger.debug(self._name + ' Vhs setChannelEnabled: ' + str(channel) + ' ' + str(enabled))
            # print(self._name + ' Vhs setChannelEnabled: ' + str(channel) + ' ' + str(enabled))
            self._enabled[channel] = enabled
            self.setEnabled(self._enabled)
        else:
            vhs_logger.debug(self._name + ' Vhs setChannelEnabled: Request ignored, profile mode is active.')
            # print(self._name + ' Vhs setChannelEnabled: Request ignored, profile mode is active.')
            pass
        pass

    def setChannelForced(self, channel, forced):
        """Set forced state for channel in all states."""
        vhs_logger.warning(self._name + ' Vhs setChannelForced: ' + str(channel) + ' ' + str(forced))
        print(self._name + ' Vhs setChannelForced: ' + str(channel) + ' ' + str(forced))
        self._forced[channel] = forced
        self.setForced(self._forced)
        pass

    def setChannelPWMRatio(self, channel, ratio):
        """Set PWM ratio channel."""
        if self._sm.manual.is_active:
            # vhs_logger.debug(self._name + ' Vhs setChannelPWMRatio: ' + str(channel) + ' ' + str(ratio))
            self._ratios[channel] = ratio
            # print(self._name + ' Vhs setChannelPWMRatio: ' + str(channel) + ' ' + str(ratio) + ' ' + str(self._ratios))
            self.setPWMRatios(self._ratios)
        else:
            vhs_logger.debug(self._name + ' Vhs setChannelPWMRatio: Request ignored, profile mode is active.')
            # print(self._name + ' Vhs setChannelPWMRatio: Request ignored, profile mode is active.')
            pass
        pass

    def setChannelSPTemperature(self, channel, sptemperature):
        """Set SPTemperature channel."""
        if not self._sm.profile.is_active:
            # vhs_logger.debug(self._name + ' Vhs setChannelSPTemperature: ' + str(channel) + ' ' + str(sptemperature))
            self._sptemperatures[channel] = sptemperature
            # print(self._name + ' Vhs setChannelSPTemperature: ' + str(channel) + ' ' + str(sptemperature) + ' ' + str(self._sptemperatures))
            self.setSPTemperatures(self._sptemperatures)
        else:
            # vhs_logger.debug(self._name + ' Vhs setChannelSPTemperature: Request ignored, profile mode is active.')
            # print(self._name + ' Vhs setChannelSPTemperature: Request ignored, profile mode is active.')
            pass
        pass

    def setDO(self, state):
        """Set output state for channel."""
        if self._sm.manual.is_active:
            # vhs_logger.debug(self._name + ' Vhs setDO: ' + str(state.tolist()))
            # print(self._name + ' Vhs setDO: ' + str(state.tolist()))
            self._do = np.copy(state)
            for ii in range(len(self._DOPWM)):
                a, e = self.__a_e(ii)
                self._DOPWM[ii].setDO(self._do[a:e])
        else:
            vhs_logger.debug(self._name + ' Vhs setDO: Request ignored, if not in manual mode.')
            # print(self._name + ' Vhs setDO: Request ignored, if not in manual mode.')
            pass
        pass

    def setEnabled(self, enabled):
        """Set enabled state for channel."""
        if not self._sm.profile.is_active:
            # vhs_logger.debug(self._name + ' Vhs setEnabled: ' + str(enabled.tolist()))
            # print(self._name + ' Vhs setEnabled: ' + str(enabled.tolist()))
            self._enabled = np.copy(enabled)
            for ii in range(len(self._DOPWM)):
                a, e = self.__a_e(ii)
                self._DOPWM[ii].setEnabled(self._enabled[a:e])
            if self._mqtt is not None:
                # vhs_logger.debug(self._name + ' Vhs Publish Enabled.')
                self._mqtt.publish_topic(self._name + '/Enabled', self._enabled, qos=0, retain=True, expand=self._expand)
                pass
        else:
            vhs_logger.debug(self._name + ' Vhs setEnabled: Request ignored, profile mode is active.')
            # print(self._name + ' Vhs setEnabled: Request ignored, profile mode is active.')
            pass
        pass

    def setForced(self, forced):
        """Set forced state for channel in all states."""
        # vhs_logger.warning(self._name + ' Vhs setForced: ' + str(forced.tolist()))
        # print(self._name + ' Vhs setForced: ' + str(forced.tolist()))
        self._forced = np.copy(forced)
        for ii in range(len(self._DOPWM)):
            a, e = self.__a_e(ii)
            self._DOPWM[ii].setForced(self._forced[a:e])
        if self._mqtt is not None:
            # vhs_logger.debug(self._name + ' Vhs Publish Forced.')
            self._mqtt.publish_topic(self._name + '/Forced', self._forced, qos=0, retain=True, expand=self._expand)
            pass
        pass

    def setPWMRatios(self, ratios):
        """Set PWM ratios."""
        if self._sm.manual.is_active:
            # vhs_logger.debug(self._name + ' Vhs setPWMRatios: ' + str(ratios.tolist()))
            # print(self._name + ' Vhs setPWMRatios: ' + str(ratios.tolist()))
            self._ratios = np.copy(ratios)
            for ii in range(len(self._DOPWM)):
                a, e = self.__a_e(ii)
                self._DOPWM[ii].setPWMRatios(self._ratio[a:e])
            if self._mqtt is not None:
                # vhs_logger.debug(self._name + ' Vhs Publish Ratios.')
                self._mqtt.publish_topic(self._name + '/PWMRatios', self._ratios, qos=0, retain=True, expand=self._expand)
                pass
        else:
            vhs_logger.debug(self._name + ' Vhs setPWMRatios: Request ignored, not in manual mode.')
            # print(self._name + ' Vhs setPWMRatios: Request ignored, not in manual mode.')
            pass
        pass

    def setProfiles(self, filename):
        """Read profiles from file an generate."""
        vhs_logger.debug(self._name + ' Vhs setProfiles: ' + filename)
        # print(self._name + ' Vhs setProfiles: ' + filename)
        if self._sm.manual.is_active:
            if os.path.exists(filename):
                print(self._name + ' Vhs setProfiles: Filename exists.')
                self.read_profile_configuration(filename=filename)
                if self._mqtt is not None:
                    # vhs_logger.debug(self._name + ' Vhs setProfiles: Publish Profile configuration filename.')
                    self._mqtt.publish_topic(self._name + '/Profiles', self._profile_config_file, qos=0, retain=True, expand=False)
                    pass
            else:
                vhs_logger.debug(self._name + ' Vhs setProfiles: filename does not exist.' + filename)
                print(self._name + ' Vhs setProfiles: filename does not exist.' + filename)
        else:
            vhs_logger.debug(self._name + ' Vhs setProfiles: Request ignored, not in manual mode.')
            print(self._name + ' Vhs setProfiles: Request ignored, not in manual mode.')
        pass

    def setSPTemperatures(self, sptemperatures):
        """Set set-point temperatures."""
        if not self._sm.profile.is_active:
            # vhs_logger.debug(self._name + ' Vhs setSPTemperatures: ' + str(sptemperatures.tolist()))
            # print(self._name + ' Vhs setSPTemperatures: ' + str(sptemperatures.tolist()))
            self._sptemperatures = np.copy(sptemperatures)
            if self._mqtt is not None:
                # vhs_logger.debug(self._name + ' Vhs Publish SPTemperatures.')
                self._mqtt.publish_topic(self._name + '/SPTemperatures', self._sptemperatures, qos=0, retain=True, expand=self._expand)
                pass
        else:
            vhs_logger.debug(self._name + ' Vhs setSPTemperatures: Request ignored, profile mode is active.')
            # print(self._name + ' Vhs setSPTemperatures: Request ignored, profile mode is active.')
            pass
        pass

    pass  # End of class definition


if __name__ == '__main__':
    print("vhs.py\n\
Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH\n\
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany\n\
eMail: H.Brand@gsi.de\n\
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme")
    # mp.freeze_support()
    app_name = re.split('.py', os.path.basename(__file__))[0]
    match os.name:
        case 'nt':
            profile_config_filename_default = 'D:\\Brand\\Python\\VacuumHeatingSystem\\profiles\\20230703_Test_SIS18_VAB7_config.csv'
        case _:
            profile_config_filename_default = '/home/brand/Python/VacuumHeatingSystem/profiles/Beispiel_config_ESR_Sued_GS_50Grad_short.csv'
            pass
    parser = argparse.ArgumentParser(prog='vhs.py', description='This is the Vacuum Heating System.', epilog='')
    parser.add_argument('-b', '--broker', dest='broker', default='sadpc009', help='MQTT Broker Node (Name or IP)')
    parser.add_argument('-u', '--user', dest='user', default=None, help='MQTT Broker User Name')
    parser.add_argument('--password', dest='password', default=None, help='MQTT Broker Password')
    parser.add_argument('-i', '--interference', dest='fuzzy_interference_mode', default='mamdani', choices=['mamdani', 'sugeno'], help='Fuzzy interference mode')
    parser.add_argument('-m', '--multiprocessing', dest='mp_mode_input', default='y', choices=['n', 'y'], help='Multiprocessing mode not working on Windows.')
    parser.add_argument('-f', '--profile', dest='profile_config_filename', default=profile_config_filename_default, help='Absolute filename containing temperature profiles configuration.')
    args = parser.parse_args()
    mqtt_broker = args.broker
    mqtt_user = args.user
    mqtt_password = args.password
    """
    if mqtt_password is None:  # but required
        mqtt_password = getpass.getpass()
        if len(mqtt_password) == 0:
            mqtt_password = None
    """
    if re.match('n', args.mp_mode_input):
        mp_mode = False
    else:
        mp_mode = True
    match os.name:  # Override command line parameter for now.
        case 'nt':
            mp_mode = False
        case _:
            mp_mode = True
    print(__name__, f'MQTT Broker: {mqtt_broker}')
    print(__name__, f'MQTT User: {mqtt_user}')
    print(__name__, f'Fuzzy_interference_mode: {args.fuzzy_interference_mode} Tpye: {type(args.fuzzy_interference_mode)}')
    print(__name__, f'Multiprocessing mode (overwritten): {str(mp_mode)}')
    print(__name__, f'Profile_config_filename: {args.profile_config_filename}')
    # exit(0)  # If command line parameters should be checked.
    # Get user name
    """
    if os.name == 'posix':
        import pwd
        user_name = pwd.getpwuid(os.geteuid()).pw_name
    else:
        user_name = os.getlogin()
    """
    user_name = getpass.getuser()
    print(__name__, f'User: {user_name}')
    # Logging setup
    print(__name__, 'Configuring logging...')
    # Define logger to be used
    # logging.basicConfig(  # filename='example.log', filemode='w', encoding='utf-8', level=logging.INFO, style='{', format='{asctime} [{levelname:8}] {process} {thread} {module} {funcName} {lineno} {message}')
    logger = logging.getLogger('App-Logger')
    logger.setLevel(logging.DEBUG)
    pyacdaq_logger = logging.getLogger('pyacdaq')
    pyacdaq_logger.setLevel(logging.WARNING)
    nidaqmx_logger = logging.getLogger('nidaqmx')
    nidaqmx_logger.setLevel(logging.ERROR)
    vhs_logger.setLevel(logging.INFO)
    # Define logging formatter
    # logging_formatter = logging.Formatter(style='{', fmt='{asctime} [{levelname:8}] {module} {funcName} {lineno} {message}')
    logging_formatter = logging.Formatter(style='{', fmt='{asctime} [{levelname:8}] {process} {thread} {module} {funcName} {lineno} {message}')
    configure_logging_handler(app_name, (logger, pyacdaq_logger, nidaqmx_logger, vhs_logger), logging_formatter, severity={'console': logging.ERROR, 'file': logging.DEBUG, 'mqtt': logging.INFO, 'syslog': logging.WARNING}, mqtt={'broker': mqtt_broker, 'topic': platform.node() + '/log_msg', 'user': mqtt_user, 'password': mqtt_password})
    try:
        print(__name__, 'Launching MQTT-Actor....', flush=True)
        # Create MQTT actor proxy
        p_mqtt = Mqtt.start('Mqtt',
                            start_periodic=False,
                            broker=mqtt_broker,
                            port=1883,
                            client_id=platform.node(),
                            clean_session=True,
                            retain=False,
                            will='offline',
                            username=mqtt_user,
                            password=mqtt_password
                            ).proxy()
        # Create hvs proxy
        print('Launching Vhs-Actor...', flush=True)
        time.sleep(3)
        a_vhs = Vhs.start('Vhs',
                          start_periodic=True,
                          mqtt=p_mqtt,
                          profile_config_filename=args.profile_config_filename,
                          fuzzy_interference_mode=args.fuzzy_interference_mode,
                          fuzzy_mp=mp_mode
                          )
        p_vhs = a_vhs.proxy()
        """
        vhs_name = p_vhs.getName().get()
        vhs_birthday = p_vhs.getBirthday().get()
        vhs_periodic = p_vhs.getPeriodicStatus().get()
        vhs_sw_revision = p_vhs.getSWRevision().get()
        print(__name__, 'Name:', vhs_name,
              '; Birthday:', vhs_birthday,
              '; Periodic:', vhs_periodic,
              '; SW:', vhs_sw_revision)
        a_vhs.tell('TestCommand via message.')
        """
        vhs_cli = VhsCli(vhs=a_vhs)
        vhs_cli.cmdloop()
    except Exception as e:
        print('_main__: Exception caught:', e)
    finally:
        p_vhs.stop()
        time.sleep(3)
        p_mqtt.stop()
    logging.shutdown()
    print(__name__, 'Done.')
