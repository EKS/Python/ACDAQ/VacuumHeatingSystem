#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
PyAcadq is a Python class library for Automation Control & Data Acquisition Systems.

PyAcadq is meant as a possible substitute (Plan B) for the LabVIEW based
[CS](https://git.gsi.de/EE-LV/CS) and [CS++](https://git.gsi.de/EE-LV/CSPP) frameworks.

nidaqmx_logger = logging.getLogger('nidaqmx') is used for package specific logging.

Classes
-------
DAQmxAi(Device): DAQmxAi actor reads analog inputs, calculates the averages.
DAQmxDoPwm(Device): DAQmxDoPwm actor reads a finite set of analog inputs from 64 channels, calculates the averages

Lizenziert unter EUPL V. 1.2

Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
"""
import logging
nidaqmx_logger = logging.getLogger('nidaqmx').addHandler(logging.NullHandler())
