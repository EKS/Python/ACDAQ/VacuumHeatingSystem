#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
DAQmxAi actor reads analog inputs, calculates the averages.

DAQmxAi actor reads a finite set of analog inputs from 64 channels, calculates the averages
The results are published to MQTT and are sent to its caller actor.

Lizenziert unter EUPL V. 1.2

Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
"""

import getpass
import logging
import os
import re
import time
# import pyacdaq.exceptions as acdaq_exceptions
from pyacdaq.logger import configure_logging_handler
from pyacdaq.actor.actor import PeriodicCallReason as PeriodicCallReason
from pyacdaq.actor.mqtt import Mqtt as Mqtt
from pyacdaq.actor.device import Device
import nidaqmx
from nidaqmx.stream_readers import AnalogMultiChannelReader
from nidaqmx import constants
import numpy as np

nidaqmx_logger = logging.getLogger('nidaqmx')


class DAQmxAi(Device):
    """Derived device actor class using nidaqmx to read analog input from NI harware."""

    def __init__(self, name,
                 start_periodic=False,
                 mqtt=None,
                 expand=False,
                 resource='',
                 id_query_flag=True,
                 reset_flag=True,
                 reset_options='',
                 selftest_flag=True,
                 nof_channels=8,
                 samples_per_channel=1000,
                 sampling_freq=1000
                 ):
        """
        Initialize instance.

        Arguments
        ---------
        name -- Name of instance
        start_periodic -- Auto start periodic action if true. Default is False.
        mqtt_actor -- Reference to MQTT actor. Default is None.
        resource -- Interface to be used Waitingfor communication with device.
        id_query_flag -- Perform ID query (*IDN?); Default is True.
        reset_flag -- Perform instrument reset; Default is True.
        reset_option -- Options to be applied after reset; Default is None.
        selftest_flag -- Perform instrument self-test after option reset; Default is True.
        expand -- Expand channel topics; Default is False.

        """
        super().__init__(name, start_periodic, mqtt, resource, id_query_flag, reset_flag, reset_options, selftest_flag)
        self._expand = expand
        self._slot = resource
        self._ai_rng_high = 10.
        self._ai_rng_low = -10.
        self._terminal_config = constants.TerminalConfiguration.RSE  # DEFAULT =-1, DIFF = 10106, NRSE = 10078, PSEUDO_DIFF = 12529, RSE = 10083
        self._nof_channels = nof_channels
        self._samples_per_channel = samples_per_channel
        self._sampling_freq = sampling_freq  # in Hz
        self._avgs = np.zeros(self._nof_channels, dtype=np.float64)
        self._stds = np.zeros(self._nof_channels, dtype=np.float64)
        self._task = None
        self._max_ai_exceptions = 3
        self._ai_exceptions = 0
        pass

    def __del__(self):
        """Finalize instance."""
        super().__del__()
        pass

    # Private methods
    def __initialize_nidaqmx_task(self):
        if self._task:
            self.__close_nidaqmx_task()
        nidaqmx_logger.debug(self._name + ' DAQmxAi __initialize_nidaqmx_task() Initialize task.')
        self._ai_exceptions = 0
        self._task = nidaqmx.Task(self._slot[1:])
        self._task.ai_channels.add_ai_voltage_chan(self._slot + '/ai0:' + str(self._nof_channels - 1),    # has to match with chans_in
                                                   max_val=self._ai_rng_high,
                                                   min_val=self._ai_rng_low,
                                                   terminal_config=self._terminal_config)
        self._task.timing.cfg_samp_clk_timing(rate=self._sampling_freq,
                                              sample_mode=constants.AcquisitionType.FINITE,  # constants.AcquisitionType.CONTINUOUS,
                                              samps_per_chan=self._samples_per_channel)
        self._stream_ai = AnalogMultiChannelReader(self._task.in_stream)
        self._buffer_ai = np.zeros((self._nof_channels, self._samples_per_channel))
        pass

    def __close_nidaqmx_task(self):
        """Close nidaqmx task."""
        nidaqmx_logger.debug(self._name + ' DAQmxAi __close_nidaqmx_task() Closing task.')
        if self._task:
            self._task.close()
            self._task = None
        pass

    # Protected override methods from super Actor
    def _on_connect(self):
        """
        Called from _on_receive() when message was received from paho.mqtt.client callback message on_connect.

        Perform action on MQTT connect here, e.g. publish and subscribe topics.

        Returns
        -------
        None.

        """
        nidaqmx_logger.debug(self._name + ' DAQmxAi _on_connect() Received MQTT connected message. Publish and subscribe...')
        try:
            if self._mqtt is not None:
                # nidaqmx_logger.info(self._name + ' DAQmxAi _on_connect() Subscribing to: ' + str(prefixed_topics) + ' returned rcs, mids = ' + str(rcs_mids))
                pass
        except Exception as e:
            nidaqmx_logger.exeption(self._name + ' DAQmxAi _on_connect(): Exception cought:')
            nidaqmx_logger.exception(self._name + ' DAQmxAi _on_connect(): Exception: ' + str(e))
        super()._on_connect()
        pass

    def _on_message(self, topic, msg):
        """https://www.advantech.com/en/products/data-acquisition-(daq)/sub_1-2mljza
        Called from Mqtt._on_receive() when message was received from paho.mqtt.client callback message on_message.

        Perform action on MQTT messages here or forward to super.

        Returns
        -------
        None.

        """
        nidaqmx_logger.debug(self._name + ' DAQmxAi _on_message() Received MQTT topic.' + topic + ' msg: ' + str(msg))
        sub_topics = re.split('/', topic)
        match sub_topics[2]:
            case _:  # Call super
                nidaqmx_logger.debug(self._name + ' DAQmxAi _on_message() Calling super() due for unknown topic: ' + topic + ' msg: ' + str(msg))
                super()._on_message(topic, msg)
        pass

    def _on_receive(self, message):
        """
        May be implemented for the actor to handle regular non-proxy messages.

        Virtual protected method.
        Override with concrete child class implementation.

        Returns
        -------
        None.

        """
        nidaqmx_logger.debug(self._name + ' DAQmxAi _on_receive() call super()._on_receive(message)')
        super()._on_receive(message)
        pass

    def _on_start(self):
        """
        Hook for doing any setup that should be done after the actor is started, but before it starts processing messages.

        Virtual protected method.
        Override with concrete child class implementation.

        Returns
        -------
        None.

        """
        nidaqmx_logger.debug(self._name + ' DAQmxAi _on_start() call super()._on_start()')
        super()._on_start()
        pass

    def _on_stop(self):
        """
        Hook for doing any cleanup that should be done after the actor has processed the last message, and before the actor stops.

        Close instrument.

        Virtual protected method.Waiting
        Override with concrete child class implementation.

        Returns
        -------
        None.

        """
        nidaqmx_logger.debug(self._name + ' DAQmxAi _on_stop()')
        try:
            if self._mqtt is not None:
                pass
        except Exception as e:
            nidaqmx_logger.exception(self._name + ' DAQmxAi _on_stop(): Exception cought:')
            nidaqmx_logger.exception(self._name + ' DAQmxAi _on_stop(): Exception: ' + str(e))
        super()._on_stop()
        pass

    def _periodic(self, reason):
        """
        Implementation of periodic actions.

        Read data from instrument.

        Parameters
        ----------
        reason : PeriodicCallReason
            Reason for calling this method.

        Returns
        -------
        None.

        """
        try:
            nidaqmx_logger.debug(self._name + ' Reading ai from slot: ' + self._slot + ' Counter=' + str(self._periodic_counter))
            match reason:
                case PeriodicCallReason.Start:
                    self.__initialize_nidaqmx_task()
                    pass
                case PeriodicCallReason.Do:
                    # t0 = time.time()
                    while self._ai_exceptions < self._max_ai_exceptions:
                        try:
                            self._stream_ai.read_many_sample(self._buffer_ai, self._samples_per_channel, timeout=constants.WAIT_INFINITELY)
                            # print(self._name + ' AI Raw /V:', buffer_ai)
                            avg = np.average(self._buffer_ai, axis=1)
                            std = np.std(self._buffer_ai, axis=1)
                            nidaqmx_logger.debug(self._name + ' Averages / V:\n' + str(avg))
                            nidaqmx_logger.debug(self._name + ' Std.Dev. / V:\n' + str(std))
                            self._avgs[:] = avg[:]
                            self._stds[:] = std[:]
                            break
                        except Exception as e:
                            if self._ai_exceptions < self._max_ai_exceptions:
                                self._ai_exceptions += 1
                                nidaqmx_logger.warning(f'{self._name} Exeption cought in _periodic.Do, retry={self._ai_exceptions}/{self._max_ai_exceptions} \r\n{str(e)}')
                            else:
                                nidaqmx_logger.Error(f'{self._name} Exeption cought in _periodic.Do, maximum retries({self._ai_exceptions}) unsuccessful -> reinitialize nidaqmx task. \r\n{str(e)}')
                                self.__initialize_nidaqmx_task()
                    if self._mqtt is not None:
                        nidaqmx_logger.debug(self._name + ' Publish AI avg and std.')
                        self._mqtt.publish_topic(self._name + '/AI', avg, qos=0, retain=True, expand=self._expand)
                        self._mqtt.publish_topic(self._name + '/AI-Std', std, qos=0, retain=True, expand=self._expand)
                    # print(self._name + ' DAQmxAi _on_periodic Do: dt=' + str(time.time() - t0))
                    pass
                case PeriodicCallReason.Stop:
                    self.__close_nidaqmx_task()
                    pass
        except Exception as e:
            nidaqmx_logger.exception(self._name + ' Exeption cought in _periodic.', str(e))
            print(self._name + ' Exeption cought in _periodic.', str(e))
        pass

    # Protected override methods from super Device
    def _close(self):
        """
        Set instrument to save mode and close connection.

        Returns
        -------
        int
            Error code.
        """
        nidaqmx_logger.debug(self._name + ' DAQmxAi _close() Closing device connection.')
        if self._task:
            self._task.close()
        return

    def _id_query(self):
        """
        Perform id query of instrument.

        Returns
        -------
        string
            Instrument ID.

        """
        nidaqmx_logger.debug(self._name + ' DAQmxAi _id_query() Performing id query.')
        device = nidaqmx.system.System.local().devices[self._slot[1:]]
        id = str(device.product_type) + '|' + str(device.product_num)
        return id

    def _initalize(self,
                   resource=None,
                   id_query_flag=True,
                   reset_flag=True,
                   reset_options=None,
                   selftest_flag=True):
        """
        Open device connection and initialize instrument.

        Parameters
        ----------
        resource : string
            Resource identifier.
        id_query_flag : bool
            Flag requesting an id_query. Default isTrue.
        reset_flag : bool
            Flag requesting a reset. Default is True.
        reset_options : string
            String containing reset options. Default is None.
        selftest_flag : bool
            Flag requesting an id_query. Default is True.

        Returns
        -------
        int
            Error code.

        """
        nidaqmx_logger.debug(self._name + ' Initialize AI task for slot: ' + self._slot)
        try:
            if reset_flag:
                self.reset(reset_options)
            if id_query_flag:
                self.id_query()
            self.revision_query()
            if selftest_flag:
                self.selftest()
        except Exception as e:
            nidaqmx_logger.exception(self._name + ' DAQmxAi _initialize()')
            nidaqmx_logger.exception(self._name + ' Exception caught: ' + str(e))
            raise e
        pass

    def _reset(self, reset_options=None):
        """
        Reset instrument with options.

        Parameters
        ----------
        reset_options : string
            String containing reset options. Default is None.

        Returns
        -------
        string
            Reset result.

        """
        nidaqmx_logger.warning(self._name + ' DAQmxAi _reset() Performing reset.')
        try:
            nidaqmx.system.System.local().devices[self._slot[1:]].reset_device()
            # response = "OK"
            response = "successful"
        except nidaqmx.errors.DaqError as e:
            response = "Failed"
            nidaqmx_logger.exception('Reset failed. ' + str(e))
        return response

    def _revision_query(self):
        """
        Perform instrument revision query, firmware and hardware.

        Returns
        -------
        string
            Firmware revision.
        string
            Hardware revision.

        """
        nidaqmx_logger.debug(self._name + ' DAQmxAi _revision_query() Performing revision query.')
        fw_version = nidaqmx.system.System.local().driver_version
        subs = re.split(',', str(fw_version))
        fw = ''
        for sub in subs:
            vs = re.search('\d+', sub).group(0)
            fw = fw + '.' + vs
        hw = 'NA'
        return fw[1:], hw

    def _selftest(self):
        """
        Perform instrument selftest.

        Returns
        -------
        int
            Selftest return code.
        string
            Selftest message.

        """
        nidaqmx_logger.warning(self._name + ' DAQmxAi _selftest() Performing selftest.')
        try:
            nidaqmx.system.System.local().devices[self._slot[1:]].self_test_device()
            code = 0
            # message = 'OK'
            message = 'Successful'
        except nidaqmx.errors.DaqError as e:
            code = -1
            message = 'Failed'
            nidaqmx_logger.exception('Selftest failed. ' + str(e))
        return code, message

    def _sn_query(self):
        """
        Perform instrument serial number query.

        Returns
        -------
        string
            Serial number.

        """
        nidaqmx_logger.debug(self._name + ' DAQmxAi _sn_query() Performing serial number query.')
        sn = str(nidaqmx.system.System.local().devices[self._slot[1:]].serial_num)
        return sn

    def _sw_revision(self):
        """
        Return revision of this class inclusive parents.

        Returns
        -------
        string
            Software revision.

        """
        return 'DAQmxAi_0.0.1.0|' + super()._sw_revision()

    # Public methods of this class

    # Getter
    def getAI(self):
        """
        Return averages of analog input values.

        Returns
        -------
        numpy.ndarray
            Avaraged analog input values. Unit: V

        """
        return self._avgs

    def getStdDev(self):
        """
        Return standard deviations of analog input values.

        Returns
        -------
        numpy.ndarray
            Standard deviation of analog input values. Unit: V

        """
        return self._stds

    pass  # End of class definition

    pass  # end of DAQmxAi class definition


if __name__ == '__main__':
    print("daqmx_ai.py\n\
Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH\n\
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany\n\
eMail: H.Brand@gsi.de\n\
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme")
    app_name = re.split('.py', os.path.basename(__file__))[0]
    # Logging setup
    print('Configuring logging...')
    # Define logger to be used
    # logging.basicConfig(  # filename='example.log', filemode='w', encoding='utf-8', level=logging.INFO, style='{', format='{asctime} [{levelname:8}] {process} {thread} {module} {funcName} {lineno} {message}')
    logger = logging.getLogger('App-Logger')
    logger.setLevel(logging.DEBUG)
    pyacdaq_logger = logging.getLogger('pyacdaq')
    pyacdaq_logger.setLevel(logging.WARNING)
    nidaqmx_logger.setLevel(logging.INFO)
    # Define logging formatter
    logging_formatter = logging.Formatter(style='{', fmt='{asctime} [{levelname:8}] {module} {funcName} {lineno} {message}')
    # logging_formatter = logging.Formatter(style='{', fmt='{asctime} [{levelname:8}] {process} {thread} {module} {funcName} {lineno} {message}')
    # configure_logging_handler((logger, pyacdaq_logger), logging_formatter, severity={'console': None, 'file': None, 'mqtt': None, 'syslog': None}, mqtt={'broker': 'ee-raspi1001', 'topic': 'ACDAQ/log_msg', 'user': '', 'password': ''})
    test = False
    try:
        mqtt_broker = input('MQTT Broker? (localhost)>')
        if len(mqtt_broker) == 0:
            mqtt_broker = 'localhost'
        mqtt_user = input('MQTT Username: ')
        if len(mqtt_user) == 0:
            mqtt_user = None
        mqtt_password = getpass.getpass()
        if len(mqtt_password) == 0:
            mqtt_password = None
        configure_logging_handler(app_name, (logger, pyacdaq_logger, nidaqmx_logger), logging_formatter, severity={'console': logging.INFO, 'file': logging.DEBUG, 'mqtt': logging.INFO, 'syslog': logging.WARNING}, mqtt={'broker': mqtt_broker, 'topic': 'ACDAQ/log_msg', 'user': mqtt_user, 'password': mqtt_password})
        print(__name__, 'Launching MQTT actor...')
        # Create MQTT actor proxy
        p_mqtt = Mqtt.start('Mqtt',
                        start_periodic=False,
                        broker=mqtt_broker,
                        port=1883,
                        client_id='ACDAQ',
                        clean_session=True,
                        retain=True,
                        will='offline',
                        username=mqtt_user,
                        password=mqtt_password
                        ).proxy()

        # Create device proxy
        print(__name__, 'Launching device actors(s)...')
        time.sleep(5)
        if test:
            a_device = DAQmxAi.start('DAQmxAi',
                                     start_periodic=True,
                                     mqtt=p_mqtt,
                                     resource='/Dev1',
                                     id_query_flag=True,
                                     reset_flag=False,
                                     reset_options='',
                                     selftest_flag=False,
                                     expand=True,
                                     nof_channels=8,
                                     samples_per_channel=1000,
                                     sampling_freq=1000
                                     )
            p_device = a_device.proxy()
            device_name = p_device.getName().get()
            device_birthday = p_device.getBirthday().get()
            device_periodic = p_device.getPeriodicStatus().get()
            device_sw_revision = p_device.getSWRevision().get()
            device_instrument_id = p_device.getInstrumentID().get()
            device_firmware_revision = p_device.getFirmwareRevision().get()
            device_hardware_revision = p_device.getHardwareRevision().get()
            device_serial_number = p_device.getSerialNumber().get()
            print(__name__, 'Name:', device_name,
                  '; Birthday:', device_birthday,
                  '; Periodic:', device_periodic,
                  '; SW:', device_sw_revision,
                  '; ID:', device_instrument_id,
                  '; FW:', device_firmware_revision,
                  '; HW:', device_hardware_revision,__name__, 
                  '; SN:', device_serial_number)
        else:
            PXI1Slot2 = DAQmxAi.start('PXI1Slot2',
                                      start_periodic=True,
                                      mqtt=p_mqtt,
                                      resource='/PXI1Slot2',
                                      id_query_flag=True,
                                      reset_flag=False,
                                      reset_options='',
                                      selftest_flag=False,
                                      expand=True,
                                      nof_channels=64,
                                      samples_per_channel=500,
                                      sampling_freq=1000
                                      )
            p_PXI1Slot2 = PXI1Slot2.proxy()
            PXI1Slot3 = DAQmxAi.start('PXI1Slot3',
                                      start_periodic=True,
                                      mqtt=p_mqtt,
                                      resource='/PXI1Slot3',
                                      id_query_flag=True,
                                      reset_flag=False,
                                      reset_options='',
                                      selftest_flag=False,
                                      expand=True,
                                      nof_channels=64,
                                      samples_per_channel=500,
                                      sampling_freq=1000
                                      )
            p_PXI1Slot3 = PXI1Slot3.proxy()
            PXI1Slot4 = DAQmxAi.start('PXI1Slot4',
                                      start_periodic=True,
                                      mqtt=p_mqtt,
                                      resource='/PXI1Slot4',
                                      id_query_flag=True,
                                      reset_flag=False,
                                      reset_options='',
                                      selftest_flag=False,
                                      expand=True,
                                      nof_channels=64,
                                      samples_per_channel=500,
                                      sampling_freq=1000
                                      )
            p_PXI1Slot4 = PXI1Slot4.proxy()
            PXI1Slot5 = DAQmxAi.start('PXI1Slot5',
                                      start_periodic=True,
                                      mqtt=p_mqtt,
                                      resource='/PXI1Slot5',
                                      id_query_flag=True,
                                      reset_flag=False,
                                      reset_options='',
                                      expand=True,
                                      selftest_flag=False,
                                      nof_channels=64,
                                      samples_per_channel=500,
                                      sampling_freq=1000
                                      )
            p_PXI1Slot5 = PXI1Slot5.proxy()
        time.sleep(5)
    except Exception as e:
        print(__name__, 'Exception caught:', e)
    finally:
        print(__name__, 'Stopping actors...')
        if test:
            p_device.stop()
        else:
            p_PXI1Slot2.stop()
            p_PXI1Slot3.stop()
            p_PXI1Slot4.stop()
            p_PXI1Slot5.stop()
        time.sleep(3)
        p_mqtt.stop()
    logging.shutdown()
    print(__name__, 'Done.')
