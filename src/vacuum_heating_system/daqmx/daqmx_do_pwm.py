#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
DAQmxDoPwm actor reads analog inputs, calculates the averages.

DAQmxDoPwm actor reads a finite set of analog inputs from 64 channels, calculates the averages
The results are published to MQTT and are sent to its caller actor.

- Replace DAQmxDoPwm with you class name.
- Replace daqmx_ai with instance name.

Lizenziert unter EUPL V. 1.2

Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
"""

import getpass
import logging
import json
import os
import re
import time
# import pyacdaq.exceptions as acdaq_exceptions
from pyacdaq.logger import configure_logging_handler
from pyacdaq.actor.actor import PeriodicCallReason as PeriodicCallReason
from pyacdaq.actor.mqtt import Mqtt as Mqtt
from pyacdaq.actor.device import Device
import nidaqmx
# from nidaqmx import constants
from nidaqmx.constants import LineGrouping
import numpy as np

nidaqmx_logger = logging.getLogger('nidaqmx')


class DAQmxDoPwm(Device):
    """Derived device actor class template."""

    def __init__(self, name,
                 start_periodic=False,
                 mqtt=None,
                 expand=False,
                 resource=None,
                 id_query_flag=True,
                 reset_flag=True,
                 reset_options=None,
                 selftest_flag=True,
                 nof_channels=8
                 ):
        """
        Initialize instance.

        Arguments
        ---------
        name -- Name of instance
        start_periodic -- Auto start periodic action if true. Default is False.
        mqtt_actor -- Reference to MQTT actor. Default is None.
        resource -- Interface to be used for communication with device.
        id_query_flag -- Perform ID query (*IDN?); Default is True.
        reset_flag -- Perform instrument reset; Default is True.
        reset_option -- Options to be applied after reset; Default is None.
        selftest_flag -- Perform instrument self-test after option reset; Default is True.
        expand -- Expand channel topics; Default is False.

        """
        super().__init__(name, start_periodic, mqtt, resource, id_query_flag, reset_flag, reset_options, selftest_flag)
        self._expand = expand
        self._slot = resource
        self._nof_channels = nof_channels
        self._sleep_time = 0.01
        self._max_do_exceptions = 3
        self._do_exceptions = 0
        self._task = None
        self._ones = np.ones(self._nof_channels, dtype=bool)
        self._zeros = np.zeros(self._nof_channels, dtype=bool)
        self._enabled = np.copy(self._ones)
        self._forced = np.copy(self._zeros)
        self._do = np.copy(self._zeros)
        if self._nof_channels > 0 and self._nof_channels <= 8:
            self._do_lines_prefixed = 'PREFIX/port0/line0:7'
        elif self._nof_channels <= 16:
            self._do_lines_prefixed = 'PREFIX/port0/line0:7,PREFIX/port1/line0:' + str((self._nof_channels - 1) - 8)
        elif self._nof_channels <= 24:
            self._do_lines_prefixed = 'PREFIX/port0/line0:7,PREFIX/port1/line0:7,PREFIX/port2/line0:' + str((self._nof_channels - 1) - 16)
        elif self._nof_channels <= 32:
            self._do_lines_prefixed = 'PREFIX/port0/line0:7,PREFIX/port1/line0:7,PREFIX/port2/line0:7,PREFIX/port3/line0:' + str((self._nof_channels - 1) - 24)
        elif self._nof_channels <= 40:
            self._do_lines_prefixed = 'PREFIX/port0/line0:7,PREFIX/port1/line0:7,PREFIX/port2/line0:7,PREFIX/port3/line0:7,PREFIX/port4/line0:' + str((self._nof_channels - 1) - 32)
        elif self._nof_channels <= 48:
            self._do_lines_prefixed = 'PREFIX/port0/line0:7,PREFIX/port1/line0:7,PREFIX/port2/line0:7,PREFIX/port3/line0:7,PREFIX/port4/line0:7,PREFIX/port5/line0:' + str((self._nof_channels - 1) - 40)
        elif self._nof_channels <= 56:
            self._do_lines_prefixed = 'PREFIX/port0/line0:7,PREFIX/port1/line0:7,PREFIX/port2/line0:7,PREFIX/port3/line0:7,PREFIX/port4/line0:7,PREFIX/port5/line0:7,PREFIX/port6/line0:' + str((self._nof_channels - 1) - 48)
        elif self._nof_channels <= 64:
            self._do_lines_prefixed = 'PREFIX/port0/line0:7,PREFIX/port1/line0:7,PREFIX/port2/line0:7,PREFIX/port3/line0:7,PREFIX/port4/line0:7,PREFIX/port5/line0:7,PREFIX/port6/line0:7,PREFIX/port7/line0:' + str((self._nof_channels - 1) - 56)
        else:
            raise IndexError('Number of channels out of range.')
        self._do_lines = re.sub('PREFIX', self._slot[1:], self._do_lines_prefixed, count=8)
        # self._ratios = np.linspace(0.05, 0.95, self._nof_channels, endpoint=False, dtype=np.float64)
        self._ratios = np.zeros(self._nof_channels, dtype=np.float64)
        self._daqmxdopwm_subscriptions = ('Set-ChannelDO', 'Set-ChannelEnable', 'Set-ChannelForce', 'Set-ChannelPWMRatio', 'Set-DO', 'Set-Enabled', 'Set-Forced', 'Set-PWMRatios', )
        pass

    def __del__(self):
        """Finalize instance."""
        super().__del__()
        pass

    # Private methods
    def __initialize(self):
        """Open device connection and initialize instrument."""
        nidaqmx_logger.debug(self._name + ' DAQmxDoPwm __initialize() Close and initialize connection.')
        if self._task:
            self._close()
        try:
            # Create task and set all DO false.
            self._do_exceptions = 0
            self._task = nidaqmx.Task(self._slot[1:])
            self._task.do_channels.add_do_chan(
                self._do_lines,
                line_grouping=LineGrouping.CHAN_PER_LINE)
            do = self._zeros
            self._task.write(do, auto_start=True)
            if self._mqtt is not None:
                nidaqmx_logger.debug(self._name + ' Publish DO.')
                self._mqtt.publish_topic(self._name + '/DO', do, qos=0, retain=True, expand=self._expand)
                pass
        except Exception as e:
            nidaqmx_logger.exception(self._name + ' DAQmxDoPwm __initialize() Initializing connection: ' + self._resource)
            nidaqmx_logger.exception(self._name + ' DAQmxDoPwm Exception caught: ' + str(e))
            raise e
        pass

    def __close(self):
        """Set instrument to save mode and close connection."""
        nidaqmx_logger.debug(self._name + ' DAQmxDoPwm __close() Closing nidaqmx task.')
        if self._task:
            do = self._zeros
            self._task.write(do, auto_start=True)
            if self._mqtt is not None:
                nidaqmx_logger.debug(self._name + ' DAQmxDoPwm Publish DO.')
                self._mqtt.publish_topic(self._name + '/DO', do, qos=0, retain=True, expand=self._expand)
                pass
            self._task.close()
            self._task = None
        pass

    # Protected override methods from super Actor
    def _on_connect(self):
        """
        Called from _on_receive() when message was received from paho.mqtt.client callback message on_connect.

        Perform action on MQTT connect here, e.g. publish and subscribe topics.

        Returns
        -------
        None.

        """
        nidaqmx_logger.debug(self._name + ' DAQmxDoPwm _on_connect() Received MQTT connected message. Publish and subscribe...')
        try:
            if self._mqtt is not None:
                nidaqmx_logger.debug(self._name + ' DAQmxDoPwm Publish DO, Enabled, Ratios.')
                self._mqtt.publish_topic(self._name + '/DO', self._do, qos=0, retain=True, expand=self._expand)
                self._mqtt.publish_topic(self._name + '/Enabled', self._enabled, qos=0, retain=True, expand=self._expand)
                self._mqtt.publish_topic(self._name + '/Forced', self._forced, qos=0, retain=True, expand=self._expand)
                self._mqtt.publish_topic(self._name + '/PWMRatios', self._ratios, qos=0, retain=True, expand=self._expand)
                prefixed_topics = ()
                for topic in self._daqmxdopwm_subscriptions:
                    prefixed_topic = self._name + '/' + topic
                    logging.debug(self._name + ' DAQmxDoPwm _on_connect() Publishing:' + str(prefixed_topic))
                    prefixed_topics = prefixed_topics + (prefixed_topic,)
                    self._mqtt.publish_topic(prefixed_topic, '', qos=0, retain=True, expand=False)
                nidaqmx_logger.debug(self._name + ' DAQmxDoPwm _on_connect() Subscribing to: ' + str(prefixed_topics))
                rcs_mids = self._mqtt.subscribe_topics(self._in_future, prefixed_topics, qos=0).get()
                nidaqmx_logger.debug(self._name + ' DAQmxDoPwm _on_connect() Subscribing to: ' + str(prefixed_topics) + ' returned rcs, mids = ' + str(rcs_mids))
                pass
        except Exception as e:
            nidaqmx_logger.exeption(self._name + ' DAQmxDoPwm _on_connect(): Exception cought:')
            nidaqmx_logger.exception(self._name + ' DAQmxDoPwm _on_connect(): Exception: ' + str(e))
        super()._on_connect()
        pass

    def _on_message(self, topic, msg):
        """https://www.advantech.com/en/products/data-acquisition-(daq)/sub_1-2mljza
        Called from Mqtt._on_receive() when message was received from paho.mqtt.client callback message on_message.

        Perform action on MQTT messages here or forward to super.

        Returns
        -------
        None.

        """
        nidaqmx_logger.debug(self._name + ' DAQmxDoPwm _on_message() Received MQTT topic.' + topic + ' msg: ' + str(msg))
        sub_topics = re.split('/', topic)
        match sub_topics[2]:
            case 'Set-ChannelDO':
                try:
                    nidaqmx_logger.debug(self._name + ' DAQmxDoPwm _on_message() Set-ChannelDO received, msg=' + str(msg.strip()))
                    value = json.loads(msg)
                    channel = int(value['channel'])
                    state = value['state']
                    self.setChannelDO(channel, state)
                except Exception as e:
                    nidaqmx_logger.debug(self._name + ' DAQmxDoPwm _on_message() Set-ChannelDO ignored due to exception: ' + str(e))
                    pass
            case 'Set-ChannelEnable':
                try:
                    nidaqmx_logger.debug(self._name + ' DAQmxDoPwm _on_message() Set-ChannelEnabled received, msg=' + str(msg.strip()))
                    value = json.loads(msg)
                    channel = int(value['channel'])
                    enabled = value['enable']
                    self.setChannelEnabled(channel, enabled)
                except Exception as e:
                    nidaqmx_logger.debug(self._name + ' DAQmxDoPwm _on_message() Set-ChannelEnabled ignored due to exception: ' + str(e))
                    pass
            case 'Set-ChannelForce':
                try:
                    nidaqmx_logger.debug(self._name + ' DAQmxDoPwm _on_message() Set-ChannelForced received, msg=' + str(msg.strip()))
                    value = json.loads(msg)
                    channel = int(value['channel'])
                    forced = value['force']
                    self.setChannelForced(channel, forced)
                except Exception as e:
                    nidaqmx_logger.debug(self._name + ' DAQmxDoPwm _on_message() Set-ChannelForced ignored due to exception: ' + str(e))
                    pass
            case 'Set-ChannelPWMRatio':
                try:
                    nidaqmx_logger.debug(self._name + ' DAQmxDoPwm _on_message() Set-ChannelPWMRatio received, msg=' + str(msg.strip()))
                    value = json.loads(msg)
                    channel = int(value['channel'])
                    ratio = value['ratio']
                    self.setChannelPWMRatio(channel, ratio)
                except Exception as e:
                    nidaqmx_logger.debug(self._name + ' DAQmxDoPwm _on_message() Set-ChannelPWMRatio ignored due to exception: ' + str(e))
                    pass
            case 'Set-DO':
                try:
                    nidaqmx_logger.debug(self._name + ' DAQmxDoPwm _on_message() Set-DO received, msg=' + str(msg.strip()))
                    if len(msg) > 2:
                        value = np.array(json.loads(msg))
                        self.setDO(value)
                    else:
                        nidaqmx_logger.debug(self._name + ' DAQmxDoPwm _on_message() Set-DO ignored. len(msg)=' + str(len(msg)))
                        pass
                except Exception as e:
                    nidaqmx_logger.debug(self._name + ' DAQmxDoPwm _on_message() Set-DO ignored due to exception: ' + str(e))
                    pass
            case 'Set-Enabled':
                try:
                    nidaqmx_logger.debug(self._name + ' DAQmxDoPwm _on_message() Set-Enabled received, msg=' + str(msg.strip()))
                    if len(msg) > 2:
                        value = np.array(json.loads(msg))
                        self.setEnabled(value)
                    else:
                        nidaqmx_logger.debug(self._name + ' DAQmxDoPwm _on_message() Set-Enabled ignored. len(msg)=' + str(len(msg)))
                        pass
                except Exception as e:
                    nidaqmx_logger.debug(self._name + ' DAQmxDoPwm _on_message() Set-Enabled ignored due to exception: ' + str(e))
                    pass
            case 'Set-Forced':
                try:
                    nidaqmx_logger.debug(self._name + ' DAQmxDoPwm _on_message() Set-Forced received, msg=' + str(msg.strip()))
                    if len(msg) > 2:
                        value = np.array(json.loads(msg))
                        self.setForced(value)
                    else:
                        nidaqmx_logger.debug(self._name + ' DAQmxDoPwm _on_message() Set-Forced ignored. len(msg)=' + str(len(msg)))
                        pass
                except Exception as e:
                    nidaqmx_logger.debug(self._name + ' DAQmxDoPwm _on_message() Set-Forced ignored due to exception: ' + str(e))
                    pass
            case 'Set-PWMRatios':
                try:
                    nidaqmx_logger.debug(self._name + ' DAQmxDoPwm _on_message() Set-PWMRatio received, msg=' + str(msg.strip()))
                    if len(msg) > 2:
                        value = np.array(json.loads(msg))
                        self.setPWMRatios(value)
                    else:
                        nidaqmx_logger.debug(self._name + ' DAQmxDoPwm _on_message() Set-PWMRatios ignored. len(msg)=' + str(len(msg)))
                        pass
                except Exception as e:
                    nidaqmx_logger.debug(self._name + ' DAQmxDoPwm _on_message() Set-PWMRatio ignored due to exception: ' + str(e))
                    pass
            case _:  # Call super
                nidaqmx_logger.debug(self._name + ' DAQmxDoPwm _on_message() Calling super() due for unknown topic: ' + topic + ' msg: ' + str(msg))
                super()._on_message(topic, msg)
        pass

    def _on_receive(self, message):
        """
        May be implemented for the actor to handle regular non-proxy messages.

        Virtual protected method.
        Override with concrete child class implementation.

        Returns
        -------
        None.

        """
        nidaqmx_logger.debug(self._name + ' DAQmxDoPwm _on_receive() call super()._on_receive(message)')
        super()._on_receive(message)
        pass

    def _on_start(self):
        """
        Hook for doing any setup that should be done after the actor is started, but before it starts processing messages.

        Virtual protected method.
        Override with concrete child class implementation.

        Returns
        -------
        None.

        """
        nidaqmx_logger.debug(self._name + ' DAQmxDoPwm _on_start() call super()._on_start()')
        super()._on_start()
        pass

    def _on_stop(self):
        """
        Hook for doing any cleanup that should be done after the actor has processed the last message, and before the actor stops.

        Close instrument.

        Virtual protected method.
        Override with concrete child class implementation.

        Returns
        -------
        None.

        """
        nidaqmx_logger.debug(self._name + ' DAQmxDoPwm _on_stop()')
        try:
            do = np.zeros(self._nof_channels, dtype=bool)
            nidaqmx_logger.debug(self._name + ' DAQmxDoPwm Set digital outputs: ' + str(do.tolist()))
            try:
                self._task.write(self._do, auto_start=True)
                if self._mqtt is not None:
                    nidaqmx_logger.debug(self._name + ' DAQmxDoPwm Publish DO.')
                    self._mqtt.publish_topic(self._name + '/DO', self._do, qos=0, retain=True, expand=self._expand)
                    pass
            except Exception as e:
                nidaqmx_logger.exception(self._name + ' DAQmxDoPwmExeption cought in SetDO.', str(e))
            if self._mqtt is not None:
                prefixed_topics = ()
                logging.debug(self._name + ' DAQmxDoPwm _on_stop() Unsubscribing: ' + str(self._device_subscriptions))
                for topic in self._daqmxdopwm_subscriptions:
                    prefixed_topic = self._name + '/' + topic
                    prefixed_topics = prefixed_topics + (prefixed_topic,)
                logging.debug(self._name + ' DAQmxDoPwm _on_stop() Unsubscribing from: ' + str(prefixed_topics))
                self._mqtt.unsubscribe_topics(self, prefixed_topics)
                pass
        except Exception as e:
            nidaqmx_logger.exception(self._name + ' DAQmxDoPwm _on_stop(): Exception cought:')
            nidaqmx_logger.exception(self._name + ' DAQmxDoPwm _on_stop(): Exception: ' + str(e))
        super()._on_stop()
        pass

    def _periodic(self, reason):
        """
        Implementation of periodic actions.

        Set DO accourding tot PWM request.

        Parameters
        ----------
        reason : PeriodicCallReason
            Reason for calling this method.

        Returns
        -------
        None.

        """
        nidaqmx_logger.debug(self._name + ' DAQmxDoPwm Writing do on slot ' + self._slot)
        # print(self._name + ' Writing do on slot ' + self._slot)
        try:
            match reason:
                case PeriodicCallReason.Start:
                    self.__initialize()
                    self._t0 = time.time()
                    pass
                case PeriodicCallReason.Do:
                    try:
                        nidaqmx_logger.debug(self._name + ' DAQmxDoPwm Write DO  Counter=' + str(self._periodic_counter))
                        self._t_diff = (time.time() - self._t0)
                        ratio = self._t_diff - int(self._t_diff)
                        try:
                            do_ratio = self._ratios > ratio
                        except TypeError as e:
                            nidaqmx_logger.exception(self._name + ' DAQmxDoPwm Exeption cought in _periodic.', str(e))
                            pass
                        do = np.logical_or(do_ratio, self._forced)
                        enabled_do = np.logical_and(do, self._enabled)
                        self._task.write(enabled_do, auto_start=True)
                        if self._mqtt is not None:
                            nidaqmx_logger.debug(self._name + ' DAQmxDoPwm Publish DO.')
                            self._mqtt.publish_topic(self._name + '/DO', enabled_do, qos=0, retain=True, expand=self._expand)
                            pass
                        time.sleep(self._sleep_time)
                        pass
                    except Exception as e:
                        if self._do_exceptions < self._max_do_exceptions:
                            self._do_exceptions += 1
                            nidaqmx_logger.warning(f'{self._name} Exeption cought in _periodic.Do, retry={self._do_exceptions}\r\n{str(e)}')
                        else:
                            nidaqmx_logger.Error(f'{self._name} Exeption cought in _periodic.Do, maximum retries(3) unsuccessful -> reinitialize nidaqmx task.\r\n{str(e)}')
                            self.initialize()
                case PeriodicCallReason.Stop:
                    self.__close()
                    pass
        except Exception as e:
            nidaqmx_logger.exception(self._name + ' DAQmxDoPwm Exeption cought in _periodic.', str(e))
        pass

    # Protected override methods from super Device
    def _close(self):
        """
        Set instrument to save mode and close connection.

        Returns
        -------
        int
            Error code.
        """
        nidaqmx_logger.debug(self._name + ' DAQmxDoPwm _close() Closing device connection.')
        if self._task:
            do = self._zeros
            self._task.write(do, auto_start=True)
            if self._mqtt is not None:
                nidaqmx_logger.debug(self._name + ' DAQmxDoPwm Publish DO.')
                self._mqtt.publish_topic(self._name + '/DO', do, qos=0, retain=True, expand=self._expand)
                pass
            self._task.close()
            self._task = None
        return

    def _id_query(self):
        """
        Perform id query of instrument.

        Returns
        -------
        string
            Instrument ID.

        """
        nidaqmx_logger.debug(self._name + ' DAQmxDoPwm _id_query() Performing id query.')
        device = nidaqmx.system.System.local().devices[self._slot[1:]]
        id = str(device.product_type) + '|' + str(device.product_num)
        return id

    def _initalize(self,
                   resource=None,
                   id_query_flag=True,
                   reset_flag=True,
                   reset_options=None,
                   selftest_flag=True):
        """
        Open device connection and initialize instrument.

        Parameters
        ----------
        resource : string
            Resource identifier.
        id_query_flag : bool
            Flag requesting an id_query. Default isTrue.
        reset_flag : bool
            Flag requesting a reset. Default is True.
        reset_options : string
            String containing reset options. Default is None.
        selftest_flag : bool
            Flag requesting an id_query. Default is True.

        Returns
        -------
        int
            Error code.

        """
        if self._task:
            self._close()
        nidaqmx_logger.debug(self._name + ' DAQmxDoPwm _initialize() Initializing connection.')
        try:
            if reset_flag:
                self.reset(reset_options)
            if id_query_flag:
                self.id_query()
            self.revision_query()
            if selftest_flag:
                self.selftest()
            # Create task and set all DO false.
            self._do_exceptions = 0
            self._task = nidaqmx.Task(self._slot[1:])
            self._task.do_channels.add_do_chan(
                self._do_lines,
                line_grouping=LineGrouping.CHAN_PER_LINE)
            do = self._zeros
            self._task.write(do, auto_start=True)
            if self._mqtt is not None:
                nidaqmx_logger.debug(self._name + ' Publish DO.')
                self._mqtt.publish_topic(self._name + '/DO', do, qos=0, retain=True, expand=self._expand)
                pass
        except Exception as e:
            nidaqmx_logger.exception(self._name + ' DAQmxDoPwm _initialize() Initializing connection: ' + resource)
            nidaqmx_logger.exception(self._name + ' DAQmxDoPwm Exception caught: ' + str(e))
            raise e
        pass

    def _reset(self, reset_options=None):
        """
        Reset instrument with options.

        Parameters
        ----------
        reset_options : string
            String containing reset options. Default is None.

        Returns
        -------
        string
            Reset result.

        """
        nidaqmx_logger.warning(self._name + ' DAQmxDoPwm _reset() Performing reset.')
        try:
            nidaqmx.system.System.local().devices[self._slot[1:]].reset_device()
            # response = "OK"
            response = "successful"
        except nidaqmx.errors.DaqError as e:
            response = "Failed"
            nidaqmx_logger.exception('Reset failed. ' + str(e))
        return response

    def _revision_query(self):
        """
        Perform instrument revision query, firmware and hardware.

        Returns
        -------
        string
            Firmware revision.
        string
            Hardware revision.

        """
        nidaqmx_logger.debug(self._name + ' DAQmxDoPwm _revision_query() Performing revision query.')
        fw_version = nidaqmx.system.System.local().driver_version
        subs = re.split(',', str(fw_version))
        fw = ''
        for sub in subs:
            vs = re.search('\d+', sub).group(0)
            fw = fw + '.' + vs
        hw = 'NA'
        return fw[1:], hw

    def _selftest(self):
        """
        Perform instrument selftest.

        Returns
        -------
        int
            Selftest return code.
        string
            Selftest message.

        """
        nidaqmx_logger.warning(self._name + ' DAQmxDoPwm _selftest() Performing selftest.')
        try:
            nidaqmx.system.System.local().devices[self._slot[1:]].self_test_device()
            code = 0
            # message = 'OK'
            message = 'Successful'
        except nidaqmx.errors.DaqError as e:
            code = -1
            message = 'Failed'
            nidaqmx_logger.exception('Selftest failed. ' + str(e))
        return code, message

    def _sn_query(self):
        """
        Perform instrument serial number query.

        Returns
        -------
        string
            Serial number.

        """
        nidaqmx_logger.debug(self._name + ' DAQmxDoPwm _sn_query() Performing serial number query.')
        sn = str(nidaqmx.system.System.local().devices[self._slot[1:]].serial_num)
        return sn

    def _sw_revision(self):
        """
        Return revision of this class inclusive parents.

        Returns
        -------
        string
            Software revision.

        """
        return 'DAQmxDoPwm_0.0.1.0|' + super()._sw_revision()

    # Public methods of this class
    # Getter
    def getDO(self):
        """Return DO state."""
        return self._do

    def getEnabled(self):
        """Return enabled state."""
        return self._enabled

    def getForced(self):
        """Return enabled state."""
        return self._forced

    def getRatios(self):
        """Return PWM ratios."""
        return self._ratios

    # Setter
    def setChannelDO(self, channel, state):
        """Set DO state of channel, if periodic action is not active."""
        if not self._periodic_active:
            nidaqmx_logger.debug(self._name + ' DAQmxDoPwm Set digital output for channel: ' + str(channel) + ' ' + str(state))
            self._do[channel] = state and self._enabled[channel]
            try:
                nidaqmx_logger.debug(self._name + ' DAQmxDoPwm Set digital outputs: ' + str(self._do.tolist()))
                self._task.write(self._do, auto_start=True)
                if self._mqtt is not None:
                    nidaqmx_logger.debug(self._name + ' Publish DO.')
                    self._mqtt.publish_topic(self._name + '/DO', self._do, qos=0, retain=True, expand=self._expand)
                    pass
            except Exception as e:
                nidaqmx_logger.exception(self._name + ' DAQmxDoPwm Exeption cought in SetChannelDO.', str(e))
        else:
            nidaqmx_logger.warning(self._name + ' DAQmxDoPwm Set digital outputs ignored. Periodic is active.')
            pass
        return self._enabled

    def setChannelEnabled(self, channel, enabled):
        """Set enabled state for channel."""
        nidaqmx_logger.debug(self._name + ' DAQmxDoPwm SetChannelEnabled: ' + str(channel) + ' ' + str(enabled))
        self._enabled[channel] = enabled
        if self._mqtt is not None:
            nidaqmx_logger.debug(self._name + ' DAQmxDoPwm Publish Enabled.')
            self._mqtt.publish_topic(self._name + '/Enabled', self._enabled, qos=0, retain=True, expand=self._expand)
            pass
        pass

    def setChannelForced(self, channel, forced):
        """Set forced state for channel."""
        nidaqmx_logger.debug(self._name + ' DAQmxDoPwm SetChannelForced: ' + str(channel) + ' ' + str(forced))
        self._forced[channel] = forced
        if self._mqtt is not None:
            nidaqmx_logger.debug(self._name + ' DAQmxDoPwm Publish Forced.')
            self._mqtt.publish_topic(self._name + '/Forced', self._forced, qos=0, retain=True, expand=self._expand)
            pass
        pass

    def setChannelPWMRatio(self, channel, ratio):
        """Set PWM ratio channel."""
        nidaqmx_logger.debug(self._name + ' DAQmxDoPwm SetPWMChannelRatio: ' + str(channel) + ' ' + str(ratio))
        self._ratios[channel] = ratio
        if self._mqtt is not None:
            nidaqmx_logger.debug(self._name + ' DAQmxDoPwm Publish Ratios.')
            self._mqtt.publish_topic(self._name + '/PWMRatio', self._ratios, qos=0, retain=True, expand=self._expand)
            pass
        pass

    def setDO(self, do):
        """Set DO state, if periodic action is not active."""
        if not self._periodic_active:
            nidaqmx_logger.debug(self._name + ' DAQmxDoPwm Set digital outputs: ' + str(do.tolist()))
            self._do = np.logical_and(do, self._enabled)
            try:
                self._task.write(self._do, auto_start=True)
                if self._mqtt is not None:
                    nidaqmx_logger.debug(self._name + ' DAQmxDoPwm Publish DO.')
                    self._mqtt.publish_topic(self._name + '/DO', self._do, qos=0, retain=True, expand=self._expand)
                    pass
            except Exception as e:
                nidaqmx_logger.exception(self._name + ' DAQmxDoPwmExeption cought in SetDO.', str(e))
                pass
        else:
            nidaqmx_logger.warning(self._name + ' DAQmxDoPwm Set digital outputs ignored. Periodic is active.')
            pass
        return self._enabled

    def setEnabled(self, enabled):
        """Set enabled state for channel."""
        nidaqmx_logger.debug(self._name + ' DAQmxDoPwm SetEnabled: ' + str(enabled.tolist()))
        self._enabled = np.copy(enabled)
        if self._mqtt is not None:
            nidaqmx_logger.debug(self._name + ' DAQmxDoPwm Publish Enabled.')
            self._mqtt.publish_topic(self._name + '/Enabled', self._enabled, qos=0, retain=True, expand=self._expand)
            pass
        pass

    def setForced(self, forced):
        """Set forced state for channel."""
        nidaqmx_logger.debug(self._name + ' DAQmxDoPwm SetForced: ' + str(forced.tolist()))
        self._forced = np.copy(forced)
        if self._mqtt is not None:
            nidaqmx_logger.debug(self._name + ' DAQmxDoPwm Publish Forced.')
            self._mqtt.publish_topic(self._name + '/Forced', self._forced, qos=0, retain=True, expand=self._expand)
            pass
        pass

    def setPWMRatios(self, ratios):
        """Set PWM ratios."""
        nidaqmx_logger.debug(self._name + ' DAQmxDoPwm SetPWMRatios: ' + str(ratios.tolist()))
        self._ratios = np.copy(ratios)
        if self._mqtt is not None:
            nidaqmx_logger.debug(self._name + ' DAQmxDoPwm Publish Ratios.')
            self._mqtt.publish_topic(self._name + '/PWMRatios', self._ratios, qos=0, retain=True, expand=self._expand)
            pass
        pass

    pass  # end of DAQmxDoPwm class definition


if __name__ == '__main__':
    print("daqmx_ai.py\n\
Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH\n\
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany\n\
eMail: H.Brand@gsi.de\n\
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme")
    app_name = re.split('.py', os.path.basename(__file__))[0]
    # Logging setup
    print('Configuring logging...')
    # Define logger to be used
    # logging.basicConfig(  # filename='example.log', filemode='w', encoding='utf-8', level=logging.INFO, style='{', format='{asctime} [{levelname:8}] {process} {thread} {module} {funcName} {lineno} {message}')
    logger = logging.getLogger('App-Logger')
    logger.setLevel(logging.DEBUG)
    pyacdaq_logger = logging.getLogger('pyacdaq')
    pyacdaq_logger.setLevel(logging.WARNING)
    nidaqmx_logger.setLevel(logging.INFO)
    # Define logging formatter
    logging_formatter = logging.Formatter(style='{', fmt='{asctime} [{levelname:8}] {module} {funcName} {lineno} {message}')
    # logging_formatter = logging.Formatter(style='{', fmt='{asctime} [{levelname:8}] {process} {thread} {module} {funcName} {lineno} {message}')
    # configure_logging_handler((logger, pyacdaq_logger), logging_formatter, severity={'console': None, 'file': None, 'mqtt': None, 'syslog': None}, mqtt={'broker': 'ee-raspi1001', 'topic': 'ACDAQ/log_msg', 'user': '', 'password': ''})
    test = False
    try:
        mqtt_broker = input('MQTT Broker? (localhost)>')
        if len(mqtt_broker) == 0:
            mqtt_broker = 'localhost'
        mqtt_user = input('MQTT Username: ')
        if len(mqtt_user) == 0:
            mqtt_user = None
        mqtt_password = getpass.getpass()
        if len(mqtt_password) == 0:
            mqtt_password = None
        configure_logging_handler(app_name, (logger, pyacdaq_logger, nidaqmx_logger), logging_formatter, severity={'console': logging.INFO, 'file': logging.DEBUG, 'mqtt': logging.INFO, 'syslog': logging.WARNING}, mqtt={'broker': mqtt_broker, 'topic': 'ACDAQ/log_msg', 'user': mqtt_user, 'password': mqtt_password})
        print(__name__, 'Launching for MQTT actor...')
        # Create MQTT actor proxy
        p_mqtt = Mqtt.start('Mqtt',
                        start_periodic=False,
                        broker=mqtt_broker,
                        port=1883,
                        client_id='ACDAQ',
                        clean_session=True,
                        retain=True,
                        will='offline',
                        username=mqtt_user,
                        password=mqtt_password
                        ).proxy()

        # Create device proxy
        print(__name__, 'Launching device actors(s)...')
        time.sleep(5)
        if test:
            a_device = DAQmxDoPwm.start('DAQmxDoPwm',
                                     start_periodic=True,
                                     mqtt=p_mqtt,
                                     resource='/Dev1',
                                     id_query_flag=True,
                                     reset_flag=True,
                                     reset_options='',
                                     selftest_flag=False,
                                     expand=True,
                                     nof_channels=8
                                     )
            p_device = a_device.proxy()
            device_name = p_device.getName().get()
            device_birthday = p_device.getBirthday().get()
            device_periodic = p_device.getPeriodicStatus().get()
            device_sw_revision = p_device.getSWRevision().get()
            device_instrument_id = p_device.getInstrumentID().get()
            device_firmware_revision = p_device.getFirmwareRevision().get()
            device_hardware_revision = p_device.getHardwareRevision().get()
            device_serial_number = p_device.getSerialNumber().get()
            print(__name__, 'Name:', device_name,
                  '; Birthday:', device_birthday,
                  '; Periodic:', device_periodic,
                  '; SW:', device_sw_revision,
                  '; ID:', device_instrument_id,
                  '; FW:', device_firmware_revision,
                  '; HW:', device_hardware_revision,
                  '; SN:', device_serial_number)
        else:
            PXI1Slot6 = DAQmxDoPwm.start('PXI1Slot6',
                                         start_periodic=True,
                                         mqtt=p_mqtt,
                                         expand=True,
                                         resource='/PXI1Slot6',
                                         id_query_flag=True,
                                         reset_flag=False,
                                         reset_options='',
                                         selftest_flag=False,
                                         nof_channels=64
                                         )
            p_PXI1Slot6 = PXI1Slot6.proxy()
            PXI1Slot7 = DAQmxDoPwm.start('PXI1Slot7',
                                         start_periodic=True,
                                         mqtt=p_mqtt,
                                         expand=True,
                                         resource='/PXI1Slot7',
                                         id_query_flag=True,
                                         reset_flag=False,
                                         reset_options='',
                                         selftest_flag=False,
                                         nof_channels=64
                                         )
            p_PXI1Slot7 = PXI1Slot7.proxy()
            PXI1Slot8 = DAQmxDoPwm.start('PXI1Slot8',
                                         start_periodic=True,
                                         mqtt=p_mqtt,
                                         expand=True,
                                         resource='/PXI1Slot8',
                                         id_query_flag=True,
                                         reset_flag=False,
                                         reset_options='',
                                         selftest_flag=False,
                                         nof_channels=64
                                         )
            p_PXI1Slot8 = PXI1Slot8.proxy()
            PXI1Slot9 = DAQmxDoPwm.start('PXI1Slot9',
                                         start_periodic=True,
                                         mqtt=p_mqtt,
                                         expand=True,
                                         resource='/PXI1Slot9',
                                         id_query_flag=True,
                                         reset_flag=False,
                                         reset_options='',
                                         selftest_flag=False,
                                         nof_channels=64
                                         )
            p_PXI1Slot9 = PXI1Slot9.proxy()
            time.sleep(5)
            print(__name__, 'Set ratios and enable channels.')
            p_PXI1Slot9.setPWMRatios(np.linspace(0.05, 0.95, 64, endpoint=False, dtype=np.float64))
            p_PXI1Slot9.setEnabled(np.ones(64, dtype=bool))
            p_PXI1Slot9.periodic_action(True)
            p_PXI1Slot8.setPWMRatios(np.linspace(0.05, 0.95, 64, endpoint=False, dtype=np.float64))
            p_PXI1Slot8.setEnabled(np.ones(64, dtype=bool))
            p_PXI1Slot8.periodic_action(True)
            p_PXI1Slot7.setPWMRatios(np.linspace(0.05, 0.95, 64, endpoint=False, dtype=np.float64))
            p_PXI1Slot7.setEnabled(np.ones(64, dtype=bool))
            p_PXI1Slot7.periodic_action(True)
            p_PXI1Slot6.setPWMRatios(np.linspace(0.05, 0.95, 64, endpoint=False, dtype=np.float64))
            p_PXI1Slot6.setEnabled(np.ones(64, dtype=bool))
            p_PXI1Slot6.periodic_action(True)
            # time.sleep(10)
            while input(__name__ + ' Stop? (default: n or stop)>').find('stop') < 0:
                pass
    except Exception as e:
        print(__name__, 'Exception caught:', e)
    finally:
        print(__name__, 'Stopping actors...')
        if test:
            p_device.stop()
        else:
            p_PXI1Slot6.stop()
            p_PXI1Slot7.stop()
            p_PXI1Slot8.stop()
            p_PXI1Slot9.stop()
        time.sleep(3)
        p_mqtt.stop()
    logging.shutdown()
    print(__name__, 'Done.')
