#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Comandline Interpreter Example.

Created on Mon Aug 14 16:57:21 2023
@author: H.Brand@gsi.de
"""

import cmd


class VhsCli(cmd.Cmd):
    """VHS Commandline Interpreter class."""

    def __init__(self, vhs=None):
        super().__init__()
        self.intro = 'Welcome to the VHS Commandline Interpreter 0.0.1.0. Type help or ? to list commands.\n'
        self.prompt = '\r\n==> '
        self._vhs = vhs
        pass

    def _parse(self, prm):
        'Convert arg to list'
        return prm.split(' ')

    def do_exit(self, prm):
        print('Exit command received. Stopping program...')
        return True

    def do_help(self, prm):
        print('VHS Commandline Interpreter. Following commands are available:')
        print('\texit: stop program.')
        print('\tenable channel 0/1')
        print('\tforce channel 0/1')
        print('\tprofile absolute path')
        print('\tpwmr channel ratio/%')
        print('\tspt channel T/°C')
        print('\trt transition: Request state transition. Allowed transitions:')
        transitions = 'request_control|release_control|start_profile|stop_profile|pause_profile|continue_profile|cooling_controlled|cooling_manual|controlled_cooling|manual_cooling|profile_cooling|pause_cooling|request_stop'.split('|')
        for t in transitions:
            print(f'\t\t{t}')
        return False

    def do_rt(self, prm):
        print(f'State change requested: {prm}')
        if self._vhs:
            self._vhs.tell('rt ' + prm)
        return False

    def do_enable(self, prm):
        print(f'Enable channel: {prm}')
        if self._vhs:
            self._vhs.tell('enable ' + prm)
        return False

    def do_force(self, prm):
        print(f'Force channel: {prm}')
        if self._vhs:
            self._vhs.tell('force ' + prm)
        return False

    def do_profile(self, prm):
        print(f'Profile: {prm}')
        if self._vhs:
            self._vhs.tell('profile ' + prm)
        return False

    def do_pwmr(self, prm):
        print(f'PWMRatio channel: {prm}%')
        if self._vhs:
            self._vhs.tell('pwmr ' + prm)
        return False

    def do_spt(self, prm):
        print(f'SPT channel: {prm}°C')
        if self._vhs:
            self._vhs.tell('spt ' + prm)
        return False

    pass  # End of class VhsCli

# http://localhost:8080/Request_State_Change?request=[request_control|release_control|start_profile|stop_profile|pause_profile|continue_profile|cooling_controlled|cooling_manual|controlled_cooling|manual_cooling|profile_cooling|pause_cooling|request_stop]\n\


if __name__ == '__main__':
    try:
        vhs_cli = VhsCli(vhs=None)
        vhs_cli.cmdloop()
    except Exception as e:
        print(e)
    finally:
        print('Program stopped.')
