#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Develop VhsSM statemaschine.

Lizenziert unter EUPL V. 1.2

Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme

- Known issues
"""
import numpy as np
import os
from statemachine import State
from statemachine import StateMachine

match os.name:
    case 'nt':
        os.environ["PATH"] += os.pathsep + 'C:\Program Files\Graphviz\bin'
    case _:
        pass


class VhsSm(StateMachine):
    """Statemachine for the Vhsclass."""
    # Define states
    initializing = State(initial=True)
    manual = State()
    controlled = State()
    profile = State()
    paused = State()
    cooling = State()
    stopped = State(final=True)
    # Define transitions
    to_manual = initializing.to(manual)
    request_control = manual.to(controlled)
    release_control = controlled.to(manual)
    start_profile = controlled.to(profile)
    stop_profile = profile.to(controlled)
    pause_profile = profile.to(paused)
    continue_profile = paused.to(profile)
    cooling_controlled = cooling.to(controlled)
    cooling_manual = cooling.to(manual)
    controlled_cooling = controlled.to(cooling)
    manual_cooling = manual.to(cooling)
    profile_cooling = profile.to(cooling)
    pause_cooling = paused.to(cooling)
    request_stop = manual.to(stopped)

    def __init__(self, vhs_in_future=None, nof_channels=0, history=False):
        self._history = history
        self.calls = []
        self._nof_channels = nof_channels
        self._vhs_in_future = vhs_in_future
        super().__init__()

    def on_enter_initializing(self):
        if self._history:
            self.calls.append("on_enter_initializing")
        if self._vhs_in_future:
            self._vhs_in_future.setDO(np.zeros(self._nof_channels, dtype=bool))
        self.send('to_manual')

    def on_enter_manual(self):
        if self._history:
            self.calls.append("on_enter_manual")
        if self._vhs_in_future:
            self._vhs_in_future.setPWMRatios(np.zeros(self._nof_channels, dtype=np.float64))

    def on_exit_manual(self):
        if self._history:
            self.calls.append("on_exit_manual")

    def on_enter_controlled(self):
        if self._history:
            self.calls.append("on_enter_controlled")

    def on_exit_controlled(self):
        if self._history:
            self.calls.append("on_exit_controlled")

    def on_enter_profile(self):
        if self._history:
            self.calls.append("on_enter_profile")
        if self._vhs_in_future:
            self._vhs_in_future.start_profile()

    def on_exit_profile(self):
        if self._history:
            self.calls.append("on_exit_profile")

    def on_enter_paused(self):
        if self._history:
            self.calls.append("on_enter_paused")
        if self._vhs_in_future:
            self._vhs_in_future.pause_profile()

    def on_exit_paused(self):
        if self._history:
            self.calls.append("on_exit_paused")
        if self._vhs_in_future:
            self._vhs_in_future.exit_pause()

    def on_enter_cooling(self):
        if self._history:
            self.calls.append("on_enter_cooling")
        if self._vhs_in_future:
            self._vhs_in_future.cool_down()

    def on_exit_cooling(self):
        if self._history:
            self.calls.append("on_exit_cooling")

        if self._vhs_in_future:
            self._vhs_in_future.setPWMRatios(np.zeros(self._nof_channels, dtype=np.float64))

    def on_enter_stopped(self):
        if self._history:
            self.calls.append("on_enter_stopped")

    def on_transition_manual_controlled(self):
        if self._history:
            self.calls.append('on_transition_manual_controlled')

    def on_transition_manual_stopped(self):
        if self._history:
            self.calls.append('on_transition_manual_stopped')

    def on_transition_controlled_cooling(self):
        if self._history:
            self.calls.append('on_transition_controlled_cooling')

    def on_transition_manual_cooling(self):
        if self._history:
            self.calls.append('on_transition_manual_cooling')

    def on_transition_controlled_manual(self):
        if self._history:
            self.calls.append('on_transition_controlled_manual')

    def on_transition_controlled_profile(self):
        if self._history:
            self.calls.append('on_transition_controlled_profile')

    def on_transition_profile_controlled(self):
        if self._history:
            self.calls.append('on_transition_profile_controlled')

    def on_transition_profile_cooling(self):
        if self._history:
            self.calls.append('on_transition_profile_cooling')

    def on_transition_profile_paused(self):
        if self._history:
            self.calls.append('on_transition_profiles_paused')

    def on_transition_paused_profile(self):
        if self._history:
            self.calls.append('on_transition_paused_profile')

    def on_transition_pause_cooling(self):
        if self._history:
            self.calls.append('on_transition_pause_cooling')

    def on_transition_cooling_controlled(self):
        if self._history:
            self.calls.append('on_transition_cooling_controlled')

    def on_transition_cooling_manual(self):
        if self._history:
            self.calls.append('on_transition_cooling_manual')


if __name__ == '__main__':
    print("vhs.py\n\
Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH\n\
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany\n\
eMail: H.Brand@gsi.de\n\
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme")
    sm = VhsSm(history=True)
    print(sm.current_state)
    sm.request_control()
    print(sm.current_state)
    sm.release_control()
    print(sm.current_state)
    sm.request_control()
    print(sm.current_state)
    sm.start_profile()
    print(sm.current_state)
    sm.stop_profile()
    print(sm.current_state)
    sm.start_profile()
    print(sm.current_state)
    sm.pause_profile()
    print(sm.current_state)
    sm.continue_profile()
    print(sm.current_state)
    sm.profile_cooling()
    print(sm.current_state)
    sm.cooling_manual()
    print(sm.current_state)
    sm.request_control()
    print(sm.current_state)
    sm.start_profile()
    print(sm.current_state)
    sm.pause_profile()
    print(sm.current_state)
    sm.pause_cooling()
    print(sm.current_state)
    sm.cooling_manual()
    print(sm.current_state)
    sm.manual_cooling()
    print(sm.current_state)
    sm.cooling_controlled()
    print(sm.current_state)
    sm.release_control()
    print(sm.current_state)
    sm.manual_cooling()
    print(sm.current_state)
    sm.cooling_manual()
    print(sm.current_state)
    sm.request_stop()
    print(sm.current_state)
    print(sm.calls)
