#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Derived actor class template.

- Replace FuzzyController with you class name.
- Replace fuzzy_controller with instance name.

Lizenziert unter EUPL V. 1.2

Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme

- Known issues
"""

import getpass
import logging
import matplotlib.pyplot as plt
import multiprocessing as mp
import numpy as np
import os
import re
import time
import simpful as sf
from pyacdaq.logger import configure_logging_handler
from pyacdaq.actor.actor import Actor as Actor
from pyacdaq.actor.actor import PeriodicCallReason as PeriodicCallReason
from pyacdaq.actor.mqtt import Mqtt as Mqtt

vhs_logger = logging.getLogger('vhs')


def create_vhs_fs(mode='mamdani'):  # mode='mamdani' or 'sugeno'
    # Create a fuzzy system object for VHC.
    FS = sf.FuzzySystem()
    FS = create_vhs_fs_input(FS)
    FS = create_vhs_fs_output(FS, mode)
    FS = create_vhs_fs_rules(FS)
    return FS


def create_vhs_fs_input(FS):
    """Define fuzzy sets and linguistic variables."""
    T_1 = sf.TrapezoidFuzzySet(a=-25., b=-25., c=-20., d=-0, term='negative')
    T_2 = sf.TrapezoidFuzzySet(a=-20., b=0., c=0., d=20., term='constant')
    T_3 = sf.TrapezoidFuzzySet(a=0., b=20., c=25., d=25., term='positive')
    # sf.LinguisticVariable([T_1, T_2, T_3], universe_of_discourse=[-25, 25]).plot()
    FS.add_linguistic_variable('TDiff', sf.LinguisticVariable([T_1, T_2, T_3], concept='Temperature-Difference', universe_of_discourse=[-25, 25]))

    G_1 = sf.TrapezoidFuzzySet(a=-25., b=-25., c=-20., d=-10., term='negative')
    G_2 = sf.TrapezoidFuzzySet(a=-20., b=-10., c=0., d=20., term='constant')
    G_3 = sf.TrapezoidFuzzySet(a=0., b=20, c=25, d=25, term='positive')
    # sf.LinguisticVariable([G_1, G_2, G_3], universe_of_discourse=[-25, 25]).plot()
    FS.add_linguistic_variable('TGain', sf.LinguisticVariable([G_1, G_2, G_3], concept='Temperature-Gain', universe_of_discourse=[-25, 25]))
    return FS


def create_vhs_fs_output(FS, mode='mamdani'):
    """Define VHS Fuzzy System Output."""
    match mode:
        case 'mamdani':
            # Define output fuzzy sets and linguistic variable
            P_1 = sf.TrapezoidFuzzySet(a=1., b=100., c=100., d=100., term='on')
            # P_1 = sf.FuzzySet(points=[[0.0, 0.0], [1.0, 0.0], [10.0, .1], [30.0, 0.2], [50.0, 0.6], [75.0, 0.9], [90.0, 0.95], [100.0, 1.0]], term="on")
            P_2 = sf.CrispSet(a=0, b=1, term="off")
            # P_2 = sf.TrapezoidFuzzySet(a=0., b=0., c=1., d=2., term='off')
            # sf.LinguisticVariable([P_1], universe_of_discourse=[0, 120]).plot()
            FS.add_linguistic_variable('Power', sf.LinguisticVariable([P_1, P_2], concept='Power', universe_of_discourse=[0, 100]))
            pass
        case 'sugeno':
            # Define output functions
            FS.set_output_function('on', '-TDiff*2 - TGain*2')
            FS.set_crisp_output_value('off', 0.)
            pass
        case _:
            pass
    return FS


def create_vhs_fs_rules(FS):
    """Define VHS Fuzzy System Rules."""
    R0 = 'IF (TDiff IS constant) AND (TGain IS negative) THEN (Power IS on) WEIGHT 1.0'
    R1 = 'IF (TDiff IS constant) AND (TGain IS constant) THEN (Power IS off WEIGHT 1.0'
    R2 = 'IF (TDiff IS constant) AND (TGain IS positive) THEN (Power IS off) WEIGHT 1.0'
    R3 = 'IF (TDiff IS positive) AND (TGain IS negative) THEN (Power IS off) WEIGHT 1.0'
    R4 = 'IF (TDiff IS positive) AND (TGain IS constant) THEN (Power IS off) WEIGHT 1.0'
    R5 = 'IF (TDiff IS positive) AND (TGain IS positive) THEN (Power IS off) WEIGHT 1.0'
    R6 = 'IF (TDiff IS negative) AND (TGain IS negative) THEN (Power IS on) WEIGHT 1.0'
    R7 = 'IF (TDiff IS negative) AND (TGain IS constant) THEN (Power IS on) WEIGHT 1.0'
    R8 = 'IF (TDiff IS negative) AND (TGain IS positive) THEN (Power IS off) WEIGHT 1.0'
    R = [R0, R1, R2, R3, R4, R5, R6, R7, R8]
    FS.add_rules(R)
    return FS, R


class FuzzyProcess(mp.Process):
    """Derived class performing performing fuzzy caluclations in process."""

    def __init__(self, name,
                 fuzzy_interference_mode=None,
                 strength=False,
                 mpq_in=None,
                 mpq_ret=None
                 ):
        """
        Initialize this instance.

        Arguments
        ---------
        name -- Name of instance
        """
        super().__init__()
        self._fs = None
        self._name = name
        self._mpq_in = mpq_in
        self._mpq_ret = mpq_ret
        vhs_logger.debug(f'{self._name} __init__() mpq_in={self._mpq_in} mpq_ret={self._mpq_ret}')
        self._strength = strength
        if not fuzzy_interference_mode:
            self._fuzzy_interference_mode = 'mamdani'
        else:
            self._fuzzy_interference_mode = fuzzy_interference_mode
        # Create a fuzzy system object for VHS.
        self._fs, self._rules = self._create_vhs_fs(mode=self._fuzzy_interference_mode)
        pass

    # Private fuzzy system methods
    def _create_vhs_fs(self, mode='mamdani'):  # mode='mamdani' or 'sugeno'
        # Create a fuzzy system object for VHC.
        FS = sf.FuzzySystem()
        FS = self._create_vhs_fs_input(FS)
        FS = self._create_vhs_fs_output(FS, mode)
        FS = self._create_vhs_fs_rules(FS)
        return FS

    def _create_vhs_fs_input(self, FS):
        """Define fuzzy sets and linguistic variables."""
        T_1 = sf.TrapezoidFuzzySet(a=-25., b=-25., c=-20., d=-0, term='negative')
        T_2 = sf.TrapezoidFuzzySet(a=-20., b=0., c=0., d=20., term='constant')
        T_3 = sf.TrapezoidFuzzySet(a=0., b=20., c=25., d=25., term='positive')
        # sf.LinguisticVariable([T_1, T_2, T_3], universe_of_discourse=[-25, 25]).plot()
        FS.add_linguistic_variable('TDiff', sf.LinguisticVariable([T_1, T_2, T_3], concept='Temperature-Difference', universe_of_discourse=[-25, 25]))

        G_1 = sf.TrapezoidFuzzySet(a=-25., b=-25., c=-20., d=-10., term='negative')
        G_2 = sf.TrapezoidFuzzySet(a=-20., b=-10., c=0., d=20., term='constant')
        G_3 = sf.TrapezoidFuzzySet(a=0., b=20, c=25, d=25, term='positive')
        # sf.LinguisticVariable([G_1, G_2, G_3], universe_of_discourse=[-25, 25]).plot()
        FS.add_linguistic_variable('TGain', sf.LinguisticVariable([G_1, G_2, G_3], concept='Temperature-Gain', universe_of_discourse=[-25, 25]))
        return FS

    def _create_vhs_fs_output(self, FS, mode='mamdani'):
        """Define VHS Fuzzy System Output."""
        match mode:
            case 'mamdani':
                # Define output fuzzy sets and linguistic variable
                P_1 = sf.TrapezoidFuzzySet(a=1., b=100., c=100., d=100., term='on')
                # P_1 = sf.FuzzySet(points=[[0.0, 0.0], [1.0, 0.0], [10.0, .1], [30.0, 0.2], [50.0, 0.6], [75.0, 0.9], [90.0, 0.95], [100.0, 1.0]], term="on")
                P_2 = sf.CrispSet(a=0, b=1, term="off")
                # P_2 = sf.TrapezoidFuzzySet(a=0., b=0., c=1., d=2., term='off')
                # sf.LinguisticVariable([P_1], universe_of_discourse=[0, 120]).plot()
                FS.add_linguistic_variable('Power', sf.LinguisticVariable([P_1, P_2], concept='Power', universe_of_discourse=[0, 100]))
                pass
            case 'sugeno':
                # Define output functions
                FS.set_output_function('on', '-TDiff*2 - TGain*2')
                FS.set_crisp_output_value('off', 0.)
                pass
            case _:
                pass
        return FS

    def _create_vhs_fs_rules(self, FS):
        """Define VHS Fuzzy System Rules."""
        R0 = 'IF (TDiff IS constant) AND (TGain IS negative) THEN (Power IS on) WEIGHT 1.0'
        R1 = 'IF (TDiff IS constant) AND (TGain IS constant) THEN (Power IS off WEIGHT 1.0'
        R2 = 'IF (TDiff IS constant) AND (TGain IS positive) THEN (Power IS off) WEIGHT 1.0'
        R3 = 'IF (TDiff IS positive) AND (TGain IS negative) THEN (Power IS off) WEIGHT 1.0'
        R4 = 'IF (TDiff IS positive) AND (TGain IS constant) THEN (Power IS off) WEIGHT 1.0'
        R5 = 'IF (TDiff IS positive) AND (TGain IS positive) THEN (Power IS off) WEIGHT 1.0'
        R6 = 'IF (TDiff IS negative) AND (TGain IS negative) THEN (Power IS on) WEIGHT 1.0'
        R7 = 'IF (TDiff IS negative) AND (TGain IS constant) THEN (Power IS on) WEIGHT 1.0'
        R8 = 'IF (TDiff IS negative) AND (TGain IS positive) THEN (Power IS off) WEIGHT 1.0'
        R = [R0, R1, R2, R3, R4, R5, R6, R7, R8]
        FS.add_rules(R)
        return FS, R

    # Private multiprocessing methods
    def mp_calc_power(self, d, g):
        """
        Caluculate power using Fuzzy System.

        Parameters
        ----------
        d : numpy.float64
            Temperature difference d = t - set-value. Unit: K
        g : numpy.float64
            Gain of temperature. Unit: K/s

        Returns
        -------
        numpy.float64
            Power as PWM ratio.

        """
        self._fs.set_variable("TDiff", d)
        self._fs.set_variable("TGain", g)
        strengths = self._fs.get_firing_strengths()
        match self._fuzzy_interference_mode:
            case 'mamdani':
                p = self._fs.Mamdani_inference(["Power"])["Power"]
                if p < 0.:
                    p = 0.
                else:
                    p *= 1.5
                pass
            case 'sugeno':
                p = self._fs.Sugeno_inference(["Power"])["Power"]
                if p < 0.:
                    p = 0.
                else:
                    p *= 2.
                pass
            case _:
                p = self._fs.inference(["Power"])["Power"]  # Perform interference
                pass
        p = min(max(0, p), 100)
        if self._strength:
            return p, strengths
        else:
            return p, None

    def mp_calc_powers(self, D, G):
        """
        Caluculate powers using Fuzzy System.

        Parameters
        ----------
        D : numpy.array(numpy.float64)
            Array of Temperature differences d = t - set-value. Unit: K
        G : numpy.array(numpy.float64)
            Array of Gains of temperature. Unit: K/s.

        Returns
        -------
        P : numpy.array(numpy.float64)
            Array of Powers as PWM ratio.

        """
        t0 = time.time()
        # time.sleep(.75)
        vhs_logger.debug(f'{self._name} mp_calc_powers: Size of D={D.size}, Size of G={G.size}')
        P = np.zeros((min(D.size, G.size)), dtype=np.float64)
        for ii in range(min(D.size, G.size)):
            p, s = self.mp_calc_power(D[ii], G[ii])
            vhs_logger.debug(f'mp_calc_powers: ii={ii}, d={D[ii]}, , g={G[ii]}, p={p}, s={s}')
            P[ii] = p
            pass
        vhs_logger.debug(f'{self._name} mp_calc_powers(): dt={str(time.time() - t0)}')
        return P

    def run(self):
        # pp = mp.parent_process()
        # p = mp.current_process()
        # vhs_logger.info(f'{self._name} {p.name} (pid={p.pid}, parent={pp.pid})) {self._name} run() entered.')
        # vhs_logger.info(f'{self._name} Current process {p}')
        # vhs_logger.info(f'{self._name} run() q_in: {self._mpq_in} q_ret: {self._mpq_ret}')
        if True:
            vhs_logger.info(f'{self._name}  run(): Waiting for first item in: {self._mpq_in}')
            # item = self._mpq_in.get(True, 30.)
            item = self._mpq_in.get()
            vhs_logger.info(f'{self._name} v: got first item: {item}')
        vhs_logger.info(f'{self._name}  run() Sending process started in q_ret: {self._mpq_ret}')
        self._mpq_ret.put(f'{self._name} run() started.')
        try:
            while True:
                try:
                    vhs_logger.debug(f'{self._name} run(): Waiting for item.')
                    item = self._mpq_in.get()  # (True, 1.)
                    vhs_logger.debug(f'{self._name} run(): Got item: {item}')
                except mp.queues.Empty:
                    vhs_logger.exception(f'{self._name} run() Exception queue empty caught.')
                    time.sleep(.1)
                    continue
                if item is None:
                    vhs_logger.debug(f'{self._name} run(): Got item=None, breaking loop.')
                    break
                else:
                    vhs_logger.debug(f'{self._name} run(): Calculating Power...')
                    # t0 = time.time()
                    P = self.mp_calc_powers(item['D'], item['G'])
                    vhs_logger.debug(f'{self._name} run(): P={P}.')
                    return_item = {'subset': item['subset'], 'Power': P}
                    vhs_logger.debug(f'{self._name} run(): Return item: {return_item}')
                    self._mpq_ret.put(return_item)
                    vhs_logger.debug(f'{self._name} run(): Response enqueued.')
        except Exception as e:
            vhs_logger.exception(f'{self._name} run() Exception queue empty caught. ' + str(e))
        finally:
            self._mpq_in.close()
            self._mpq_ret.close()
            pass
        vhs_logger.debug(f'{self._name} run() Done.')
        pass
    pass  # End of class FuzzyProcess


class FuzzyController(Actor):
    """Derived class performing the heat control."""

    def __init__(self, name,
                 start_periodic=False,
                 mqtt=None,
                 fuzzy_interference_mode=None,
                 fuzzy_mp=False,
                 mpq_in=None,
                 mpq_ret=None
                 ):
        """
        Initialize this instance.

        Arguments
        ---------
        name -- Name of instance
        start_periodic -- Auto start periodic action if true. Default is False.
        mqtt_actor -- Reference to MQTT actor. Default is None.

        """
        super().__init__(name, start_periodic, mqtt)
        self._fuzzy_mp = fuzzy_mp
        self._mpq_in = mpq_in
        self._mpq_ret = mpq_ret
        vhs_logger.debug(f'{self._name} __init__() mpq_in={self._mpq_in} mpq_ret={self._mpq_ret}')
        if not fuzzy_interference_mode:
            self._fuzzy_interference_mode = fuzzy_interference_mode
        else:
            self._fuzzy_interference_mode = 'mamdani'
        # Create a fuzzy system object for VHS.
        self._fs, self._rules = create_vhs_fs(mode=self._fuzzy_interference_mode)
        self._fuzzy_controller_subscriptions = ('Set-Command', )
        pass

    def __del__(self):
        """Finalize instance."""
        super().__del__()
        pass

    # Protected override methods from super Actor
    def _on_connect(self):
        """
        Called from _on_receive() when message was received from paho.mqtt.client callback message on_connect.

        Perform action on MQTT connect here, e.g. publish and subscribe topics.

        Returns
        -------
        None.

        """
        vhs_logger.debug(self._name + ' FuzzyController _on_connect() Received MQTT connected message. Publish and subscribe...')
        try:
            if self._mqtt is not None:
                vhs_logger.debug(self._name + ' FuzzyController _on_connect() Publishing:' + str(self._fuzzy_controller_subscriptions))
                prefixed_topics = ()
                for topic in self._fuzzy_controller_subscriptions:
                    prefixed_topic = self._name + '/' + topic
                    vhs_logger.debug(self._name + ' FuzzyController _on_connect() Publishing:' + str(prefixed_topic))
                    prefixed_topics = prefixed_topics + (prefixed_topic,)
                    self._mqtt.publish_topic(prefixed_topic, '', qos=0, retain=True, expand=False)
                vhs_logger.debug(self._name + ' FuzzyController _on_connect() Subscribing to: ' + str(prefixed_topics))
                rcs_mids = self._mqtt.subscribe_topics(self._in_future, prefixed_topics, qos=0).get()
                vhs_logger.debug(self._name + ' FuzzyController _on_connect() Subscribing to: ' + str(prefixed_topics) + ' returned rcs, mids = ' + str(rcs_mids))
        except Exception as e:
            vhs_logger.exception(self._name + ' FuzzyController _on_connect(): Exception: ' + str(e))
        super()._on_connect()
        pass

    def _on_message(self, topic, msg):
        """
        Called from Mqtt._on_receive() when message was received from paho.mqtt.client callback message on_message.

        Perform action on MQTT messages here or forward to super.

        Returns
        -------
        None.

        """
        vhs_logger.debug(self._name + ' FuzzyController _on_message() Received MQTT topic.' + topic + ' msg: ' + str(msg))
        try:
            sub_topics = re.split('/', topic)
            vhs_logger.debug(self._name + ' FuzzyController _on_message() Received MQTT sub-topics: ', str(sub_topics))
            match sub_topics[2]:
                case 'Set-Command':
                    try:
                        vhs_logger.debug(self._name + ' FuzzyController _on_message() Set-Command received, msg=' + str(msg.strip()))
                        pass
                    except Exception as e:
                        vhs_logger.exception(self._name + ' _on_message() Set-Command ignored due to exception: ' + str(e))
                case _:  # Call super
                    vhs_logger.debug(self._name + ' FuzzyController _on_message() Calling super() due for unknown topic: ' + topic + ' msg: ' + str(msg))
                    super()._on_message(topic, msg)
        except TypeError:
            pass
        pass

    def _on_receive(self, message):
        """
        May be implemented for the actor to handle regular non-proxy messages.

        Virtual protected method.
        Override with concrete child class implementation.

        Returns
        -------
        None.

        """
        vhs_logger.debug(self._name + ' FuzzyController _on_receive() call super()._on_receive(message)')
        super()._on_receive(message)
        pass

    def _on_start(self):
        """
        Hook for doing any setup that should be done after the actor is started, but before it starts processing messages.

        Virtual protected method.
        Override with concrete child class implementation.

        Returns
        -------
        None.

        """
        vhs_logger.debug(self._name + ' FuzzyController _on_start()')
        # super()._on_start()
        self._in_future.start_fuzzy_process()
        pass

    def start_fuzzy_process(self):
        """
        Create and start process for fuzzy calculations.

        Returns
        -------
        None.

        """
        vhs_logger.info(f'{self._name} FuzzyController start_fuzzy_process()')
        # super()._on_start()
        if self._fuzzy_mp:
            vhs_logger.info(f'{self._name} start_fuzzy_process() Try to create and start process.')
            try:
                vhs_logger.info(f'{self._name} Create FuzzyProzess with mpq_in={self._mpq_in}, mpq_ret={self._mpq_ret}')
                self._fsp = FuzzyProcess(self._name, mpq_in=self._mpq_in, mpq_ret=self._mpq_ret)
                vhs_logger.info(f'{self._name} start_fuzzy_process() FuzzyProcess created. Exit code={self._fsp.exitcode} Details: {self._fsp}')
                vhs_logger.info(f'{self._name} start_fuzzy_process() Trying to start FuzzyProcess.')
                self._fsp.start()  # exitcode=-SIGSEGV> when run from console. Why? Works in Spyder.
                time.sleep(1)
                vhs_logger.debug(f'{self._name} start_fuzzy_process() started. Exit code={self._fsp.exitcode} Details: {self._fsp}')
                vhs_logger.info(f'{self._name} start_fuzzy_process() Trying to send first item.')
                self._mpq_in.put('First put.')
                vhs_logger.debug(f'{self._name} start_fuzzy_process() Process status after first put to FuzzyProcess. Exit code={self._fsp.exitcode} Details: {self._fsp}')
                vhs_logger.info(self._name + ' FuzzyController start_fuzzy_process() done.')
            except Exception as e:
                vhs_logger.exception(f'{self._name} start_fuzzy_process() FuzzyProcess not created/started due to exception. Details: {str(e)}')
        pass

    def _on_stop(self):
        """
        Hook for doing any cleanup that should be done after the actor has processed the last message, and before the actor stops.

        Virtual protected method.
        Override with concrete child class implementation.

        Returns
        -------
        None.

        """
        vhs_logger.debug(self._name + ' FuzzyController _on_stop()')
        if self._fuzzy_mp:
            vhs_logger.debug(f'{self._name} _on_stop() Wait for joining process.')
            try:
                self._mpq_in.put(None)
                self._fsp.join()
                vhs_logger.debug(f'{self._name} _on_stop() Process joined.')
            except Exception as e:
                vhs_logger.exception(f'{self._name} _on_stop() Process not joined due to exception: {str(e)}.')
                pass
        if self._mqtt is not None:
            prefixed_topics = ()
            vhs_logger.debug(self._name + ' FuzzyController _on_stop() Unsubscribing: ' + str(self._fuzzy_controller_subscriptions))
            for topic in self._fuzzy_controller_subscriptions:
                prefixed_topic = self._name + '/' + topic
                prefixed_topics = prefixed_topics + (prefixed_topic,)
            vhs_logger.debug(self._name + ' FuzzyController _on_stop() Unsubscribing from: ' + str(prefixed_topics))
            self._mqtt.unsubscribe_topics(self, prefixed_topics)
        super()._on_stop()
        pass

    def _periodic(self, reason):
        """
        Implementation of periodic actions.

        Read data from instrument.

        Parameters
        ----------
        reason : PeriodicCallReason
            Reason for calling this method.

        Returns
        -------
        None.

        """
        match reason:
            case PeriodicCallReason.Start:
                pass
            case PeriodicCallReason.Do:
                time.sleep(1)
                pass
            case PeriodicCallReason.Stop:
                pass
        pass

    # Protected override methods from super Device
    def _sw_revision(self):
        """
        Return revision of this class inclusive parents.

        Returns
        -------
        string
            Software revision.

        """
        return 'FuzzyController_0.0.1.0|' + super()._sw_revision()

    # Public methods of this class
    def calc_power(self, d, g, mode='mamdani', strength=False):
        """
        Caluculate power using Fuzzy System.

        Parameters
        ----------
        d : numpy.float64
            Temperature difference d = t - set-value. Unit: K
        g : numpy.float64
            Gain of temperature. Unit: K/s
        mode : str, optional
            Interference mode: 'mamdani' or 'sugeno'. The default is 'mamdani'.
        strength : bool, optional
            Return a list of the firing strengths of the the rules. The default is False.

        Returns
        -------
        numpy.float64
            Power as PWM ratio.

        """
        self._fs.set_variable("TDiff", d)
        self._fs.set_variable("TGain", g)
        strengths = self._fs.get_firing_strengths()
        match mode:
            case 'mamdani':
                p = self._fs.Mamdani_inference(["Power"])["Power"]
                if p < 0.:
                    p = 0.
                else:
                    p *= 1.5
                pass
            case 'sugeno':
                p = self._fs.Sugeno_inference(["Power"])["Power"]
                if p < 0.:
                    p = 0.
                else:
                    p *= 2.
                pass
                pass
            case _:
                p = self._fs.inference(["Power"])["Power"]  # Perform inference
                pass
        p = min(max(0, p), 100)
        if strength:
            return p, strengths
        else:
            return p, None

    def calc_powers(self, D, G, mode='mamdani'):
        """
        Caluculate powers using Fuzzy System.

        Parameters
        ----------
        D : numpy.array(numpy.float64)
            Array of Temperature differences d = t - set-value. Unit: K
        G : numpy.array(numpy.float64)
            Array of Gains of temperature. Unit: K/s.
        mode : str, optional
            Interference mode: 'mamdani' or 'sugeno'. The default is 'mamdani'.

        Returns
        -------
        P : numpy.array(numpy.float64)
            Array of Powers as PWM ratio.

        """
        t0 = time.time()
        # time.sleep(.75)
        vhs_logger.debug(f'Size of D={D.size}, Size of G={G.size}')
        P = np.zeros((min(D.size, G.size)), dtype=np.float64)
        for ii in range(min(D.size, G.size)):
            p, s = self.calc_power(D[ii], G[ii], mode, strength=False)
            vhs_logger.debug(f'ii={ii}, d={D[ii]}, , g={G[ii]}, p={p}, s={s}')
            P[ii] = p
            time.sleep(.001)
            pass
        vhs_logger.debug(self._name + ' FuzzyController calc_powers(): dt=' + str(time.time() - t0))
        return P

    def plot_fs(self, d_list, g_list, mode='mamdani'):
        """
        Plot Power vs. TDiff, TGain for selected values.

        Parameters
        ----------
        D : numpy.array(numpy.float64)
            Array of Temperature differences d = t - set-value. Unit: K
        G : numpy.array(numpy.float64)
            Array of Gains of temperature. Unit: K/s.
        mode : str, optional
            Interference mode: 'mamdani' or 'sugeno'. The default is 'mamdani'.

        Returns
        -------
        P : numpy.array(numpy.float64)
            Array of Powers as PWM ratio.
        D : numpy.array(numpy.float64)
            Array of Temperature differences d = t - set-value. Unit: K
        G : numpy.array(numpy.float64)
            Array of Gains of temperature. Unit: K/s.

        """
        self._fs.produce_figure(outputfile='', max_figures_per_row=3)
        # self._fs.plot_variable('TDiff')
        # self._fs.plot_variable('TGain')
        # self._fs.plot_variable('Power')
        D, G = np.meshgrid(d_list, g_list)
        P = np.zeros((d_list.size, g_list.size), dtype=np.float64)
        pdi = 0
        for d in d_list:
            pgi = 0
            for g in g_list:
                p, s = self.calc_power(d, g, mode)
                P[pdi, pgi] = p
                pgi += 1
            pdi += 1
        plt.figure()
        cp = plt.contourf(D, G, P)
        plt.colorbar(cp)
        plt.xlabel('TDiff /K')
        plt.ylabel('TGain /K/s')
        match mode:
            case 'mamdani':
                plt.title('VHS Fuzzy System (Mamdani)')
                pass
            case 'sugeno':
                plt.title('VHS Fuzzy System (Sugeno)')
                pass
            case _:
                plt.title('VHSFuzzy System (auto)')
                pass
        plt.show()
        return P, D, G

    def print_fs(self, d_list, g_list, mode='mamdani', strength=False):
        """
        Print TDiff, TGain, Power with rules for selected values.

        Parameters
        ----------
        D : numpy.array(numpy.float64)
            Array of Temperature differences d = t - set-value. Unit: K
        G : numpy.array(numpy.float64)
            Array of Gains of temperature. Unit: K/s.
        mode : str, optional
            Interference mode: 'mamdani' or 'sugeno'. The default is 'mamdani'.
        strength : bool
            print strength of rules. Default is False.

        Returns
        -------
         result : int=0

        """
        for d in d_list:
            for g in g_list:
                p, strengths = self.calc_power(d, g, mode, strength)
                print('\nTDiff=', d, 'TGain=', g, 'Power=', p)
                if strengths:
                    for ii in range(len(strengths)):
                        if strengths[ii] > 0:
                            print('\t', self._rules[ii], 'Strength=', strengths[ii])
        return 0

    # Getter

    pass  # End of class definition


if __name__ == '__main__':
    print("fuzzy_controller.py\n\
Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH\n\
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany\n\
eMail: H.Brand@gsi.de\n\
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme")
    app_name = re.split('.py', os.path.basename(__file__))[0]
    # Logging setup
    print(__name__, 'Configuring logging...')
    # Define logger to be used
    # logging.basicConfig(  # filename='example.log', filemode='w', encoding='utf-8', level=logging.INFO, style='{', format='{asctime} [{levelname:8}] {process} {thread} {module} {funcName} {lineno} {message}')
    logger = logging.getLogger('App-Logger')
    logger.setLevel(logging.DEBUG)
    pyacdaq_logger = logging.getLogger('pyacdaq')
    pyacdaq_logger.setLevel(logging.WARNING)
    vhs_logger.setLevel(logging.DEBUG)
    # Define logging formatter
    logging_formatter = logging.Formatter(style='{', fmt='{asctime} [{levelname:8}] {module} {funcName} {lineno} {message}')
    # logging_formatter = logging.Formatter(style='{', fmt='{asctime} [{levelname:8}] {process} {thread} {module} {funcName} {lineno} {message}')
    # configure_logging_handler((logger, pyacdaq_logger), logging_formatter, severity={'console': None, 'file': None, 'mqtt': None, 'syslog': None}, mqtt={'broker': 'ee-raspi1001', 'topic': 'ACDAQ/log_msg', 'user': '', 'password': ''})
    test = False
    try:
        mqtt_broker = input('MQTT Broker? (localhost)>')
        if len(mqtt_broker) == 0:
            mqtt_broker = 'localhost'
        mqtt_user = input('MQTT Username: ')
        if len(mqtt_user) == 0:
            mqtt_user = None
        mqtt_password = getpass.getpass()
        if len(mqtt_password) == 0:
            mqtt_password = None
        configure_logging_handler(app_name, (logger, pyacdaq_logger, vhs_logger), logging_formatter, severity={'console': logging.INFO, 'file': logging.DEBUG, 'mqtt': logging.INFO, 'syslog': logging.WARNING}, mqtt={'broker': mqtt_broker, 'topic': app_name + '/log_msg', 'user': mqtt_user, 'password': mqtt_password})
        mp_mode_input = input('Multiprocessing mode? ([n,y] default: y )>')
        if re.match('n', mp_mode_input):
            mp_mode = False
        else:
            mp_mode = True
        print(f'Multiprocessing mode: {str(mp_mode)}')
        # Create MQTT actor proxy
        print(__name__, 'Launching MQTT actor...')
        ma = Mqtt.start('Mqtt',
                        start_periodic=False,
                        broker=mqtt_broker,
                        port=1883,
                        client_id='FuzzyController',
                        clean_session=True,
                        retain=False,
                        will='offline',
                        username=mqtt_user,
                        password=mqtt_password
                        ).proxy()
        # Create fuzzy_controller actor proxy
        print(__name__, 'Launching fuzzy_controller actor.')
        time.sleep(3)
        a_fuzzy_controller = FuzzyController.start('fuzzy_controller',
                                                   start_periodic=True,
                                                   mqtt=ma,
                                                   fuzzy_mp=mp_mode
                                                   )
        p_fuzzy_controller = a_fuzzy_controller.proxy()
        fuzzy_controller_name = p_fuzzy_controller.getName().get()
        fuzzy_controller_birthday = p_fuzzy_controller.getBirthday().get()
        fuzzy_controller_periodic = p_fuzzy_controller.getPeriodicStatus().get()
        fuzzy_controller_sw_revision = p_fuzzy_controller.getSWRevision().get()
        print(__name__, 'Name:', fuzzy_controller_name,
              '; Birthday:', fuzzy_controller_birthday,
              '; Periodic:', fuzzy_controller_periodic,
              '; SW:', fuzzy_controller_sw_revision)
        d = np.float64(-10.)
        g = np.float64(-10.)
        print(__name__, 'd=', d, 'g=', g, 'ratio=', p_fuzzy_controller.calc_power(d, g, mode='mamdani', strength=True).get())
        if True:
            d_list = np.linspace(-25.0, 25.0, 64, dtype=np.float64)
            g_list = np.linspace(-25.0, 25.0, 64, dtype=np.float64)
            for ii in range(10):
                P = p_fuzzy_controller.calc_powers(d_list, g_list, mode='mamdani').get()
            print(__name__, 'PWM Ratios:', P)
        if True:
            d_list = np.linspace(-25.0, 25.0, 51, dtype=np.float64)
            g_list = np.linspace(-25.0, 25.0, 51, dtype=np.float64)
            P, D, G = p_fuzzy_controller.plot_fs(d_list, g_list, mode='mamdani').get()
        if True:
            d_list = np.array([-25., -10., -5., 2.5, 0., 2.5, 5., 10., 15., 25.], dtype=np.float64)
            g_list = np.array([-25., -15., -10., -5., 0., 5., 10., 15., 25.], dtype=np.float64)
            p_fuzzy_controller.print_fs(d_list, g_list, mode='mamdani', strength=True).get()
        time.sleep(15)
    except Exception as e:
        print(__name__, 'Exception caught:', e)
    finally:
        print(__name__, 'Stopping actors...')
        p_fuzzy_controller.stop()
        time.sleep(3)
        ma.stop()
    logging.shutdown()
    print(__name__, 'Done.')
